package com.pbs.diveapp.webServices.handler

import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.selectPhotos.model.SelectPhotoResponse
interface AddProfileHandler: BaseHandler {
    fun onSuccessFully(response: SelectPhotoResponse)
}