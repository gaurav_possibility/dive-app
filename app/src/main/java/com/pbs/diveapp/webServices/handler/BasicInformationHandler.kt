package com.pbs.diveapp.webServices.handler



import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse

interface BasicInformationHandler: BaseHandler {
    fun onSuccess(response: LoginResponse)
}