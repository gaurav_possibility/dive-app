package com.pbs.diveapp.webServices.handler

import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.likeScreen.model.SwippeResponse

interface SwipeHandler:BaseHandler {
    fun onSuccessful(swippeResponse: SwippeResponse)
}