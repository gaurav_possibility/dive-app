package com.dating.datingapp.webServices.handler

/*
* This class is used as Base Handler for this app, in api call
*
* created by GTB on 02/04/2021
*/
interface BaseHandler {
    fun onError(message: String?)
//    fun onServerError(message: String?)
}