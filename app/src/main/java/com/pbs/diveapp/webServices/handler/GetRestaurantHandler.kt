package com.pbs.diveapp.webServices.handler

import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse

interface GetRestaurantHandler:BaseHandler {
    fun onSuccessful(getRestaurantResponse: GetRestaurantResponse)
}