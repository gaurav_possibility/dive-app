package com.pbs.diveapp.webServices.handler

import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.chatScreen.model.MatchesListResponse

interface MatchListHandler:BaseHandler {
    fun onSuccessful(matchesListResponse: MatchesListResponse)
}