package com.dating.datingapp.webServices

import com.pbs.diveapp.fragments.chatScreen.model.ChatMsgListResponse
import com.pbs.diveapp.fragments.chatScreen.model.MatchesListResponse
import com.pbs.diveapp.fragments.likeScreen.model.HomeListResponse
import com.pbs.diveapp.fragments.likeScreen.model.SwippeResponse
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpData

import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.fragments.selectPhotos.model.SelectPhotoResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

import retrofit2.Call
import retrofit2.http.*


interface Apis {

    @POST("signup")
    fun signUp(@Body request: HashMap<String,String>):Call<LoginResponse>

//    @POST("user/forgotPassword")
//    fun forgotPassword(@Body request: SignUpData): retrofit2.Call<SignUpResponse>

    @POST("login")
    fun signIn(@Body request:HashMap<String,String>): Call<LoginResponse>

    @POST("social-login")
    fun socialLogin(@Body request:HashMap<String,String>): Call<LoginResponse>

    @POST("update-profile")
    fun updateProfile(@Body hashMap: HashMap<String,String>): Call<LoginResponse>

    @GET("home")
    fun userList():Call<HomeListResponse>


    @POST("send-otp")
    fun sendOTP(@Body hashMap: HashMap<String,String>): Call<LoginResponse>

    @POST("verify-otp")
    fun verifyOTP(@Body hashMap: HashMap<String,String>): Call<SignUpResponse>
//
//    @POST("user/getUserDetails")
//    fun getUserDetail(@Body request: DataEditProfile): android.telecom.Call<SettingScreenModel>

    @POST("swipe")
    fun swipe(@Body hashMap: HashMap<String, String>):Call<SwippeResponse>

    @POST("logout")
    fun logout():Call<LoginResponse>

    @GET("get-friends")
    fun getMatchList():Call<MatchesListResponse>

    @Multipart
    @POST("update-profile-pic")
    fun updateProfileImage(@Part image: MultipartBody.Part?):Call<LoginResponse>

    @GET("profile-detail")
    fun getUserDetails():Call<LoginResponse>

    @Multipart
    @POST("update-profile-pic")
    fun imageProfile(
        @Part image: List<MultipartBody.Part?>
    ): Call<SelectPhotoResponse>


    @GET("chat-detail")
    fun getChatMsgList(@Query("user_id") userId :String):Call<ChatMsgListResponse>

    @POST("delete-images")
    fun deleteImages(@Body request: HashMap<String,String>):Call<LoginResponse>


    @GET("https://maps.googleapis.com/maps/api/place/nearbysearch/json?")
    fun getRestaurant(@Query("location")location:String,
    @Query("type")type:String,@Query("key")key:String,
    @Query("radius")radius:String):Call<GetRestaurantResponse>
}