package com.pbs.diveapp.webServices.handler

import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.likeScreen.model.HomeListResponse

interface HomeListHandler:BaseHandler {
    fun onSuccessful(homeListResponse: HomeListResponse)
}