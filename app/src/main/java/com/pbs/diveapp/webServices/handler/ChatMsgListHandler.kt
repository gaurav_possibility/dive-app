package com.pbs.diveapp.webServices.handler

import com.dating.datingapp.webServices.handler.BaseHandler
import com.pbs.diveapp.fragments.chatScreen.model.ChatMsgListResponse

interface ChatMsgListHandler :BaseHandler{
    fun onSuccessful(chatMsgListResponse: ChatMsgListResponse)
}