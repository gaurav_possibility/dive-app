package com.pbs.diveapp.webServices

import android.util.Log
import com.dating.datingapp.webServices.Apis
import com.dating.datingapp.webServices.handler.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.pbs.diveapp.fragments.chatScreen.model.ChatMsgListResponse
import com.pbs.diveapp.fragments.chatScreen.model.MatchesListResponse
import com.pbs.diveapp.fragments.likeScreen.model.HomeListResponse
import com.pbs.diveapp.fragments.likeScreen.model.SwippeResponse
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.fragments.selectPhotos.model.EditProfileModel
import com.pbs.diveapp.fragments.selectPhotos.model.SelectPhotoResponse
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.SharedPref
import com.pbs.diveapp.webServices.handler.*
import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class WebService {

    companion object {
        private lateinit var apiInterface: Apis

        //this for get response in body.
        private val interceptor: HttpLoggingInterceptor by lazy {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        }

        //this is for handler write, read and connection time for apis.
        private val httpClient: OkHttpClient by lazy {
            OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                //this for header Authorization and application/json to responses from apis.
                .addInterceptor(Interceptor { chain: Interceptor.Chain ->
                    val request = chain.request()
                    val newRequest = request.newBuilder()
                        .header(
                            "Authorization",
                            "Bearer  " + SharedPref.instance.getString(AppConstant.ACCESS_TOKEN)
                        )
                        .header(
                            "Accept",
                            "application/json"
                        )
                    chain.proceed(newRequest.build())
                })
                //this give us apis response on OkHttpClient in Logcat and Run.
                .addInterceptor(interceptor).build()
        }

        // this is retrofit applied with apis and OkHttpClient.
        val mInstance: WebService by lazy {
            //logging interceptor
            apiInterface = Retrofit.Builder()
                .baseUrl(AppConstant.APP_BASE_URL)
                .client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder()
                            .setLenient()
                            .create()
                    )
                )
                .build().create(Apis::class.java)
            WebService()
        }
    }

    fun signup(request: HashMap<String, String>, handler: BasicInformationHandler) {
        apiInterface.signUp(request).enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                handler.onError(t.message ?: "")
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.success) {
                        handler.onSuccess(response.body()!!)
                    } else {
                        handler.onError(response.body()!!.message)
                    }
                } else {
                    response.errorBody()?.let { body ->
                        try {
                            val resType = object : TypeToken<LoginResponse>() {}.type
                            val errBody: LoginResponse? =
                                Gson().fromJson(body.charStream(), resType)

                            Log.e("errorBody: ", "${errBody?.message}")
                            handler.onError(errBody?.message ?: "")
                        } catch (e: Exception) {
                            Log.e("errorBody: ", "${e.message}")
                            handler.onError(e.localizedMessage)
                        }
                    }
                }
            }
        })
    }

//fun forgotPassword(request: SignUpData, handler: signupHandler) {
//    apiInterface.forgotPassword(request).enqueue(object : Callback<SignUpResponse> {
//        override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {
//            handler.onError(t.message ?: "")
//        }
//
//        override fun onResponse(
//            call: Call<SignUpResponse>,
//            response: Response<SignUpResponse>
//        ) {
//            if (response.isSuccessful) {
//                if (response.body() != null) {
//                    if (response.isSuccessful) {
//                        handler.onSuccess(response.body()!!)
//                    } else {
//                        handler.onError(response.message())
//                    }
//                } else {
//                    response.errorBody()?.let { body ->
//                        try {
//                            val resType = object : TypeToken<SignUpResponse>() {}.type
//                            val errBody: SignUpResponse? =
//                                Gson().fromJson(body.charStream(), resType)
//
//                            Log.e("errorBody: ", "${errBody?.message}")
//                            handler.onError(errBody?.message ?: "")
//                        } catch (e: Exception) {
//                            Log.e("errorBody: ", "${e.message}")
//                            handler.onError(e.localizedMessage)
//                        }
//                    }
//                }
//            }
//        }
//    })
//}


    fun updateProfile(hashMap: HashMap<String, String>, handler: signupHandler) {
        apiInterface.updateProfile(hashMap)
            .enqueue(object : Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.success)
                            handler.onSuccess(response = response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    } else {
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<LoginResponse>() {}.type
                                val errBody: LoginResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }

            })
    }

    fun userList(handler: HomeListHandler) {
        apiInterface.userList()
            .enqueue(object : Callback<HomeListResponse> {
                override fun onResponse(
                    call: Call<HomeListResponse>,
                    response: Response<HomeListResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.success)
                            handler.onSuccessful(response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    } else {
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<HomeListResponse>() {}.type
                                val errBody: HomeListResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<HomeListResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }
            })
    }

fun sendOTP(hashMap: HashMap<String, String>, handler: signupHandler) {
    apiInterface.sendOTP(hashMap)
        .enqueue(object : Callback<LoginResponse> {
            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.success)
                        handler.onSuccess(response = response.body()!!)
                    else
                        handler.onError(response.body()!!.message)
                } else {
                    response.errorBody()?.let { body ->
                        try {
                            val resType = object : TypeToken<LoginResponse>() {}.type
                            val errBody: LoginResponse? =
                                Gson().fromJson(body.charStream(), resType)
                            Log.e("errorBody: ", "${errBody?.message}")
                            handler.onError(errBody?.message ?: "")
                        } catch (e: Exception) {
                            Log.e("errorBody: ", "${e.message}")
                            handler.onError(e.localizedMessage)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
            }
        })
}

    fun swipe(hashMap: HashMap<String, String>, handler: SwipeHandler) {
        apiInterface.swipe(hashMap)
            .enqueue(object : Callback<SwippeResponse> {
                override fun onResponse(
                    call: Call<SwippeResponse>,
                    response: Response<SwippeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.success)
                            handler.onSuccessful(response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    } else {
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object :
                                    TypeToken<SwippeResponse>() {}.type
                                val errBody: SwippeResponse? =
                                    Gson().fromJson(
                                        body.charStream(),
                                        resType
                                    )

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(
                    call: Call<SwippeResponse>,
                    t: Throwable
                ) {
                    handler.onError(t.message ?: "")
                }
            })
    }

    fun logout(handler: signupHandler) {
        apiInterface.logout().enqueue(object : Callback<LoginResponse> {
            override fun onFailure(
                call: Call<LoginResponse>,
                t: Throwable
            ) {
                handler.onError(t.message ?: "")
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.success) {
                        handler.onSuccess(response.body()!!)
                    } else {
                        handler.onError(response.body()!!.message)
                    }
                } else {
                    response.errorBody()?.let { body ->
                        try {
                            val resType =
                                object : TypeToken<LoginResponse>() {}.type
                            val errBody: LoginResponse? =
                                Gson().fromJson(body.charStream(), resType)

                            Log.e("errorBody: ", "${errBody?.message}")
                            handler.onError(errBody?.message ?: "")
                        } catch (e: Exception) {
                            Log.e("errorBody: ", "${e.message}")
                            handler.onError(e.localizedMessage)
                        }
                    }
                }
            }
        })
    }


    fun getMatchList(handler:MatchListHandler){
        apiInterface.getMatchList()
            .enqueue(object :Callback<MatchesListResponse>{
                override fun onResponse(
                    call: Call<MatchesListResponse>,
                    response: Response<MatchesListResponse>
                ) {
                    if (response.isSuccessful && response.body() !=null){
                        if (response.body()!!.success)
                            handler.onSuccessful(response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    }else{
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<MatchesListResponse>() {}.type
                                val errBody: MatchesListResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<MatchesListResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }
            })
    }


    fun verify(hashMap: HashMap<String,String>,handler: asfasdfasddsfHandler){
        apiInterface.verifyOTP(hashMap)
            .enqueue(object :Callback<SignUpResponse>{
                override fun onResponse(
                    call: Call<SignUpResponse>,
                    response: Response<SignUpResponse>
                ) {
                    if (response.isSuccessful && response.body() !=null){
                        if (response.body()!!.success)
                            handler.onSuccess(response = response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    }else{
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<SignUpResponse>() {}.type
                                val errBody: SignUpResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }

            })
    }

    fun updateImage(image: MultipartBody.Part?,handler: loginHandler){
        apiInterface.updateProfileImage(image)
            .enqueue(object :Callback<LoginResponse>{
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if (response.isSuccessful && response.body() !=null){
                        if (response.body()!!.success)
                            handler.onSuccess(response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    }else{
                        response.errorBody()?.let { body ->
                            try {
                                val resType =
                                    object : TypeToken<LoginResponse>() {}.type
                                val errBody: LoginResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }
            })
    }


    fun getUserDetails(handler:loginHandler){
        apiInterface.getUserDetails()
            .enqueue(object :Callback<LoginResponse>{
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if (response.isSuccessful && response.body() !=null){
                        if (response.body()!!.success)
                            handler.onSuccess(response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    }else{
                        response.errorBody()?.let { body ->
                            try {
                                val resType =
                                    object : TypeToken<LoginResponse>() {}.type
                                val errBody: LoginResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }
            })
    }
//    fun verifyOtp(otpId: String, otp: String, handler: signupHandler) {
//        apiInterface.verifyOtp(otpId, otp).enqueue(object : Callback<SignUpResponse> {
//            override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {
//                handler.onError(t.message ?: "")
//            }
//
//            override fun onResponse(
//                call: Call<SignUpResponse>,
//                response: Response<SignUpResponse>
//            ) {
//                if (response.isSuccessful) {
//                    if (response.body() != null) {
//                        if (response.isSuccessful) {
//                            handler.onSuccess(response.body()!!)
//                        } else {
//                            handler.onError(response.message().toString())
//                        }
//                    } else {
//                        response.errorBody()?.let { body ->
//                            try {
//                                val resType = object : TypeToken<SignUpResponse>() {}.type
//                                val errBody: SignUpResponse? =
//                                    Gson().fromJson(body.charStream(), resType)
//
//                                Log.e("errorBody: ", "${errBody?.message}")
//                                handler.onError(errBody?.message ?: "")
//                            } catch (e: Exception) {
//                                Log.e("errorBody: ", "${e.message}")
//                                handler.onError(e.localizedMessage)
//                            }
//                        }
//                    }
//                }
//            }
//        })
//    }localizedMessage


//    fun basicInformation(request: BasicInformationData, handler: BasicInformationHandler) {
//        apiInterface.basicInformation(request).enqueue(object : Callback<BasicResponse> {
//            override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
//                handler.onError(t.message ?: "")
//            }
//
//            override fun onResponse(
//                call: Call<BasicResponse>,
//                response: Response<BasicResponse>
//            ) {
//                if (response.isSuccessful) {
//                    if (response.body() != null) {
//                        if (response.isSuccessful) {
//                            handler.onSuccess(response.body()!!)
//                        } else {
//                            handler.onError(response.message().toString())
//                        }
//                    } else {
//                        response.errorBody()?.let { body ->
//                            try {
//                                val resType = object : TypeToken<BasicResponse>() {}.type
//                                val errBody: SignUpResponse? =
//                                    Gson().fromJson(body.charStream(), resType)
//
//                                Log.e("errorBody: ", "${errBody?.message}")
//                                handler.onError(errBody?.message ?: "")
//                            } catch (e: Exception) {
//                                Log.e("errorBody: ", "${e.message}")
//                                handler.onError(e.localizedMessage)
//                            }
//                        }
//                    }
//                }
//            }
//        })
//    }

            fun login(request: HashMap<String, String>, handler: loginHandler) {
                apiInterface.signIn(request).enqueue(object : Callback<LoginResponse> {
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        handler.onError(t.message.toString() ?: "")
                    }

                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        if (response.isSuccessful && response.body() != null) {

                            if (response.body()!!.success) {
                                handler.onSuccess(response.body()!!)
                            } else {
                                handler.onError(response.body()!!.message.toString())
                            }
                        } else {
                            response.errorBody()?.let { body ->
                                try {
                                    val resType = object : TypeToken<LoginResponse>() {}.type
                                    val errBody: LoginResponse? =
                                        Gson().fromJson(body.charStream(), resType)

                                    Log.e("errorBody: ", "${errBody?.message}")
                                    handler.onError(errBody?.message ?: "")
                                } catch (e: Exception) {
                                    Log.e("errorBody: ", "${e.message}")
                                    handler.onError(e.localizedMessage)
                                }
                            }
                        }
                    }
                })
            }

            fun socialLogin(request: HashMap<String, String>, handler: loginHandler) {
                apiInterface.socialLogin(request).enqueue(object : Callback<LoginResponse> {
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        handler.onError(t.message.toString() ?: "")
                    }

                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        if (response.isSuccessful && response.body() != null) {

                            if (response.body()!!.success) {
                                handler.onSuccess(response.body()!!)
                            } else {
                                handler.onError(response.body()!!.message.toString())
                            }
                        } else {
                            response.errorBody()?.let { body ->
                                try {
                                    val resType = object : TypeToken<LoginResponse>() {}.type
                                    val errBody: LoginResponse? =
                                        Gson().fromJson(body.charStream(), resType)

                                    Log.e("errorBody: ", "${errBody?.message}")
                                    handler.onError(errBody?.message ?: "")
                                } catch (e: Exception) {
                                    Log.e("errorBody: ", "${e.message}")
                                    handler.onError(e.localizedMessage)
                                }
                            }
                        }
                    }
                })
            }

    fun imageEdit(
        userId: RequestBody,
        image: MutableList<MultipartBody.Part?>,
        handler: AddProfileHandler
    ) {

        // val uploadImage = CommonMethods.getRequestBodyFromFile("images", image)
        apiInterface.imageProfile(image)
            .enqueue(object : Callback<SelectPhotoResponse> {
                override fun onFailure(call: Call<SelectPhotoResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }

                override fun onResponse(
                    call: Call<SelectPhotoResponse>,
                    response: Response<SelectPhotoResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {

                        if (response.body()!!.success!!) {
                            handler.onSuccessFully(response.body()!!)
                        } else {
                            handler.onError(response.message().toString())
                        }
                    } else {
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<EditProfileModel>() {}.type
                                val errBody: SignUpResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

            })
    }

    fun getChatMsgList(hashMap: String, handler:ChatMsgListHandler){

        apiInterface.getChatMsgList(hashMap)
            .enqueue(object :Callback<ChatMsgListResponse>{
                override fun onResponse(
                    call: Call<ChatMsgListResponse>,
                    response: Response<ChatMsgListResponse>
                ) {
                    if (response.isSuccessful && response.body() !=null){
                        if (response.body()!!.success)
                            handler.onSuccessful(response.body()!!)
                        else
                            handler.onError(response.body()!!.message)
                    }else{
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<ChatMsgListResponse>() {}.type
                                val errBody: ChatMsgListResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.message}")
                                handler.onError(errBody?.message ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ChatMsgListResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }

            })
    }

    fun deleteImages(request: HashMap<String, String>, handler: BasicInformationHandler) {
        apiInterface.deleteImages(request).enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                handler.onError(t.message ?: "")
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.success) {
                        handler.onSuccess(response.body()!!)
                    } else {
                        handler.onError(response.body()!!.message)
                    }
                } else {
                    response.errorBody()?.let { body ->
                        try {
                            val resType = object : TypeToken<LoginResponse>() {}.type
                            val errBody: LoginResponse? =
                                Gson().fromJson(body.charStream(), resType)

                            Log.e("errorBody: ", "${errBody?.message}")
                            handler.onError(errBody?.message ?: "")
                        } catch (e: Exception) {
                            Log.e("errorBody: ", "${e.message}")
                            handler.onError(e.localizedMessage)
                        }
                    }
                }
            }
        })
    }

    fun getRestaurant(location:String,type:String,key:String,radius:String,handler:GetRestaurantHandler){
        apiInterface.getRestaurant(location,type,key,radius)
            .enqueue(object :Callback<GetRestaurantResponse>{
                override fun onResponse(
                    call: Call<GetRestaurantResponse>,
                    response: Response<GetRestaurantResponse>
                ) {
                    if (response.isSuccessful && response.body() !=null){
                        if (response.body()!!.status=="OK")
                            handler.onSuccessful(response.body()!!)
                        else
                            handler.onError(response.body()!!.status)
                    }else{
                        response.errorBody()?.let { body ->
                            try {
                                val resType = object : TypeToken<GetRestaurantResponse>() {}.type
                                val errBody: GetRestaurantResponse? =
                                    Gson().fromJson(body.charStream(), resType)

                                Log.e("errorBody: ", "${errBody?.status}")
                                handler.onError(errBody?.status ?: "")
                            } catch (e: Exception) {
                                Log.e("errorBody: ", "${e.message}")
                                handler.onError(e.localizedMessage)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<GetRestaurantResponse>, t: Throwable) {
                    handler.onError(t.message ?: "")
                }
            })
    }
        }