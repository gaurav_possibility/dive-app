package com.pbs.diveapp.fragments.mapScreen.presenter

import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.fragments.mapScreen.view.MapScreenFragmentView
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.GetRestaurantHandler

class MapScreenFragmentPresenter(val view: MapScreenFragmentView) : MapScreenFragmentViewHandler {
    override fun getRest(location: String, type: String, key: String, radius: String) {
        view.showProgressBar()
        WebService.mInstance.getRestaurant(
            location,
            type,
            key,
            radius,
            object : GetRestaurantHandler {
                override fun onSuccessful(getRestaurantResponse: GetRestaurantResponse) {
                    view.hideProgressBar()
                    view.onSuccessful(getRestaurantResponse)
                }

                override fun onError(message: String?) {
                    view.hideProgressBar()
                    view.showToast(message!!)
                }
            })
    }
}