package com.pbs.diveapp.fragments.loginScreen.LoginModel

import com.pbs.diveapp.fragments.likeScreen.model.Images

data class Data(
    val account_status: String,
    val age: String,
    val apple_id: Any,
    val bio: String,
    val country_code: String,
    val created_at: String,
    val deleted_at: Any,
    val device_token: String,
    val device_type: String,
    val dob: Any,
    val education: String,
    val email: String,
    val email_verified_at: Any,
    val facebook_id: Any,
    val gender: String,
    val height: String,
    val here_for: String,
    val hometown: String,
    val id: Int,
    val image: String?=null,
    val user_images: List<ImagesLogin>,
    val interests: List<Any>,
    val is_number_verified: String,
    val is_profile_completed: String,
    val lat: String,
    val lng: String,
    val looking_for: String,
    val mobile: String,
    val name: String,
    val updated_at: String,
    val user_type: String
)

data class ImagesLogin(
    val created_at: String,
    val id: Int,
    val updated_at: String,
    val user_id: Int,
    val file: String,
    val file_type: String
)