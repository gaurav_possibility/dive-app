package com.pbs.diveapp.fragments.mapScreen

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.eventDetailsScreen.EventDetailsActivity
import com.pbs.diveapp.activities.venueDetailsScreen.VenueDetailsActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.mapScreen.adapter.EventsListAdapter
import com.pbs.diveapp.fragments.mapScreen.adapter.ItemClickView
import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.fragments.mapScreen.model.ResultRest
import com.pbs.diveapp.fragments.mapScreen.presenter.MapScreenFragmentPresenter
import com.pbs.diveapp.fragments.mapScreen.view.MapScreenFragmentView
import com.pbs.diveapp.fragments.venueListScreen.adapter.ItemClick
import com.pbs.diveapp.fragments.venueListScreen.adapter.VenueListAdapter
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_map_screen.*
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class MapScreenFragment : BaseFragment(), OnMapReadyCallback, View.OnClickListener,
    MapScreenFragmentView, ItemClick,ItemClickView {

    private lateinit var mMap: GoogleMap
    lateinit var mContext: Context


    lateinit var mPresenter:MapScreenFragmentPresenter
    val listRest = ArrayList<ResultRest>()
    private val LOCATION_PERMISSION = 2
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    var wayLatitude: Double = 0.0
    var wayLongitude: Double = 0.0
    var city = ""
    var state = ""
    var address = ""

    lateinit var mAdapter: VenueListAdapter
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_map_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = MapScreenFragmentPresenter(this)
        locationRequest = LocationRequest()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)
        tvMap.isSelected = true
        allClicks()
        map_view.onCreate(savedInstanceState);
        map_view.getMapAsync(this); //this is important

        val list = arrayListOf(
            "1 G.W.SherKey's Raw Bar and Grill",
            "2 G.W.SherKey's Raw Bar and Grill",
            "3 G.W.SherKey's Raw Bar and Grill"
        )
        mAdapter = VenueListAdapter(mContext, listRest, this,0)
        rvList.layoutManager = LinearLayoutManager(mContext)
        rvList.adapter = mAdapter

        rvEventList.layoutManager = LinearLayoutManager(mContext)
        rvEventList.adapter = EventsListAdapter(mContext,list,this)

    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
        getLocation()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onDestroy() {
        try {
            map_view.onDestroy()
        }catch (e:Exception){}
        super.onDestroy()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in Sydney")
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

    }

    private fun allClicks() {
        tvList.setOnClickListener(this)
        tvMap.setOnClickListener(this)
        tvEvents.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvList -> {
                map_view.visibility =View.GONE
                layoutEventsList.visibility =View.GONE
                layoutList.visibility =View.VISIBLE
                tvList.isSelected = true
                tvMap.isSelected = false
                tvEvents.isSelected = false
            }
            R.id.tvMap -> {
                layoutList.visibility =View.GONE
                layoutEventsList.visibility =View.GONE
                map_view.visibility =View.VISIBLE
                tvList.isSelected = false
                tvMap.isSelected = true
                tvEvents.isSelected = false
            }
            R.id.tvEvents -> {
                layoutList.visibility =View.GONE
                map_view.visibility =View.GONE
                layoutEventsList.visibility =View.VISIBLE
                tvList.isSelected = false
                tvMap.isSelected = false
                tvEvents.isSelected = true
            }
        }
    }

    override fun onSuccessful(getRestaurantResponse: GetRestaurantResponse) {
        listRest.clear()
        listRest.addAll(getRestaurantResponse.results)
        mAdapter.notifyDataSetChanged()
    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
        Log.e("asdfasdf","$message")
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }

    override fun onClick(position: Int, item: ResultRest) {

    }

    override fun onClickView(position: Int, item: ResultRest) {
        requireActivity().startActivity(Intent(mContext,VenueDetailsActivity::class.java)
            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            .putExtra("restDetails",Gson().toJson(item)))
    }

    override fun onClickView(position: Int) {
        requireActivity().startActivity(Intent(mContext,EventDetailsActivity::class.java)
            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
    }

    private fun getLocation() {
        if (ContextCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                AppConstant.LOCATION_REQUEST
            )
        } else {
            try {
                locationRequest!!.interval = 6000
                locationRequest!!.fastestInterval = 1000
                locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                mFusedLocationClient!!.lastLocation
                    .addOnSuccessListener(OnSuccessListener<Location?> { location ->
                        if (location != null) {
                            wayLatitude = location.latitude
                            wayLongitude = location.longitude
                            getAddress(mContext, wayLatitude, wayLongitude)
                            val asdf="$wayLatitude,$wayLongitude"
                            mPresenter.getRest(asdf.toString(),"restaurant","AIzaSyBNi4H4h7NUKCMFJUyEzUgPrfRhcRKhSnQ",
                                "25000")
                            Log.e(
                                "locationnnn","" +
                                        wayLatitude.toString() + " " + wayLongitude
                            )


                        } else {
                            mFusedLocationClient!!.requestLocationUpdates(
                                locationRequest,
                                object : LocationCallback() {
                                    override fun onLocationResult(p0: LocationResult?) {

                                        super.onLocationResult(p0)
                                        if (p0 != null)
                                            if (p0!!.locations.isEmpty().not()) {
                                                wayLatitude = p0.locations[0].latitude
                                                wayLongitude = p0.locations[0].longitude
                                                getAddress(mContext, wayLatitude, wayLongitude)
                                                val asdf="$wayLatitude,$wayLongitude"
                                                mPresenter.getRest(asdf.toString(),"restaurant","AIzaSyBNi4H4h7NUKCMFJUyEzUgPrfRhcRKhSnQ",
                                                    "25000")
                                                Log.e(
                                                    "locationnnn",
                                                    wayLatitude.toString() + " " + wayLongitude
                                                )
                                                mFusedLocationClient!!.removeLocationUpdates(this)

                                            }
                                    }
                                },
                                null
                            )
                        }
                    })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            LOCATION_PERMISSION -> // If the permission is granted, get the location,
                getLocation()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            AppConstant.LOCATION_REQUEST -> // If the permission is granted, get the location,
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation()
                } else {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(
//                            this,
//                            android.Manifest.permission.ACCESS_FINE_LOCATION
//                        )
//                    ) {
////                        getLocation()
//                    } else {
//                        val snackbar: Snackbar = Snackbar.make(
//                            drawerLayout,
//                            resources.getString(R.string.message_text),
//                            5000
//                        )
//                        snackbar.setAction(
//                            resources.getString(R.string.settings),
//                            View.OnClickListener {
//                                if (this == null) {
//                                    return@OnClickListener
//                                }
//                                val intent = Intent()
//                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                                val uri: Uri = Uri.fromParts(
//                                    "package",
//                                    packageName,
//                                    null
//                                )
//                                intent.data = uri
//                                this.startActivity(intent)
//                            })
//                        snackbar.show()
//
//                    }
                }
        }
    }

    private fun getAddress(
        context: Context?,
        LATITUDE: Double,
        LONGITUDE: Double
    ) {
        try {
            val geocoder = Geocoder(context, Locale.getDefault())
            val addresses =
                geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null && addresses.size > 0) {
                address = addresses[0].getAddressLine(0)
                city = addresses[0].locality
//                    .plus(" "+addresses[0].adminArea)
                state = addresses[0].adminArea
                Log.e("areaa","cc".plus(addresses[0].locality.plus(" "+addresses[0].adminArea).plus(addresses[0].adminArea)))
            }
        } catch (e: IOException) {
            e.printStackTrace()
//            CommonMethod.logForApp(e.message.toString())
        }
        return
    }



}