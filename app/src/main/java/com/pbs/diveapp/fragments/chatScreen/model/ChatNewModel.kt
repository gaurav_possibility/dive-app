package com.pbs.diveapp.fragments.chatScreen.model

data class ChatNewModel(
//    var userId: String? = null,
//    var userPic: String? = null,
//    var username: String? = null,
//    var receiverId: String? = null,
//    var receivername: String? = null,
//    var receiverPic: String? = null,
//    var chatId: String? = null,
//    var usertype: String? = null,

    var type:String? = null,
    var sender_id :Int? = null,
    var receiver_id:Int? = null,
    var chatId: String? = null,
//    var groupId: String? = null,
    var message: String? = null,
    var createdAt: String? = null
)