package com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.loginScreen.LoginScreenFragment
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.presenter.SignupPresenter
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.view.SignUpPresenterView
import com.pbs.diveapp.fragments.signUpFlow.userName.NameFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_sign_up_screen.*
import java.util.*

class SignUpScreenFragment : BaseFragment(), View.OnClickListener, SignUpPresenterView {

    lateinit var callbackManager: CallbackManager    //fb call manager
    private val RC_SIGN_IN = 7
    var socialId: String = ""
    lateinit var userType: String
    var firstName: String = ""
    var userName: String = ""
    var emailFacebook: String = ""
    var lastname: String = ""
    var profileImage: String = ""
    lateinit var mPresenter: SignupPresenter

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_sign_up_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        allClicks()

        // this is used to facebook lognin
        mPresenter = SignupPresenter(this)
        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    val request =
                        GraphRequest.newMeRequest(result!!.accessToken) { `object`, response ->
                            try {
                                LoginManager.getInstance().logOut()
                                AccessToken.setCurrentAccessToken(null)
                                socialId = `object`.getString("id")
                                userName = `object`.getString("name")
                                firstName = `object`.getString("first_name")
                                if (`object`.has("last_name"))
                                    lastname = `object`.getString("last_name")

                                if (`object`.has("email")) {
                                    emailFacebook = `object`.getString("email")
                                } else {
                                    emailFacebook = ""
                                }

                                val im = `object`.getString("picture")
                                val profileImage =
                                    ("https://graph.facebook.com/" + socialId + "/picture?type=large")

//                                var hashMap = HashMap<String, String>()
//                                hashMap["email"] = emailFacebook
//                                hashMap["type"] = "1"
//                                hashMap["facebook_id"] = socialId
//                                hashMap["name"] = userName
//                                hashMap["first_name"] = firstName
//                                hashMap["last_name"] = lastname
//                                hashMap["user_type"] = userType
//
//                                mpresenter.socialLogin(hashMap)
                                Log.i(
                                    "FacebookSingIn",
                                    "Response  :" + socialId + "  " + emailFacebook + " f " + firstName + " l " + lastname + " u " + userName
                                )
                            } catch (e: Exception) {
                                Log.i("FacebookSingIn", "Response error  :" + e.message)
                            }
                        }
                    val parameters = Bundle()
                    parameters.putString("fields", "email,name,first_name, last_name,picture")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    CommonMethods.showToast(mContext, "Login Cancel")
                }

                override fun onError(error: FacebookException?) {
                    CommonMethods.showToast(mContext, "" + error!!.message)
                }
            })

    }

    private fun allClicks() {
        btnSignUp.setOnClickListener(this)
        tvSignIn.setOnClickListener(this)
        btnFacebook.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSignUp -> {

                if (etEmail.text.toString().trim().isEmpty()) {
                    CommonMethods.showToast(mContext, "Please enter email address")
                } else if (!CommonMethods.emailValidator(etEmail.text.toString().trim())) {
                    CommonMethods.showToast(mContext, "Please enter valid email")
                } else if (etPassword.text.toString().isEmpty()) {
                    CommonMethods.showToast(mContext, "Please enter password")
                } else if (etPassword.text.toString().trim().length <= 5) {
                    CommonMethods.showToast(mContext, "Password must be greater or equal to 6")
                } else {

                    var hashMap = HashMap<String, String>()
                    hashMap["email"] = etEmail.text.toString().trim()
                    hashMap["name"] = ""
                    hashMap["password"] = etPassword.text.toString().trim()

                       mPresenter.signUp(hashMap)

//                    FirebaseMessaging.getInstance().token.addOnCompleteListener {
//                        if (it.isComplete) {
//                            val firebaseToken = it.result.toString()
//                            hashMap["device_token"] = firebaseToken
//                            hashMap["device_type"] = "android"
//
//                            Log.e("firebaseToken", firebaseToken)
//                            mPresenter.signUp(hashMap)
//
//                        }
//                    }
                }
            }
            R.id.tvSignIn -> {
                CommonMethods.replaceFragmentActivityWithoutStack(
                    requireFragmentManager(),
                    LoginScreenFragment(),
                    R.id.splashContainer
                )
            }
            R.id.btnFacebook -> {
               /* if (CommonMethods.phoneIsOnlineSocial(mContext)) {
                    LoginManager.getInstance().logInWithReadPermissions(
                        this,
                        Arrays.asList("public_profile", "email")
                    )
                } else {
                    CommonMethods.showToast(mContext, "Internet connection not available")
                }*/
            }
        }
    }

    override fun onSuccessSignUP(response: LoginResponse) {
        if (response.success) {
            hideBaseProgressDialog()
            SharedPref.instance.setString(AppConstant.ACCESS_TOKEN,response.access_token)
            SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(response.data))
            SharedPref.instance.setString(AppConstant.USER_ID, response.data.id.toString())
            SharedPref.instance.setString(AppConstant.USER_NAME,response.data.name.toString())
            SharedPref.instance.setString(AppConstant.EMAIL,response.data.email.toString())
            SharedPref.instance.setBoolean(AppConstant.USER_LOGGED_IN,true)
            CommonMethods.replaceFragmentScreenWithBackStack(
                requireFragmentManager(),
                NameFragment(),
                R.id.splashContainer
            )
        }

    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message.toString())
    }

    override fun showProgressBar() {
        showBaseProgressDialog(mContext, "")
    }

    override fun hideProgressBar() {
        hideBaseProgressDialog()
    }
}