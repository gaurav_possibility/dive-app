package com.pbs.diveapp.fragments.likeScreen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.ContainerActivity
import com.pbs.diveapp.activities.SplashScreenActivity
import com.pbs.diveapp.activities.swipeUserProfile.SwipeUserProfileActivity
import com.pbs.diveapp.activities.userProfile.UserProfileActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.likeScreen.adapter.CardStackAdapter
import com.pbs.diveapp.fragments.likeScreen.adapter.ClickProfile
import com.pbs.diveapp.fragments.likeScreen.adapter.ImageAdapter
import com.pbs.diveapp.fragments.likeScreen.model.DataX
import com.pbs.diveapp.fragments.likeScreen.model.HomeListResponse
import com.pbs.diveapp.fragments.likeScreen.model.SwippeResponse
import com.pbs.diveapp.fragments.likeScreen.presenter.ListScreenFragmentPresenter
import com.pbs.diveapp.fragments.likeScreen.view.ListScreenFragmentView
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.fragment_list_screen.*


class ListScreenFragment : BaseFragment(), View.OnClickListener, ListScreenFragmentView,
    CardStackListener, ClickProfile {

    lateinit var mContext: Context
    private var behavior: BottomSheetBehavior<*>? = null

    private var currentPage = 0
    private var NUM_PAGES = 0
    lateinit var mPresenter:ListScreenFragmentPresenter

    val userList =ArrayList<DataX>()

   lateinit var userDetails : Data

    private val cardStackView by lazy { requireActivity().findViewById<CardStackView>(R.id.card_stack_view) }
    private val manager by lazy { CardStackLayoutManager(activity, this) }
    private val adapter by lazy { CardStackAdapter(userList, mContext, this) }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_list_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = ListScreenFragmentPresenter(this)
        allClicks()

//        try {
//            userDetails =Gson().fromJson(SharedPref.instance.getString(AppConstant.USER_DATA),Data::class.java)
//        }catch (e:java.lang.Exception){}
//        behavior = BottomSheetBehavior.from(userDetails)
//        behavior?.isDraggable = false

//val imageList = arrayListOf(R.drawable.demo_image)
//        verticalViewPager.adapter = ImageAdapter(mContext, image)
//        NUM_PAGES = image.size
//
//        indicator.attachToPager(verticalViewPager)

        // Auto start of viewpager
//        val handler = Handler()
//        val Update = Runnable {
//            if (currentPage == NUM_PAGES) {
//                currentPage = 0
//            }
//            verticalViewPager.setCurrentItem(currentPage++, true)
//        }
//        val swipeTimer = Timer()
//        swipeTimer.schedule(object : TimerTask() {
//            override fun run() {
//                handler.post(Update)
//            }
//        }, 3000, 3000)

        // Pager listener over indicator
//        indicator.setOnPageChangeListener(object : OnPageChangeListener {
//            override fun onPageSelected(position: Int) {
//                currentPage = position
//            }
//
//            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {}
//            override fun onPageScrollStateChanged(pos: Int) {}
//        })


    }

    override fun onResume() {
        super.onResume()
        mPresenter.getUserList()
        try {
            userDetails =Gson().fromJson(SharedPref.instance.getString(AppConstant.USER_DATA),Data::class.java)
        }catch (e:java.lang.Exception){}
        etSearch.setText("")
        etSearch.clearFocus()
        setupCardStackView()
        CommonMethods.setProfileImage(mContext, userDetails.image.toString(),ivUserProfile)
    }


    private fun setupCardStackView() {
        initialize()
    }

    private fun initialize() {
        manager.setStackFrom(StackFrom.None)
        manager.setVisibleCount(3)
        manager.setTranslationInterval(8.0f)     // for increase and decrease the height of back images
        manager.setScaleInterval(0.9f)           // for increase decrease the width of back images
        manager.setSwipeThreshold(0.1f)
        manager.setMaxDegree(40.0f)
        manager.setDirections(Direction.HORIZONTAL)
        manager.setCanScrollHorizontal(true)
        manager.setCanScrollVertical(false)
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
        manager.setOverlayInterpolator(LinearInterpolator())
        cardStackView.layoutManager = manager
        cardStackView.adapter = adapter
        cardStackView.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }

    private fun allClicks() {
//        layoutUserDetails.setOnClickListener(this)
        ivShare.setOnClickListener(this)
        ivDisLike.setOnClickListener(this)
        ivLike.setOnClickListener(this)
        ivUserProfile.setOnClickListener(this)
        iv_cross.setOnClickListener(this)
        iv_search.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
//            R.id.layoutUserDetails -> {
//                if (behavior?.state == BottomSheetBehavior.STATE_COLLAPSED) {
//                    coordinatorDetails.visibility = View.VISIBLE
//                    behavior?.state =
//                        BottomSheetBehavior.STATE_EXPANDED
//                } else {
//                    behavior?.state =
//                        BottomSheetBehavior.STATE_COLLAPSED
//                    coordinatorDetails.visibility = View.GONE
//                }
//            }
            R.id.ivShare -> {
                AlertDialog.Builder(mContext)
                    .setTitle("Logout")
                    .setMessage("Would you like to logout?")
                    .setPositiveButton("Yes") { dialog, which ->
                        // logout
                        mPresenter.logout()
                    }
                    .setNegativeButton(
                        "No"
                    ) { _, _ ->
                        // user doesn't want to logout
                    }
                    .show()
            }
            R.id.ivDisLike->{
                val setting = SwipeAnimationSetting.Builder()
                    .setDirection(Direction.Left)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(AccelerateInterpolator())
                    .build()
                manager.setSwipeAnimationSetting(setting)
                cardStackView.swipe()
            }
            R.id.ivLike->{
                val setting = SwipeAnimationSetting.Builder()
                    .setDirection(Direction.Right)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(AccelerateInterpolator())
                    .build()
                manager.setSwipeAnimationSetting(setting)
                cardStackView.swipe()
            }
            R.id.ivUserProfile->{
                requireActivity().startActivity(
                    Intent(
                        mContext,
                        UserProfileActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

                )
            }
            R.id.iv_search ->{
                etSearch.visibility=View.VISIBLE
                iv_cross.visibility=View.VISIBLE
                iv_search.visibility=View.GONE
            }
            R.id.iv_cross ->{
                iv_search.visibility=View.VISIBLE
                etSearch.visibility=View.GONE
                iv_cross.visibility=View.GONE
            }
        }
    }

    override fun onSuccessful(homeListResponse: HomeListResponse) {
        userList.clear()
        userList.addAll(homeListResponse.data.data)
        adapter.notifyDataSetChanged()
    }

    override fun onSuccessfulSwipe(swippeResponse: SwippeResponse) {
//        showToast(swippeResponse.message)
        if (swippeResponse.is_matched){
            showProgressBar()
            Handler().postDelayed(
                {
                    hideProgressBar()
            requireActivity().startActivity(
                Intent(mContext, ContainerActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .putExtra("FromScreens", "2")
                    .putExtra("UserDetails", Gson().toJson(swippeResponse.profile_detail))
            )
                },
                500
            )
        }
    }

    override fun onSuccessfulLogout(loginResponse: LoginResponse) {
        showToast(loginResponse.message)
        SharedPref.instance.clear()
        requireActivity().startActivity(Intent(mContext,SplashScreenActivity::class.java))
        requireActivity().finishAffinity()
    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {

    }

    override fun onCardSwiped(direction: Direction?) {
        Log.d("CardStackView", "onCardSwiped: p = ${manager.topPosition}, d = $direction")
        if (direction.toString() == ("Right")) {
            try {
                val hashMap = HashMap<String,String>()
                hashMap["swipe_id"]= userList[manager.topPosition - 1].id.toString()
                hashMap["type"]="right"
                mPresenter.swipeUser(hashMap)
            }catch (e:Exception){}
        }else{
            try {
                val hashMap = HashMap<String,String>()
                hashMap["swipe_id"]=userList[manager.topPosition - 1].id.toString()
                hashMap["type"]="left"
                mPresenter.swipeUser(hashMap)
            }catch (e:Exception){}
        }
    }

    override fun onCardRewound() {

    }

    override fun onCardCanceled() {

    }

    override fun onCardAppeared(view: View?, position: Int) {

    }

    override fun onCardDisappeared(view: View?, position: Int) {

    }

    override fun onClickProfile(position: Int) {

    }

    override fun openProfileDetails(position: Int,details:DataX) {
//        requireActivity().startActivity(Intent(mContext,SwipeUserProfileActivity::class.java)
//            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//            .putExtra("UserDetailsGet",Gson().toJson(details)))
    }
}