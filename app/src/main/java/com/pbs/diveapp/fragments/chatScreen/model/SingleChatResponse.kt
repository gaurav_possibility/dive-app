package com.pbs.diveapp.fragments.chatScreen.model

data class SingleChatResponse(
	val data: List<DataItemChat?>? = null,
	val success: Boolean? = null,
	val message: String? = null
)

data class DataItemChat(
    val imageName: String? = null,
    val chatId: String? = null,
    val userPic: String? = null,
    val messagetype: String? = null,
    val count: String? = null,
    val isBlocked: Boolean? = null,
    val type: String? = null,
    val message: String? = null,
    var isSeen: Int? = null,
    val userId: String? = null,
    val receivername: String? = null,
    val createdAt: String? = null,
    val receiverId: String? = null,
    val userCount: String? = null,
    val receiverPic: String? = null,
    val V: Int? = null,
    val receiverCount: String? = null,
    val id: String? = null,
    val username: String? = null,
    val updatedAt: String? = null,
    val usertype: String? = null
)

