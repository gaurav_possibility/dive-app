package com.pbs.diveapp.fragments.likeScreen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.likeScreen.model.Interest
import kotlinx.android.synthetic.main.item_list_interest_layout.view.*

class InterestListAdapter(val mContext: Context, val list: ArrayList<Interest>) :
    RecyclerView.Adapter<InterestListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InterestListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.item_list_interest_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: InterestListAdapter.MyViewHolder, position: Int) {
        holder.name.text = list[position].interest
    }

    override fun getItemCount() = list.size
    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val name = item.tvInterestListName!!
    }
}