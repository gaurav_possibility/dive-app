package com.pbs.diveapp.fragments.signUpFlow.ageScreen

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.sexualPreference.SexualPreferenceFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_age_screen.*
import java.util.*

/*
* This class is used as Age Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class AgeScreenFragment(val userName:String) : BaseFragment(), View.OnClickListener {


    lateinit var mContext: Context
    var selectedDate=""
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_age_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        allClicks()

    }

    private fun allClicks() {
        btnNextAge.setOnClickListener(this)
        layoutDate.setOnClickListener(this)
        etMonth.setOnClickListener(this)
        etDay.setOnClickListener(this)
        etYear.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextAge -> {
                if (etMonth.text.toString().trim().isEmpty() && etDay.text.toString().trim()
                        .isEmpty() && etYear.text.toString().trim().isEmpty()
                ) {
                    CommonMethods.showToast(mContext, "Please enter your birthday")
                } else {
                    CommonMethods.replaceFragmentScreenWithBackStack(
                        requireFragmentManager(),
                        SexualPreferenceFragment(userName,selectedDate),
                        R.id.splashContainer
                    )
                }
            }
            R.id.layoutDate -> {
                val c = Calendar.getInstance()
                c.add(Calendar.YEAR, -18)
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val time: Long = c.timeInMillis

                val dpd = DatePickerDialog(requireContext(), { _, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    etMonth.setText("" + (monthOfYear + 1))
                    etDay.setText(dayOfMonth.toString())
                    etYear.setText(year.toString())
                     selectedDate = "" + year + "-" + (monthOfYear + 1) + "-" +dayOfMonth
                    //yyyy-MM-dd

                }, year, month, day)
                dpd.datePicker.maxDate = time - 1000
                dpd.show()
            }
            R.id.etMonth -> {
                layoutDate.performClick()
            }
            R.id.etDay -> {
                layoutDate.performClick()
            }
            R.id.etYear -> {
                layoutDate.performClick()
            }
        }
    }
}