package com.pbs.strive.signup.presenter

interface SingUpPresenterHandler {
    fun signUp(request:HashMap<String,String>)
    fun updateInfo(auth:String,hashMap: HashMap<String,String>)
}