package com.pbs.diveapp.fragments.notification.view

import com.pbs.diveapp.fragments.chatScreen.model.MatchesListResponse
import com.pbs.diveapp.utils.BaseView

interface ChatScreenFragmentView:BaseView {
    fun onSuccessful(matchesListResponse: MatchesListResponse)
}