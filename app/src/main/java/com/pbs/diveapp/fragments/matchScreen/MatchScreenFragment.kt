package com.pbs.diveapp.fragments.matchScreen

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.ContainerActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.chatScreen.ChatScreenFragment
import com.pbs.diveapp.fragments.likeScreen.model.ProfileDetail
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.fragments.venueListScreen.VenuesListFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_match_screen.*
import kotlinx.android.synthetic.main.item_toolbar.*

class MatchScreenFragment(private val profileDetail: String) : BaseFragment(),
    View.OnClickListener {


    lateinit var proDetail: ProfileDetail
    lateinit var mContext: Context
    lateinit var myDetails: Data
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_match_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        allClicks()
        proDetail = Gson().fromJson(profileDetail, ProfileDetail::class.java)
        myDetails =
            Gson().fromJson(SharedPref.instance.getString(AppConstant.USER_DATA), Data::class.java)
        ivBackArrow.setColorFilter(Color.WHITE)
        tvNewMatch.text = "YOU'VE GOT A NEW MATCH!"
        CommonMethods.setProfileImage(mContext, proDetail.image, ivOtherProfileImage)
        tvOtherUserName.text = proDetail.name
        tvMyName.text = myDetails.name
        CommonMethods.setProfileImage(mContext, myDetails.image!!, ivMyProfileImage)

    }

    private fun allClicks() {
        ivBackArrow.setOnClickListener(this)
        btnSetUpDate.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                requireActivity().onBackPressed()
            }
            R.id.btnSetUpDate -> {
//                CommonMethods.replaceFragmentActivityWithoutStack(
//                    requireFragmentManager(),
//                    VenuesListFragment(proDetail.name, proDetail.email.toString(),proDetail.id.toString(),
//                    proDetail.image),
//                    R.id.fragmentContainer
//                )

                CommonMethods.replaceFragmentActivityWithoutStack(
                    requireFragmentManager(),
                    ChatScreenFragment(profileDetail),
                    R.id.fragmentContainer
                )

//                requireActivity().startActivity(
//                    Intent(mContext, ContainerActivity::class.java)
//                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                        .putExtra("FromScreens", "1")
//                        .putExtra("UserDetails", Gson().toJson(profileDetail))
//                )
            }
        }
    }
}