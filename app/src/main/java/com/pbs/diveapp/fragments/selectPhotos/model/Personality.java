
package com.pbs.diveapp.fragments.selectPhotos.model;


import com.google.gson.annotations.SerializedName;


public class Personality {

    @SerializedName("personality")
    private String mPersonality;

    public String getPersonality() {
        return mPersonality;
    }

    public void setPersonality(String personality) {
        mPersonality = personality;
    }

}
