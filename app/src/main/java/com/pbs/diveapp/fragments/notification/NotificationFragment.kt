package com.pbs.diveapp.fragments.notification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.ContainerActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.chatScreen.model.DataXMatch
import com.pbs.diveapp.fragments.chatScreen.model.MatchesListResponse
import com.pbs.diveapp.fragments.notification.presenter.ChatScreenFragmentPresenter
import com.pbs.diveapp.fragments.notification.view.ChatScreenFragmentView
import com.pbs.diveapp.fragments.notification.adapter.MatchItemClick
import com.pbs.diveapp.fragments.notification.adapter.MatchListAdapter
import com.pbs.diveapp.fragments.notification.adapter.NotificationClick
import com.pbs.diveapp.fragments.notification.adapter.NotificationListAdapter
import com.pbs.diveapp.fragments.notification.view.NotificationFragmentView
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : BaseFragment(), View.OnClickListener, NotificationFragmentView,
    MatchItemClick, NotificationClick, ChatScreenFragmentView {
    lateinit var mContext: Context
    lateinit var mAdapter: MatchListAdapter
    lateinit var mAdapterNotification: NotificationListAdapter
    lateinit var mPresenter: ChatScreenFragmentPresenter
    var matchList = ArrayList<DataXMatch>()
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_notification
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvMatch.isSelected = true
        allClicks()

        mPresenter = ChatScreenFragmentPresenter(this)


        val name = arrayListOf("Jessica Brown", "Brown")
        mAdapter = MatchListAdapter(mContext, matchList, this)
        rvMatchList.layoutManager = LinearLayoutManager(mContext)
        rvMatchList.adapter = mAdapter

        val notifi = arrayListOf("Jessica", "Monika", "Rima")
        mAdapterNotification = NotificationListAdapter(mContext, notifi, this)
        rvNotificationList.layoutManager = LinearLayoutManager(mContext)
        rvNotificationList.adapter = mAdapterNotification


    }

    private fun allClicks() {
        tvMatch.setOnClickListener(this)
        tvNotification.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        mPresenter.getMatchList()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvMatch -> {
                tvMatch.isSelected = true
                tvNotification.isSelected = false
                rvNotificationList.visibility = View.GONE
                rvMatchList.visibility = View.VISIBLE

            }
            R.id.tvNotification -> {
                tvMatch.isSelected = false
                tvNotification.isSelected = true
                rvNotificationList.visibility = View.VISIBLE
                rvMatchList.visibility = View.GONE

            }
        }
    }

    override fun onSuccessful(matchesListResponse: MatchesListResponse) {
        matchList.clear()
        matchList.addAll(matchesListResponse.data.data)
        mAdapter.notifyDataSetChanged()

    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }


    override fun onMatchClick(position: Int, details: DataXMatch) {

        requireActivity().startActivity(
            Intent(mContext, ContainerActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .putExtra("FromScreens", "1")
                .putExtra("UserDetails", Gson().toJson(details))
        )
    }

    override fun onNotClick(position: Int) {

    }
}