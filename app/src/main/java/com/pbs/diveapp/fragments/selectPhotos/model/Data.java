
package com.pbs.diveapp.fragments.selectPhotos.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Data {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("age")
    private String mAge;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("career")
    private String mCareer;
    @SerializedName("community")
    private String mCommunity;
    @SerializedName("companyname")
    private String mCompanyname;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("deviceId")
    private String mDeviceId;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("education")
    private String mEducation;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("firstname")
    private String mFirstname;
    @SerializedName("forgotPassword")
    private Boolean mForgotPassword;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("height")
    private String mHeight;
   // @SerializedName("images")
    //private List<com.dating.datingapp.fragments.loginScreen.LoginModel.Image> mImages;
    @SerializedName("isBlocked")
    private Boolean mIsBlocked;
    @SerializedName("isprofile")
    private Boolean mIsprofile;
    @SerializedName("languages")
    private List<Language> mLanguages;
    @SerializedName("lastname")
    private String mLastname;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("lookingfor")
    private String mLookingfor;
    @SerializedName("personalitys")
    private List<Personality> mPersonalitys;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("questions")
    private List<Question> mQuestions;
    @SerializedName("religion")
    private String mReligion;
    @SerializedName("sexual")
    private String mSexual;
    @SerializedName("socialId")
    private String mSocialId;
    @SerializedName("subscription")
    private String mSubscription;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("usertype")
    private String mUsertype;
    @SerializedName("__v")
    private Long m_V;
    @SerializedName("_id")
    private String m_id;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public String getCareer() {
        return mCareer;
    }

    public void setCareer(String career) {
        mCareer = career;
    }

    public String getCommunity() {
        return mCommunity;
    }

    public void setCommunity(String community) {
        mCommunity = community;
    }

    public String getCompanyname() {
        return mCompanyname;
    }

    public void setCompanyname(String companyname) {
        mCompanyname = companyname;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEducation() {
        return mEducation;
    }

    public void setEducation(String education) {
        mEducation = education;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFirstname() {
        return mFirstname;
    }

    public void setFirstname(String firstname) {
        mFirstname = firstname;
    }

    public Boolean getForgotPassword() {
        return mForgotPassword;
    }

    public void setForgotPassword(Boolean forgotPassword) {
        mForgotPassword = forgotPassword;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getHeight() {
        return mHeight;
    }

    public void setHeight(String height) {
        mHeight = height;
    }

//    public List<com.dating.datingapp.fragments.loginScreen.LoginModel.Image> getImages() {
//        return mImages;
//    }
//
//    public void setImages(List<com.dating.datingapp.fragments.loginScreen.LoginModel.Image> images) {
//        mImages = images;
//    }

    public Boolean getIsBlocked() {
        return mIsBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        mIsBlocked = isBlocked;
    }

    public Boolean getIsprofile() {
        return mIsprofile;
    }

    public void setIsprofile(Boolean isprofile) {
        mIsprofile = isprofile;
    }

    public List<Language> getLanguages() {
        return mLanguages;
    }

    public void setLanguages(List<Language> languages) {
        mLanguages = languages;
    }

    public String getLastname() {
        return mLastname;
    }

    public void setLastname(String lastname) {
        mLastname = lastname;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getLookingfor() {
        return mLookingfor;
    }

    public void setLookingfor(String lookingfor) {
        mLookingfor = lookingfor;
    }

    public List<Personality> getPersonalitys() {
        return mPersonalitys;
    }

    public void setPersonalitys(List<Personality> personalitys) {
        mPersonalitys = personalitys;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public List<Question> getQuestions() {
        return mQuestions;
    }

    public void setQuestions(List<Question> questions) {
        mQuestions = questions;
    }

    public String getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = religion;
    }

    public String getSexual() {
        return mSexual;
    }

    public void setSexual(String sexual) {
        mSexual = sexual;
    }

    public String getSocialId() {
        return mSocialId;
    }

    public void setSocialId(String socialId) {
        mSocialId = socialId;
    }

    public String getSubscription() {
        return mSubscription;
    }

    public void setSubscription(String subscription) {
        mSubscription = subscription;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

    public Long get_V() {
        return m_V;
    }

    public void set_V(Long _V) {
        m_V = _V;
    }

    public String get_id() {
        return m_id;
    }

    public void set_id(String _id) {
        m_id = _id;
    }

}
