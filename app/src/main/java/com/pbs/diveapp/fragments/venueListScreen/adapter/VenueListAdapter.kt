package com.pbs.diveapp.fragments.venueListScreen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.mapScreen.model.ResultRest
import kotlinx.android.synthetic.main.item_venue_layout.view.*

class VenueListAdapter(
    val mContext: Context,
    val list: ArrayList<ResultRest>,
    private val clickListener: ItemClick,
    private val fromScreen:Int
) :
    RecyclerView.Adapter<VenueListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VenueListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.item_venue_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: VenueListAdapter.MyViewHolder, position: Int) {
        holder.venueName.text = list[position].name
        holder.itemView.setOnClickListener {
            clickListener.onClick(position, list[position])
        }
        if (fromScreen==0){
            holder.tvSelect.text ="View"
        }
        holder.tvSelect.setOnClickListener {
            clickListener.onClickView(position,list[position])
        }
    }

    override fun getItemCount() = list.size

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val venueName: TextView = item.tvAddress
        val distance: TextView = item.tvDistance
        val tvSelect :TextView = item.tvSelect
    }
}

interface ItemClick {
    fun onClick(position: Int, item: ResultRest)
    fun onClickView(position: Int,item: ResultRest)
}