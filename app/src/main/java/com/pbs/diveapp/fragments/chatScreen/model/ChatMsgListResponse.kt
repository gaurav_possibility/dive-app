package com.pbs.diveapp.fragments.chatScreen.model

data class ChatMsgListResponse(
    val `data`: List<DataChat>,
    val message: String,
    val status: Int,
    val success: Boolean
)

data class DataChat(
    val chat_id: Int? = null,
    val created_at: String? = null,
    val `file`: String? = null,
    val file_extension: String? = null,
    val file_type: String? = null,
    val id: Int? = null,
    var is_seen: String? = null,
    val message: String? = null,
    val sent_by: Int? = null,
    val type: String? = null,
    val updated_at: String? = null,
    val sent_by_detail: SentDetail
)
data class SentDetail(val id:Int,val name:String,val image:String?=null)