package com.pbs.diveapp.fragments.mapScreen.model

data class GetRestaurantResponse(
    val html_attributions: List<Any>,
    val next_page_token: String,
    val results: List<ResultRest>,
    val status: String
)