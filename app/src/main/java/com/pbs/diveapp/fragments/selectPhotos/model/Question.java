
package com.pbs.diveapp.fragments.selectPhotos.model;


import com.google.gson.annotations.SerializedName;


public class Question {

    @SerializedName("answer")
    private String mAnswer;
    @SerializedName("question")
    private String mQuestion;

    public String getAnswer() {
        return mAnswer;
    }

    public void setAnswer(String answer) {
        mAnswer = answer;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public void setQuestion(String question) {
        mQuestion = question;
    }

}
