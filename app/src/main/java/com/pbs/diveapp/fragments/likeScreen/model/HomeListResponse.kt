package com.pbs.diveapp.fragments.likeScreen.model

data class HomeListResponse(
    val `data`: Data,
    val message: String,
    val status: Int,
    val success: Boolean
)

data class Link(
    val active: Boolean,
    val label: String,
    val url: Any
)
data class DataX(
    val account_status: String,
    val age: String,
    val apple_id: Any,
    val bio: String,
    val country_code: String,
    val created_at: String,
    val deleted_at: Any,
    val device_token: String,
    val device_type: String,
    val distance: Int,
    val dob: Any,
    val education: String?=null,
    val email: String,
    val email_verified_at: Any,
    val facebook_id: Any,
    val gender: String,
    val height: String,
    val here_for: String?=null,
    val hometown: String?=null,
    val id: Int,
    val image: String,
    val interests: List<Interest>,
    val user_images: List<Images>,
    val is_number_verified: String,
    val is_profile_completed: String,
    val lat: String,
    val lng: String,
    val looking_for: String? = null,
    val mobile: String,
    val name: String? = null,
    val updated_at: String,
    val user_type: String
)
data class Data(
    val current_page: Int,
    val `data`: List<DataX>,
    val first_page_url: String,
    val from: Int,
    val last_page: Int,
    val last_page_url: String,
    val links: List<Link>,
    val next_page_url: Any,
    val path: String,
    val per_page: Int,
    val prev_page_url: Any,
    val to: Int,
    val total: Int
)

data class Interest(
    val created_at: String,
    val id: Int,
    val interest: String,
    val updated_at: String,
    val user_id: Int
)
data class Images(
    val created_at: String,
    val id: Int,
    val updated_at: String,
    val user_id: Int,
    val file: String,
    val file_type: String
)