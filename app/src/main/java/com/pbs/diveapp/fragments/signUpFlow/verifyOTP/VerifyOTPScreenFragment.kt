package com.pbs.diveapp.fragments.signUpFlow.verifyOTP

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.dating.datingapp.fragments.selectPhotos.SelectPhotosFragment
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.MainActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.presenter.PhoneVerifyScreenFragmentPresenter
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.view.PhoneVerifyScreenFragmentView
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.fragments.signUpFlow.userName.NameFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_phone_verify_screen.*
import kotlinx.android.synthetic.main.fragment_verify_o_t_p_screen.*

class VerifyOTPScreenFragment(
    var otp: String,
    var phone: String,
    var validateNumber: Boolean,
    var withNUmber: String
) :
    BaseFragment(),
    View.OnClickListener,
    PhoneVerifyScreenFragmentView {

    lateinit var mContext: Context
    lateinit var mPresenter: PhoneVerifyScreenFragmentPresenter


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_verify_o_t_p_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = PhoneVerifyScreenFragmentPresenter(this)

        etFirstOtp.setText(otp[0].toString())
        etSecondOtp.setText(otp[1].toString())
        etThirdOtp.setText(otp[2].toString())
        etFourthOtp.setText(otp[3].toString())
        allClicks()
    }

    private fun allClicks() {
        btnNextOTP.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextOTP -> {
                val hashMap = HashMap<String, String>()
                if (withNUmber == "withNumber") {
                    hashMap["country_code"] = "+91"
                    hashMap["mobile"] = phone
                    hashMap["otp"] =
                        "" + etFirstOtp.text.trim() + etSecondOtp.text.trim() + etThirdOtp.text.trim() + etFourthOtp.text.trim()

                } else {
                    hashMap["country_code"] = "+91"
                    hashMap["mobile"] = phone
                    hashMap["user_id"] =
                        SharedPref.instance.getString(AppConstant.USER_ID).toString()
                    hashMap["otp"] =
                        "" + etFirstOtp.text.trim() + etSecondOtp.text.trim() + etThirdOtp.text.trim() + etFourthOtp.text.trim()

                }
                mPresenter.verifyOTP(hashMap)
            }
        }
    }

    override fun onSuccessfulUpdateProfile(signUpResponse: LoginResponse) {

    }

    override fun onSuccessfullySendOTP(signUpResponse: LoginResponse) {

    }

    override fun onSuccessfullyVerifyOTP(signUpResponse: SignUpResponse) {

        if (signUpResponse.success) {

            if (!validateNumber && withNUmber != "withNumber") {

                CommonMethods.replaceFragmentActivityWithoutStack(
                    requireFragmentManager(),
                    SelectPhotosFragment(),
                    R.id.splashContainer
                )

//                requireActivity().startActivity(
//                    Intent(mContext, MainActivity::class.java)
//                        .setFlags(
//                            Intent.FLAG_ACTIVITY_SINGLE_TOP or
//                                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
//                        )
//                )
             //   requireActivity().finishAffinity()
            } else if (validateNumber && withNUmber == "withNumber") {

                SharedPref.instance.setString(AppConstant.ACCESS_TOKEN, signUpResponse.accessToken)
                SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(signUpResponse.data))
                SharedPref.instance.setString(AppConstant.USER_ID, signUpResponse.data._id.toString())
                SharedPref.instance.setBoolean(AppConstant.USER_LOGGED_IN, true)

                requireActivity().startActivity(
                    Intent(mContext, MainActivity::class.java)
                        .setFlags(
                            Intent.FLAG_ACTIVITY_SINGLE_TOP or
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        )
                )
                requireActivity().finishAffinity()
            } else if (!validateNumber && withNUmber == "withNumber") {
                SharedPref.instance.setString(AppConstant.ACCESS_TOKEN, signUpResponse.accessToken)
                SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(signUpResponse.data))
                SharedPref.instance.setString(AppConstant.USER_ID, signUpResponse.data._id.toString())
                SharedPref.instance.setBoolean(AppConstant.USER_LOGGED_IN, true)
                CommonMethods.replaceFragmentActivityWithoutStack(
                    requireFragmentManager(),
                    NameFragment(),
                    R.id.splashContainer
                )
            }
        }
    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }

}