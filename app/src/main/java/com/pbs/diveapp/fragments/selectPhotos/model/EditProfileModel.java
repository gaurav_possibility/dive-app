
package com.pbs.diveapp.fragments.selectPhotos.model;


import com.google.gson.annotations.SerializedName;

public class EditProfileModel {

    @SerializedName("data")
    private DataEditProfile mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("success")
    private Boolean mSuccess;

    public DataEditProfile getData() {
        return mData;
    }

    public void setData(DataEditProfile data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(Boolean success) {
        mSuccess = success;
    }

}
