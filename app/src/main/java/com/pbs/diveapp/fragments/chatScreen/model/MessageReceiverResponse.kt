package com.pbs.diveapp.fragments.chatScreen.model

import com.pbs.diveapp.fragments.chatScreen.view.*

data class MessageReceiverResponse(
    val created_at: String,
    val group_id: Any,
    val id: Int,
    val last_message: LastMessage,
    val receiver_id: Int,
    val sender_detail: SenderDetail,
    val sender_id: Int,
    val unseen_message_count: Int,
    val updated_at: String
)

data class LastMessage(
    val chat_id: Int,
    val created_at: String,
    val `file`: String,
    val file_extension: String,
    val file_type: String,
    val id: Int,
    val is_seen: String,
    val message: String,
    val sent_by: Int,
    val sent_by_detail: SentByDetail,
    val type: String,
    val updated_at: String
)
data class SenderDetail(
    val id: Int,
    val image: String,
    val interests: List<Any>,
    val name: String,
)

data class SentByDetail(
    val id: Int,
    val image: String,
    val interests: List<Any>,
    val name: String
)