package com.pbs.diveapp.fragments.signUpFlow.welcomeScreen

import android.content.Context
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginScreenFragment
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.SignUpScreenFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_welcome_screen.*

/*
* This class is used as Choose SignUp or Login Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class WelcomeScreenFragment : BaseFragment(), View.OnClickListener {

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_welcome_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        allClicks()
        
    }

    private fun allClicks() {
        btnJoinDive.setOnClickListener(this)
        btnLogIn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnJoinDive -> {
                CommonMethods.replaceFragmentScreenWithBackStack(
                    requireFragmentManager(),
                    SignUpScreenFragment(),
                    R.id.splashContainer
                )
            }
            R.id.btnLogIn -> {
                CommonMethods.replaceFragmentScreenWithBackStack(requireFragmentManager(),
                LoginScreenFragment(),R.id.splashContainer)
//                requireActivity().startActivity(
//                    Intent(mContext, MainActivity::class.java)
//                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                )
//                requireActivity().finishAffinity()
            }
        }
    }
}