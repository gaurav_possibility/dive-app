package com.dating.datingapp.fragments.selectPhotos

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.ImageViewTarget
import com.dating.datingapp.activities.editProfile.adapterProfile.AdapterProfileEdit
import com.dating.datingapp.activities.editProfile.adapterProfile.addImage
import com.dating.datingapp.activities.editProfile.adapterProfile.editImage
import com.dating.datingapp.activities.editProfile.adapterProfile.removeImage
import com.dating.datingapp.webServices.handler.loginHandler
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.MainActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.selectPhotos.model.EditModel
import com.pbs.diveapp.fragments.selectPhotos.model.SelectPhotoResponse
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.AddProfileHandler
import com.pbs.diveapp.webServices.handler.BasicInformationHandler
import kotlinx.android.synthetic.main.fragment_select_photos.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/*
* This class is used as Add Photos screen for this app
*
* created by GTB on 02/04/2021
*/
class SelectPhotosFragment : BaseFragment(), addImage, removeImage, editImage,
    View.OnClickListener {
    lateinit var mContext: Context
    override var mImageFile: File? = null
    private val PICK_IMAGE_CAMERA = 1
    private val PICK_IMAGE_GALLERY = 2
    lateinit var productImageFile: File
    lateinit var userDetails: Data

    // lateinit var userData: Data
    var ALL_PERMISSIONS = 22

    //hell
    val imageList = ArrayList<String>()
    lateinit var list: MutableList<EditModel>
    lateinit var adapter: AdapterProfileEdit
    lateinit var businessId: RequestBody
    lateinit var alertDialog: android.app.AlertDialog

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_select_photos
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        businessId = RequestBody.create(
            "text/plain".toMediaTypeOrNull(),
            SharedPref.instance.getString(AppConstant.USER_ID).toString()
        )
        btnNextPhoto.setOnClickListener(this)
        layoutSelectImage.setOnClickListener(this)
        image_layout_second.setOnClickListener(this)
        ivAddFirst.setOnClickListener(this)
        ivAddSecond.setOnClickListener(this)
        ivAddThird.setOnClickListener(this)
        ivAddFourth.setOnClickListener(this)
        list = mutableListOf()

//        try {
//            userDetails = Gson().fromJson(
//                SharedPref.instance.getString(AppConstant.USER_DATA),
//                Data::class.java
//            )
//        } catch (e: java.lang.Exception) {
//        }

        list.add(EditModel(getDrawable(mContext, R.drawable.ic_cross_edit).toString(), imageList))
        list.add(EditModel(getDrawable(mContext, R.drawable.image_bg).toString(), imageList))
        list.add(EditModel(getDrawable(mContext, R.drawable.image_bg).toString(), imageList))
        list.add(EditModel(getDrawable(mContext, R.drawable.image_bg).toString(), imageList))

        adapter = AdapterProfileEdit(mContext, list, this, this, this)
        rec_edit.adapter = adapter

        if (SharedPref.instance.getString("personPhoto")?.startsWith("http")!!) {
//            Log.e("personPhoto", SharedPref.getInstance(mContext).getString("personPhoto"))
//
//            //  val url = URL(SharedPref.getInstance(mContext).getString("personPhoto"))
//            //   val image = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            imageList.add(SharedPref.instance.getString("personPhoto").toString())
//
            val gallery1
                    : MutableList<MultipartBody.Part?> =
                java.util.ArrayList()

            if (SharedPref.instance.getString("personPhoto")!!.isNotEmpty()) {
                var fileGallery1: File? = null
                fileGallery1 = File(SharedPref.instance.getString("personPhoto"))
                gallery1.add(prepareFilePart("image[]", fileGallery1))
                //   hitImagewApi(businessId,gallery1)
                //  hitImageApi(businessId, gallery1)
            }
            setImages()

        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextPhoto -> {
//                selectImagesCameraPermission()
                if (imageList.size < 2) {
                    CommonMethods.showToast(mContext, "Please add minimum two images")
                } else {

                    startActivity(
                        Intent(mContext, MainActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                }
            }
            R.id.layoutSelectImage -> {
                if (imageList.size < 4) {
                    selectImagesCameraPermission()
                }
            }
            R.id.image_layout_second -> {
                if (imageList.size < 4) {
                    selectImagesCameraPermission()
                }
            }
            R.id.ivAddFirst -> {
                if (ivAddFirst.drawable.constantState!! == mContext.resources.getDrawable(R.drawable.ic_cross_edit).constantState) {
                    removeImage(0)
                }
            }
            R.id.ivAddSecond -> {
                if (ivAddSecond.drawable.constantState!! == mContext.resources.getDrawable(R.drawable.ic_cross_edit).constantState) {
                    removeImage(1)
                }
            }
            R.id.ivAddThird -> {
                if (ivAddThird.drawable.constantState!! == mContext.resources.getDrawable(R.drawable.ic_cross_edit).constantState) {
                    removeImage(2)
                }
            }
            R.id.ivAddFourth -> {
                if (ivAddFourth.drawable.constantState!! == mContext.resources.getDrawable(R.drawable.ic_cross_edit).constantState) {
                    removeImage(3)
                }
            }
        }
    }

    fun selectImagesCameraPermission() {
        if (CommonMethods.isReadStorageGranted(mContext)
        ) {
            Log.e("qwer234", " click1 ")
            if (CommonMethods.isCameraGranted(mContext)) {
                selectImage()
                Log.e("qwer234", " click2 ")
            } else {
                val permissions =
                    arrayOf(Manifest.permission.CAMERA)
                request_Permissions(activity, 2, *permissions)
                Log.e("qwer234", " click3 ")
            }
        } else {
            val permissions = arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            request_Permissions(activity, 1, *permissions)
            Log.e("qwer234", " click4 ")
        }
    }

    fun request_Permissions(
        activity: Activity?,
        permissionId: Int,
        vararg permissions: String?
    ) {
        requestPermissions(permissions, permissionId)
    }

    private fun selectImage() {

        try {

            val builder: AlertDialog.Builder = AlertDialog.Builder(mContext)
            builder.setCancelable(false)
            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            @SuppressLint("InflateParams")
            val view: View = inflater.inflate(R.layout.addmedia, null)
            builder.setView(view)
            builder.setCancelable(true)
            val formPopup = builder.create()
            formPopup.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            val yesTV = view.findViewById<TextView>(R.id.yesTV)
//            val noTV = view.findViewById<TextView>(R.id.noTV)
            val cross_image = view.findViewById<ImageView>(R.id.cross_image)
            val camera = view.findViewById<ImageView>(R.id.camera)
            val gallery = view.findViewById<ImageView>(R.id.gallery)
//            yesTV.setOnClickListener {
//                formPopup.dismiss()
            cross_image.setOnClickListener(View.OnClickListener {
                formPopup.dismiss()
            })

            camera.setOnClickListener(View.OnClickListener {
                formPopup.dismiss()
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                mImageFile = CommonMethods.createImageFile(mContext)
                cameraIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    FileProvider.getUriForFile(
                        mContext,
                        mContext.packageName + ".provider",
                        mImageFile!!
                    )
                )
                startActivityForResult(cameraIntent, PICK_IMAGE_CAMERA)

            })
            gallery.setOnClickListener(View.OnClickListener {
                formPopup.dismiss()
                val pickPhoto = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)

            })


            // when (view.id) {
//
//                R.id.camera -> {
//                    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//                    mImageFile = CommonMethods.createImageFile(this)
//                    cameraIntent.putExtra(
//                        MediaStore.EXTRA_OUTPUT,
//                        FileProvider.getUriForFile(
//                            this,
//                            this.packageName + ".provider",
//                            mImageFile!!
//                        )
//                    )
//                    startActivityForResult(cameraIntent, PICK_IMAGE_CAMERA)
//
////                            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
////                            startActivityForResult(intent, PICK_IMAGE_CAMERA)
//                }
//                R.id.gallery -> {
//                    formPopup.dismiss()
//                    val pickPhoto = Intent(
//                        Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
//                    )
//                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)
//                }
//
//                R.id.cross_image -> {
//                    formPopup.dismiss()
//                }
//
//
//                R.id.prompt -> {
//                    formPopup.dismiss()
//                }
//                else -> {
//                    requestPermission()
//                    Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
//                }

            //       }

            formPopup.show()
            alertDialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                    noTV.setOnClickListener {
//
//                        formPopup.dismiss()
//                    }


            //        }
//        val pm: PackageManager = getPackageManager()
//            val hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName())
//            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
//                val options =
//                    arrayOf<CharSequence>("Take Photo", "Choose From Gallery", "Cancel")
//                val builder =
//                    AlertDialog.Builder(this)
//                builder.setTitle("Select Option")
//                builder.setItems(
//                    options
            //    )


//            }else  {
//
//            }
        } catch (e: Exception) {
            //   Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
//        try {
//            val pm: PackageManager = context!!.getPackageManager()
//            val hasPerm = pm.checkPermission(Manifest.permission.CAMERA, context!!.getPackageName())
//            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
//                val options =
//                    arrayOf<CharSequence>("Take Photo", "Choose From Gallery", "Cancel")
//                val builder =
//                    AlertDialog.Builder(context!!)
//                builder.setTitle("Select Option")
//                builder.setItems(
//                    options
//                ) { dialog: DialogInterface, item: Int ->
//                    when {
//                        options[item] == "Take Photo" -> {
//                            dialog.dismiss()
//
//                            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//                            mImageFile = CommonMethods.createImageFile(mContext)
//                            cameraIntent.putExtra(
//                                MediaStore.EXTRA_OUTPUT,
//                                FileProvider.getUriForFile(
//                                    mContext,
//                                    mContext.packageName + ".provider",
//                                    mImageFile!!
//                                )
//                            )
//                            startActivityForResult(cameraIntent, PICK_IMAGE_CAMERA)
//
////                            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
////                            startActivityForResult(intent, PICK_IMAGE_CAMERA)
//                        }
//                        options[item] == "Choose From Gallery" -> {
//                            dialog.dismiss()
//                            val pickPhoto = Intent(
//                                Intent.ACTION_PICK,
//                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
//                            )
//                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)
//                        }
//                        options[item] == "Cancel" -> {
//                            dialog.dismiss()
//                        }
//                    }
//                }
//                builder.show()
//            } else requestPermission()
//            // Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
//        } catch (e: Exception) {
//            Toast.makeText(context!!, "Camera Permission error", Toast.LENGTH_SHORT).show()
//            e.printStackTrace()
//        }
    }

    private fun requestPermission() {
        val permissions = arrayOf(Manifest.permission.CAMERA)
//        ActivityCompat.requestPermissions(activity!!, permissions, ALL_PERMISSIONS)
        requestPermissions(permissions, ALL_PERMISSIONS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE_CAMERA) {
                try {
                    val selectedImage = mImageFile!!.toUri()

                    val bitmap1 = BitmapFactory.decodeFile(mImageFile!!.path)
                    val newBitMap: Bitmap =
                        CommonMethods.changeImageOrientation(mImageFile!!.path, bitmap1!!)!!
                    val resizedBitMap: Bitmap =
                        CommonMethods.getResizedBitmap(newBitMap, 900, 900)!!

                    val imgPath = if (Build.VERSION.SDK_INT != Build.VERSION_CODES.M) {
                        val newImage1: File = CommonMethods.createFileFromBitMap(resizedBitMap)!!
                        newImage1.path
                    } else {
                        mImageFile!!.path
                    }
//                    val imgPath = if (Build.VERSION.SDK_INT != Build.VERSION_CODES.R) {
//                        val newImage1: File = CommonMethods.createFileFromBitMap(resizedBitMap)!!
//                        newImage1.path
//                    } else {
//                        mImageFile!!.path
//                    }
//                    ivFirstImage.setImageBitmap(resizedBitMap)
                    imageList.add(imgPath)

                    val gallery1
                            : MutableList<MultipartBody.Part?> =
                        java.util.ArrayList()

                    if (imgPath.isNotEmpty()) {
                        var fileGallery1: File? = null
                        fileGallery1 = File(imgPath)
                        gallery1.add(prepareFilePart("image[]", fileGallery1))
                        //   hitImagewApi(businessId,gallery1)
                        hitImageApi(businessId, gallery1)
                    }


//                   adapter.notifyDataSetChanged()
                    setImages()
                } catch (e: java.lang.Exception) {
                    Log.e("errorPrint", e.localizedMessage)
                }
            } else if (requestCode == PICK_IMAGE_GALLERY && data != null) {
                val selectedImage = data.data
                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(
                            mContext.getContentResolver(),
                            selectedImage
                        )
                    val bytes = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    Log.e("Activity", "Pick from Gallery::>>> ")
                    val newBitMap: Bitmap = CommonMethods.changeImageOrientation(
                        CommonMethods.getRealPathFromUri(
                            requireActivity(),
                            selectedImage
                        )!!.absolutePath,
                        bitmap!!
                    )!!
                    var imgPath = CommonMethods.compressImage(
                        CommonMethods.getRealPathFromUri(
                            requireActivity(),
                            selectedImage
                        )!!.absolutePath
                    ).toString()

                    val bitmap1 = BitmapFactory.decodeFile(imgPath)
                    val newBitMap1: Bitmap? = CommonMethods.changeImageOrientation(imgPath, bitmap1)
                    val resizedBitMap: Bitmap? =
                        CommonMethods.getResizedBitmap(newBitMap1, 900, 900)
                    val newImage: File? = CommonMethods.createFileFromBitMap(resizedBitMap!!)
                    imgPath = newImage!!.absolutePath

                    imageList.add(imgPath)

                    val gallery1
                            : MutableList<MultipartBody.Part?> =
                        java.util.ArrayList()

                    if (imgPath.isNotEmpty()) {
                        var fileGallery1: File? = null
                        fileGallery1 = File(imgPath)
                        gallery1.add(prepareFilePart("image[]", fileGallery1))
                        //   hitImagewApi(businessId,gallery1)
                        hitImageApi(businessId, gallery1)
                    }
//                    adapter.notifyDataSetChanged()
                    setImages()
//                    ivFirstImage.setImageBitmap(newBitMap)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                Log.e("TAGFragment", "External storage2")
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(
                        "TAGFragment",
                        "Permission: " + permissions[0] + "was " + grantResults[0]
                    )
                    //resume tasks needing this permission
                    selectImagesCameraPermission()
                    //     selectImage()
                } else {
                    Log.e("TAGFragment", "External storage2 Deny ")

                    // permission was not granted
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                    ) {
                        selectImagesCameraPermission()
                    } else {
                        val snackbar: Snackbar = Snackbar.make(
                            root_layout,
                            resources.getString(R.string.message_text),
                            Snackbar.LENGTH_LONG
                        )
                        snackbar.setAction(
                            resources.getString(R.string.settings),
                            View.OnClickListener {
                                if (mContext == null) {
                                    return@OnClickListener
                                }
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri: Uri = Uri.fromParts(
                                    "package",
                                    mContext.packageName,
                                    null
                                )
                                intent.data = uri
                                this.startActivity(intent)
                            })
                        snackbar.show()
                    }
                }
            }
            2 -> {
                Log.d("TAGFragment", "Camera ")
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(
                        "TAGFragment",
                        "Permission: " + permissions[0] + "was " + grantResults[0]
                    )
                    //resume tasks needing this permission
                    selectImagesCameraPermission()
                    //  selectImage()
                } else {
                    Log.e("TAGFragment", "External storage2 Deny ")

                    // permission was not granted
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            Manifest.permission.CAMERA
                        )
                    ) {
                        selectImagesCameraPermission()
                    } else {
                        val snackbar: Snackbar = Snackbar.make(
                            root_layout,
                            resources.getString(R.string.message_text),
                            Snackbar.LENGTH_LONG
                        )
                        snackbar.setAction(
                            resources.getString(R.string.settings),
                            View.OnClickListener {
                                if (mContext == null) {
                                    return@OnClickListener
                                }
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri: Uri = Uri.fromParts(
                                    "package",
                                    mContext.packageName,
                                    null
                                )
                                intent.data = uri
                                this.startActivity(intent)
                            })
                        snackbar.show()
                    }
                }
            }
        }
    }

    fun setImages() {
        val imageViewList = arrayListOf(ivFirstImage, ivSecondImage, ivThirdImage, ivFourthImage)
        val addImageList = arrayListOf(ivAddFirst, ivAddSecond, ivAddThird, ivAddFourth)
        if (imageList.isEmpty().not()) {
            for (i in imageList.indices) {
                try {
                    if (imageList[i].startsWith("http")) {
//                        val bitmap = BitmapFactory.decodeFile(imageList[i])
//                        val newBitMap: Bitmap = CommonMethods.changeImageOrientation(imageList[i], bitmap)!!
//                        val resizedBitMap: Bitmap = CommonMethods.getResizedBitmap(newBitMap, 1200, 1200)!!
                        Glide.with(this).asDrawable()
                            .load(imageList[i])
                            .into(imageViewList[i])

                    } else {
                        Glide.with(this).asDrawable()
                            .load(imageList[i])
                            .into(object :
                                ImageViewTarget<Drawable?>(imageViewList[i]) {
                                override fun setResource(resource: Drawable?) {
                                    imageViewList[i].setImageDrawable(resource) //This is important

                                }
                            })
                    }

                    addImageList[i].setImageDrawable(this.resources.getDrawable(R.drawable.ic_cross_edit))
                } catch (e: java.lang.Exception) {

                }
            }
        }
    }

    fun removeImage(position: Int) {
        imageList.removeAt(position)
        val imageViewList =
            arrayListOf(ivFirstImage, ivSecondImage, ivThirdImage, ivFourthImage)
        val addImageList = arrayListOf(ivAddFirst, ivAddSecond, ivAddThird, ivAddFourth)
        for (i in imageViewList.indices) {
            imageViewList[i].setImageDrawable(null)
            addImageList[i].setImageDrawable(mContext.resources.getDrawable(R.drawable.add))
        }

        deleteImage(userDetails.user_images[position].id.toString())
        setImages()

    }

    override fun addImages(position: Int) {
        selectImagesCameraPermission()
    }

    override fun removeImages(position: Int) {
        imageList.removeAt(position)
        adapter.notifyDataSetChanged()
    }

    private fun prepareFilePart(
        partName: String,
        file: File
    ): MultipartBody.Part? {
        //"image/jpeg"
        //"image/bmp"
        //"image/gif"
        //"image/jpg"
        //"image/png"
        //"image/*"
        val requestFile: RequestBody =
            RequestBody.create("image/jpeg".toMediaTypeOrNull(), file)
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }

    fun hitImageApi(
        userId: RequestBody,
        image: MutableList<MultipartBody.Part?>
    ) {
        showBaseProgressDialog(mContext, "Please wait")
        WebService.mInstance.imageEdit(
            userId = userId,
            image = image,

            handler = object : AddProfileHandler {
                override fun onSuccessFully(response: SelectPhotoResponse) {
                    if (response.success!!) {

                        hideBaseProgressDialog()
                        SharedPref.instance.setString("islogin", "1")
                        SharedPref.instance.setString("loginsuccess", "345")
                        getUserDetails()
                        //  SharedPref.instance.setString(AppConstant.USER_ID, response.data.id)
//                        SharedPref.instance.setString(
//                            AppConstant.USER_DATA,
//                            Gson().toJson(response.data)
//                        )
//                        startActivity(
//                            Intent(mContext, MainActivity::class.java)
//                                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                        )
                    }

                }

                override fun onError(message: String?) {
                    hideBaseProgressDialog()
                    CommonMethods.showToast(mContext, message.toString())
                    Log.e("data", message.toString())
                }

//                override fun onServerError(message: String?) {
//                    hideBaseProgressDialog()
//                    CommonMethods.showToast(mContext, message.toString())
//                    Log.e("data", message.toString())
//                }

            })
    }

    fun deleteImage(id: String) {
        showBaseProgressDialog(mContext, "")
        var list=ArrayList<String>()
        list.add(id)
        val hash = HashMap<String, String>()
        hash["image_id"] = Gson().toJson(list)

        WebService.mInstance.deleteImages(hash, object : BasicInformationHandler {

            override fun onSuccess(response: LoginResponse) {
                CommonMethods.showToast(mContext, response.message!!)
                hideBaseProgressDialog()
            }

            override fun onError(message: String?) {
                hideBaseProgressDialog()
                CommonMethods.showToast(mContext, message!!)
            }

        })
    }

    override fun editImage(position: Int) {

    }

    fun getUserDetails() {

        WebService.mInstance.getUserDetails(handler = object : loginHandler {
            override fun onSuccess(response: LoginResponse) {
                userDetails = response.data

            }

            override fun onError(message: String?) {

            }
        })
    }
}