package com.pbs.diveapp.fragments.notification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_notification_layout.view.*

class NotificationListAdapter(
    val mContext: Context,
    private val notificationList: ArrayList<String>,
    private val click: NotificationClick
) :
    RecyclerView.Adapter<NotificationListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationListAdapter.MyViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_notification_layout, parent, false)
        return MyViewHolder(view)


    }

    override fun onBindViewHolder(holder: NotificationListAdapter.MyViewHolder, position: Int) {
        holder.titleNoti.text = notificationList[position]
        holder.itemView.setOnClickListener {
            click.onNotClick(position)
        }

    }
    override fun getItemCount() = notificationList.size

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val titleNoti: TextView = item.tvNotificationTitle
        val notificationDetails: TextView = item.tvNotiDetails
        val notificationTime: TextView = item.tvNotiTime
    }
}

interface NotificationClick {
    fun onNotClick(position: Int)
}