package com.pbs.diveapp.fragments.chatScreen.view

import com.pbs.diveapp.fragments.chatScreen.model.ChatMsgListResponse
import com.pbs.diveapp.utils.BaseView

interface MessageScreenFragmentView:BaseView {
    fun onSuccessful(chatMsgListResponse: ChatMsgListResponse)
}