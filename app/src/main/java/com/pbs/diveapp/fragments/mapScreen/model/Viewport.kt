package com.pbs.diveapp.fragments.mapScreen.model

data class Viewport(
    val northeast: Northeast,
    val southwest: Southwest
)