package com.pbs.diveapp.fragments.mapScreen.model

data class PlusCode(
    val compound_code: String,
    val global_code: String
)