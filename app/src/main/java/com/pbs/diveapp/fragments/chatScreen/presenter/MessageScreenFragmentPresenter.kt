package com.pbs.diveapp.fragments.chatScreen.presenter

import com.pbs.diveapp.fragments.chatScreen.model.ChatMsgListResponse
import com.pbs.diveapp.fragments.chatScreen.view.MessageScreenFragmentView
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.ChatMsgListHandler

class MessageScreenFragmentPresenter(val view:MessageScreenFragmentView):MessageScreenFragmentHandler {
    override fun getMessageList(hashMap:String) {
        view.showProgressBar()
        WebService.mInstance.getChatMsgList(hashMap,object :ChatMsgListHandler{
            override fun onSuccessful(chatMsgListResponse: ChatMsgListResponse) {
              view.hideProgressBar()
                view.onSuccessful(chatMsgListResponse)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }
}