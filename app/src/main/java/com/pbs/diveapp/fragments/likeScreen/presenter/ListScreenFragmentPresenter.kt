package com.pbs.diveapp.fragments.likeScreen.presenter

import com.dating.datingapp.webServices.handler.signupHandler
import com.pbs.diveapp.fragments.likeScreen.model.HomeListResponse
import com.pbs.diveapp.fragments.likeScreen.model.SwippeResponse
import com.pbs.diveapp.fragments.likeScreen.view.ListScreenFragmentView
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.HomeListHandler
import com.pbs.diveapp.webServices.handler.SwipeHandler

class ListScreenFragmentPresenter(val view: ListScreenFragmentView) : ListScreenFragmentHandler {
    override fun getUserList() {
        view.showProgressBar()
        WebService.mInstance.userList(object : HomeListHandler {
            override fun onSuccessful(homeListResponse: HomeListResponse) {
                view.hideProgressBar()
                view.onSuccessful(homeListResponse)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }

    override fun swipeUser(hashMap: HashMap<String, String>) {
//        view.showProgressBar()
        WebService.mInstance.swipe(hashMap, object : SwipeHandler {
            override fun onSuccessful(swippeResponse: SwippeResponse) {
                view.hideProgressBar()
                view.onSuccessfulSwipe(swippeResponse)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }

    override fun logout() {
        view.showProgressBar()
        WebService.mInstance.logout(object:signupHandler {
            override fun onSuccess(response: LoginResponse) {
               view.hideProgressBar()
                view.onSuccessfulLogout(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }
}