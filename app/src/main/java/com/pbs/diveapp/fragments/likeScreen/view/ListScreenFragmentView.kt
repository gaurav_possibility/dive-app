package com.pbs.diveapp.fragments.likeScreen.view

import com.pbs.diveapp.fragments.likeScreen.model.HomeListResponse
import com.pbs.diveapp.fragments.likeScreen.model.SwippeResponse
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.utils.BaseView

interface ListScreenFragmentView:BaseView {
    fun onSuccessful(homeListResponse: HomeListResponse)
    fun onSuccessfulSwipe(swippeResponse: SwippeResponse)
    fun onSuccessfulLogout(loginResponse: LoginResponse)
}