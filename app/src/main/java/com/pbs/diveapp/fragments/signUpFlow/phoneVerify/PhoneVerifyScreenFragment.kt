package com.pbs.diveapp.fragments.signUpFlow.phoneVerify

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.facebook.share.Share
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.MainActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.presenter.PhoneVerifyScreenFragmentPresenter
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.view.PhoneVerifyScreenFragmentView
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.fragments.signUpFlow.verifyOTP.VerifyOTPScreenFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_phone_verify_screen.*

/*
* This class is used as Phone Verify Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class PhoneVerifyScreenFragment(
    var userName: String,
    var userAge: String,
    var sexualOrientation: String,
    var whyRuHere: String,
    var userHeight: String,
    var userEdutcation: String,
    var userHomeTown: String,
    var intersetList: ArrayList<String>,
    var withNumber: String
) : BaseFragment(), View.OnClickListener,
    PhoneVerifyScreenFragmentView {

    lateinit var mPresenter: PhoneVerifyScreenFragmentPresenter
    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_phone_verify_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        allClicks()
        mPresenter = PhoneVerifyScreenFragmentPresenter(this)

        if (withNumber == "withNumber") {
            tvcountPhone.visibility =View.GONE
        }
    }

    private fun allClicks() {
        btnNextPhone.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextPhone -> {
                val hashMap = HashMap<String, String>()
                if (etPhoneNumber.text.toString().trim().isEmpty()) {
                    showToast("Please enter your phone number")
                } else {
                    if (withNumber == "withNumber") {
                        hashMap["country_code"] = "+91"
                        hashMap["mobile"] = etPhoneNumber.text.toString().trim()
                        hashMap["user_id"] = ""

                    } else {
                        hashMap["country_code"] = "+91"
                        hashMap["mobile"] = etPhoneNumber.text.toString().trim()
                        hashMap["user_id"] = SharedPref.instance.getString(AppConstant.USER_ID).toString()

                    }
                    mPresenter.sendOTP(hashMap)
                }
//                requireActivity().startActivity(
//                    Intent(mContext, MainActivity::class.java)
//                        .setFlags(
//                            Intent.FLAG_ACTIVITY_SINGLE_TOP or
//                                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
//                        )
//                )
//                requireActivity().finishAffinity()
            }
        }
    }

    override fun onSuccessfulUpdateProfile(signUpResponse: LoginResponse) {
        //   CommonMethods.replaceFragmentActivityWithoutStack(requireFragmentManager(),VerifyOTPScreenFragment(),R.id.splashContainer)
    }

    override fun onSuccessfullySendOTP(signUpResponse: LoginResponse) {

        if (signUpResponse.success) {
            CommonMethods.replaceFragmentActivityWithoutStack(
                requireFragmentManager(),
                VerifyOTPScreenFragment(
                    signUpResponse.otp!!,
                    etPhoneNumber.text!!.trim().toString(),
                    signUpResponse.is_number_already_exist,
                    withNumber
                ),
                R.id.splashContainer
            )

        }
    }

    override fun onSuccessfullyVerifyOTP(signUpResponse: SignUpResponse) {

    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }
}