package com.pbs.diveapp.fragments.chatScreen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.chatScreen.model.DataChat
import com.pbs.diveapp.fragments.chatScreen.model.DataItemChat
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.item_chat_box_from.view.*
import okhttp3.internal.notify

class MessageAdapter(
    val mContext: Context,
    var myId: String,
    var listMessage: ArrayList<DataChat>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var touch = 1
    override fun getItemViewType(position: Int): Int {
        return if (listMessage[position].sent_by.toString() == myId) {
            R.layout.item_chat_box_my
        } else
            R.layout.item_chat_box_from
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            MyViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
        view.init()
        return view
    }

    override fun getItemCount(): Int {
        return listMessage.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = holder as MyViewHolder
        item.textMessage.text = listMessage[position].message
//        if (!listMessage[position].userPic.isNullOrEmpty()) {
//            Glide.with(mContext)
//                .load(AppConstant.IMAGE_BASE_URL + listMessage[position].userPic)
//                .circleCrop()
//                .into(item.user_image)
//        }
        if (!listMessage[position].created_at!!.isNullOrEmpty()) {
            holder.sendTime.text = CommonMethods.getDate(CommonMethods.getLocalTime(listMessage[position].created_at.toString()).toString(),0)
//            holder.sendTime.text =
//                CommonMethods.getDate(listMessage[position].createdAt.toString(), 0)
        }
        if (listMessage[position].sent_by_detail.image !=null)
            CommonMethods.setProfileImage(mContext,listMessage[position].sent_by_detail.image.toString(),holder.user_image)
//        if (listMessage[position].userId == myId) {
//            if (listMessage[position].isSeen == 1) {
//                holder.seenIcon.visibility = View.VISIBLE
//            } else {
//                holder.seenIcon.visibility = View.GONE
//            }
//        } else {
//            holder.seenIcon.visibility = View.GONE
//        }
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var textMessage = item.tvChatFrom
        var user_image = item.ivfromUserProfile
        var sendTime = item.tvSendTime
//        var seenIcon = item.ivSeenIcon
        fun init() {
//            textMessage.setOnClickListener {
//                if (touch == 1) {
//                    sendTime.visibility = View.VISIBLE
//                    ++touch
//                } else if (touch == 2) {
//                    sendTime.visibility = View.GONE
//                    --touch
//                }
//            }
        }
    }

    fun updateList(chatList: ArrayList<DataChat>) {
        listMessage.clear()
        listMessage.addAll(chatList)
        notify()
    }
}