package com.pbs.diveapp.fragments.signUpFlow.whyAreScreen

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.heightEducationScreen.HeightAndEducationScreenFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_why_are_screen.*

/*
* This class is used as Why are you here Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class WhyAreScreenFragment(var userName:String,var userAge:String,var sexualOrientation:String) : BaseFragment(), View.OnClickListener {

    private lateinit var listPopupWindow: ListPopupWindow
    private var listOption = ArrayList<String>()
    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }


    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_why_are_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        allClicks()

        listOption.clear()
        listOption.addAll(mContext.resources.getStringArray(R.array.listss))
        listPopupWindow = ListPopupWindow(mContext)
        listPopupWindow.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item, listOption
            )
        )
        listPopupWindow.anchorView = tvChooseList
        listPopupWindow.isModal = true
        listPopupWindow.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        );
//        val display = windowManager.defaultDisplay
//        val size = Point()
//        display.getSize(size)
//        val width: Int = size.x
//        val height: Int = size.y
        listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
    //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
//        listPopupWindow.setWidth(700)
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            tvChooseList.text = listOption[position]
            listPopupWindow.dismiss()
        }

    }

    private fun allClicks() {
        btnNextOnHere.setOnClickListener(this)
        tvChooseList.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextOnHere -> {
                if (tvChooseList.text.toString().trim().isEmpty()) {
                    CommonMethods.showToast(mContext, "Please select one option")
                } else {
                    CommonMethods.replaceFragmentScreenWithBackStack(
                        requireFragmentManager(),
                        HeightAndEducationScreenFragment(userName,userAge,sexualOrientation,tvChooseList.text.toString().trim()),
                        R.id.splashContainer
                    )
                }
            }
            R.id.tvChooseList -> {
                listPopupWindow.show()
            }
        }
    }
}