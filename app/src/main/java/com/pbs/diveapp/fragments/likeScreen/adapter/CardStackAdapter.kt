package com.pbs.diveapp.fragments.likeScreen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.likeScreen.model.DataX
import com.pbs.diveapp.fragments.likeScreen.model.Images
import com.pbs.diveapp.fragments.likeScreen.model.Interest
import kotlinx.android.synthetic.main.activity_user_profile.view.*
import kotlinx.android.synthetic.main.item_cardstack_layout.view.*
import kotlinx.android.synthetic.main.item_cardstack_layout.view.tvUserDetails
import kotlinx.android.synthetic.main.item_profile_details.view.*
import org.w3c.dom.Text
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator


class CardStackAdapter(
    private var spots: List<DataX> = emptyList(),
    val mContext: Context,
    val profileClick: ClickProfile
) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {
    var snapHelper: LinearSnapHelper? = null
    lateinit var adapter: ImageAdapter
    private var x1 = 0f
    private var x2: Float = 0f
    val MIN_DISTANCE = 150
    private var behavior: BottomSheetBehavior<*>? = null

    private var currentPage = 0
    private var NUM_PAGES = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_cardstack_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        behavior = BottomSheetBehavior.from(holder.userDetails)
//        behavior?.isDraggable = false
        var i = 0
        val spot = spots[position]
        if (!spot.name.isNullOrEmpty())
            holder.name.text = "${spot.name}"

        holder.ivOpenDetails.setOnClickListener {
            if (holder.userDetails.visibility == View.GONE) {
                holder.userDetails.visibility = View.VISIBLE
//                behavior?.state =
//                    BottomSheetBehavior.STATE_EXPANDED
                holder.ivOpenDetails.setRotation(holder.ivOpenDetails.rotation + 180);
            } else {
//                behavior?.state =
//                    BottomSheetBehavior.STATE_COLLAPSED
                holder.userDetails.visibility = View.GONE
                holder.ivOpenDetails.setRotation(holder.ivOpenDetails.rotation + 180);
            }
        }

        if (spot.here_for != null)
            holder.tvLookingForCard.text = "I'm looking for a : " + spot.here_for

        if (spot.education != null)
            holder.tvEducationCard.text = spot.education

        if (spot.looking_for != null)
            holder.tvSexual.text = spot.looking_for

        if (spot.hometown != null)
            holder.tvAddress.text = spot.hometown

        if (spot.height != null) {
            holder.tvHeight.text = spot.height
        }

        if (spot.interests.isNotEmpty()) {
            val interlist = ArrayList<Interest>()
            interlist.addAll(spot.interests)
//            holder.rvInterestList.layoutManager =
//                LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
            holder.rvInterestList.adapter = InterestListAdapter(mContext, interlist)
        }

        if (spot.user_images.isNotEmpty()) {

            val imageList = ArrayList<Images>()
            imageList.addAll(spot.user_images)
            holder.verticalViewPager.adapter = ImageAdapter(mContext, imageList)
            NUM_PAGES = imageList.size

            holder.indicator.attachToPager(holder.verticalViewPager)

        } else {

//            val imageLists = arrayListOf(R.drawable.demo_image, R.drawable.demo_image_2, R.drawable.demo_image_3, R.drawable.demo_image_4)
//            holder.verticalViewPager.adapter = ImageAdapter(mContext, imageLists)
//            NUM_PAGES = imageList.size
//            holder.indicator.attachToPager(holder.verticalViewPager)

        }
        holder.name.setOnClickListener {
            profileClick.openProfileDetails(position,spot)
        }
    }
    override fun getItemCount(): Int {
        return spots.size
    }

    fun setSpots(spots: List<DataX>) {
        this.spots = spots
    }

    fun getSpots(): List<DataX> {
        return spots
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.tvUserName)

        var userDetails = view.userDetails!!
        val ivOpenDetails: ImageView = view.ivOpenDetails

        //        var coordinatorDetails:CoordinatorLayout=view.coordinatorDetails
        var verticalViewPager: ViewPager = view.verticalViewPager
        var indicator: ScrollingPagerIndicator = view.indicator
        var tvUserDetails: TextView = view.tvUserDetails
        var tvAddress: TextView = view.tvAddress
        var tvLookingForCard: TextView = view.tvLookingForCard
        var tvSexual: TextView = view.tvSexual
        var tvEducationCard: TextView = view.tvEducationCard
        var tvDescriptionCard: TextView = view.tvDescriptionCard
        var rvInterestList: RecyclerView = view.rvInterestList
        var tvHeight: TextView = view.tv_height
    }

}

interface ClickProfile {
    fun onClickProfile(position: Int)
    fun openProfileDetails(position: Int,details:DataX)
}
