package com.pbs.diveapp.fragments.venueListScreen.view

import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.utils.BaseView

interface VenuesListFragmentView:BaseView {
    fun onSuccessful()
    fun onSuccessfulRest(getRestaurantResponse: GetRestaurantResponse)
}