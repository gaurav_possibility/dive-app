package com.pbs.diveapp.fragments.venueListScreen.presenter

interface VenuesListFragmentHandler {
    fun getList()
    fun getRest(location:String,type:String,key:String,radius:String)
}