
package com.pbs.diveapp.fragments.selectPhotos.model;


import com.google.gson.annotations.SerializedName;


public class Language {

    @SerializedName("language")
    private String mLanguage;

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

}
