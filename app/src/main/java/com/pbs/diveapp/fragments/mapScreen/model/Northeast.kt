package com.pbs.diveapp.fragments.mapScreen.model

data class Northeast(
    val lat: Double,
    val lng: Double
)