package com.pbs.diveapp.fragments.signUpFlow.favcofeebar

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.FavoritePlaceScreenFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_fav_cofee_bar.*
import kotlinx.android.synthetic.main.fragment_height_and_education_screen.*


class FavCoffeeBar(
    var userName: String,
    var userAge: String,
    var sexualOrientation: String,
    var whyRuHere: String,
    var userHeight: String,
    var userEducation: String,
    var userHomeTown: String,
    var interestList: ArrayList<String>,
    var withNumber: String,
    var vaccinated:String
) : BaseFragment(), View.OnClickListener {
    lateinit var mContext: Context
    private lateinit var listPopupWindowDinner: ListPopupWindow
    private lateinit var listPopupWindowCoffee: ListPopupWindow
    private lateinit var listPopupWindowBar: ListPopupWindow
    private var dinnerList = ArrayList<String>()
    private var coffeeList = ArrayList<String>()
    private var barLIst = ArrayList<String>()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        allClicks()
        dinnerList.clear()   // clear list of height
        dinnerList.addAll(mContext.resources.getStringArray(R.array.dinner))  // add data in height list

        coffeeList.clear()
        coffeeList.addAll(mContext.resources.getStringArray(R.array.coffee))   // add data in education list

        barLIst.clear()
        barLIst.addAll(mContext.resources.getStringArray(R.array.bar))   // add data in vaccine list

        /// dinner popup

        listPopupWindowDinner = ListPopupWindow(mContext)
        listPopupWindowDinner.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                dinnerList     // set adapter for height list
            )
        )
        listPopupWindowDinner.anchorView = etDinner
        listPopupWindowDinner.isModal = true
        listPopupWindowDinner.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        listPopupWindowDinner.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindowDinner.setOnItemClickListener { parent, view, position, id ->
            etDinner.setText(dinnerList[position])
            listPopupWindowDinner.dismiss()
        }

        // coffee popup

        listPopupWindowCoffee = ListPopupWindow(mContext)
        listPopupWindowCoffee.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                coffeeList     // set adapter for height list
            )
        )
        listPopupWindowCoffee.anchorView = etCoffee
        listPopupWindowCoffee.isModal = true
        listPopupWindowCoffee.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        //     listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindowCoffee.setOnItemClickListener { parent, view, position, id ->
            etCoffee.setText(coffeeList[position])
            listPopupWindowCoffee.dismiss()
        }

        // bar popup

        listPopupWindowBar = ListPopupWindow(mContext)
        listPopupWindowBar.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                barLIst     // set adapter for height list
            )
        )
        listPopupWindowBar.anchorView = etBar
        listPopupWindowBar.isModal = true
        listPopupWindowBar.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        //     listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindowBar.setOnItemClickListener { parent, view, position, id ->
            etBar.setText(barLIst[position])
            listPopupWindowBar.dismiss()
        }
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_fav_cofee_bar
    }

    fun allClicks() {
        btnNextCoffeeBar.setOnClickListener(this)
        etDinner.setOnClickListener(this)
        etCoffee.setOnClickListener(this)
        etBar.setOnClickListener(this)
    }


    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.btnNextCoffeeBar -> {

                when {
                    etDinner.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please select your favorite dinner")
                    }
                    etCoffee.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please select your favorite coffee")
                    }
                    etBar.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please select your favorite bar")
                    }
                    else -> {
                        CommonMethods.replaceFragmentScreenWithBackStack(
                            requireFragmentManager(),
                            FavoritePlaceScreenFragment(
                                userName,
                                userAge, sexualOrientation, whyRuHere,
                                userHeight,
                                userEducation,
                                userHomeTown,vaccinated
                            ,etDinner.text.toString().trim(),etCoffee.text.toString().trim(),etBar.text.toString().trim()),
                            R.id.splashContainer
                        )
                    }
                }
            }
            R.id.etDinner -> {
                listPopupWindowDinner.show()
            }
            R.id.etCoffee -> {
                listPopupWindowCoffee.show()
            }
            R.id.etBar -> {
                listPopupWindowBar.show()
            }
        }
    }
}