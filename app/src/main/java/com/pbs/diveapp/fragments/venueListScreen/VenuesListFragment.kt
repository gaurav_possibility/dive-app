package com.pbs.diveapp.fragments.venueListScreen

import android.Manifest
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.fragments.mapScreen.model.ResultRest
import com.pbs.diveapp.fragments.venueListScreen.adapter.ItemClick
import com.pbs.diveapp.fragments.venueListScreen.adapter.VenueListAdapter
import com.pbs.diveapp.fragments.venueListScreen.presenter.VenuesListFragmentPresenter
import com.pbs.diveapp.fragments.venueListScreen.view.VenuesListFragmentView
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_venues_list.*
import kotlinx.android.synthetic.main.item_toolbar.*
import java.util.*


class VenuesListFragment(private val userName :String,private val userEmail:String,val userId:String,val userImage:String) : BaseFragment(), View.OnClickListener, VenuesListFragmentView, ItemClick {

    lateinit var mContext: Context
    lateinit var mPresenter: VenuesListFragmentPresenter


    val listRest = ArrayList<ResultRest>()
    lateinit var mAdapter:VenueListAdapter
    private val LOCATION_PERMISSION = 2
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    var wayLatitude: Double = 0.0
    var wayLongitude: Double = 0.0
    var city = ""
    var state = ""
    var address = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_venues_list
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = VenuesListFragmentPresenter(this)
        locationRequest = LocationRequest()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)
        getLocation()
        ivBackArrow.setColorFilter(Color.WHITE)
        allClicks()
        val list = arrayListOf(
            "1 G.W.SherKey's Raw Bar and Grill",
            "2 G.W.SherKey's Raw Bar and Grill",
            "3 G.W.SherKey's Raw Bar and Grill"
        )
        rvVenueList.layoutManager = LinearLayoutManager(mContext)
        mAdapter = VenueListAdapter(mContext, listRest, this,1)
        rvVenueList.adapter = mAdapter
    }

    private fun allClicks() {
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                requireActivity().onBackPressed()
            }
        }
    }

    override fun onSuccessful() {

    }

    override fun onSuccessfulRest(getRestaurantResponse: GetRestaurantResponse) {
        listRest.clear()
        listRest.addAll(getRestaurantResponse.results)
        mAdapter.notifyDataSetChanged()
    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }

    private fun dialogConfirmDate(address: ResultRest) {
        val dialogBuilder =
            android.app.AlertDialog.Builder(mContext)

        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView: View =
            inflater.inflate(R.layout.item_confim_date_layout, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(true)

        val alertDialog = dialogBuilder.create()
        val cvSendInvite = dialogView.findViewById<View>(R.id.cvSendInvite) as CardView
        val tvDateUserName = dialogView.findViewById<View>(R.id.tvDateUserName) as TextView
        val tvSelectedAddress = dialogView.findViewById<View>(R.id.tvSelectedAddress) as TextView
        val tvSelectedTime = dialogView.findViewById<View>(R.id.tvSelectedTime) as TextView
        val tvSelectedDate = dialogView.findViewById<View>(R.id.tvSelectedDate) as TextView
        val tvDateUserEmail = dialogView.findViewById<View>(R.id.tvDateUserEmail) as TextView
        val layoutSelectDate = dialogView.findViewById<View>(R.id.layoutSelectDate) as LinearLayout
        val layoutSelectTime = dialogView.findViewById<View>(R.id.layoutSelectTime) as LinearLayout
        val ivDateUserImage = dialogView.findViewById<View>(R.id.ivDateUserImage) as CircleImageView

        cvSendInvite.setOnClickListener {
            alertDialog.dismiss()
            requireActivity().onBackPressed()
        }

        tvDateUserName.text = userName
        tvSelectedAddress.text = address.name
        tvSelectedDate.text = CommonMethods.getCurrentDateCalender()
        CommonMethods.setProfileImage(mContext,userImage,ivDateUserImage)
        tvDateUserEmail.text = userEmail
        layoutSelectDate.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val time: Long = c.timeInMillis

            val dpd = DatePickerDialog(requireContext(), { _, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                tvSelectedDate.text = CommonMethods.getDateForDisplay(
                    "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year,
                    2
                ) //dd/MM/yyyy
                //yyyy-MM-dd

            }, year, month, day)
            dpd.datePicker.minDate = time + 24 * 60 * 60 * 1000
            dpd.show()
        }
        layoutSelectTime.setOnClickListener {
            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
            val minute = mcurrentTime[Calendar.MINUTE]
            val mTimePicker: TimePickerDialog
            mTimePicker = TimePickerDialog(
                mContext,
                { timePicker, selectedHour, selectedMinute ->
                    tvSelectedTime.setText(CommonMethods.convert24To12Hours("$selectedHour:$selectedMinute"))
                },
                hour,
                minute,
                false
            ) //Yes 24 hour time

            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }

        alertDialog.show()
        alertDialog.window!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(position: Int, item: ResultRest) {
        dialogConfirmDate(item)
    }

    override fun onClickView(position: Int, item: ResultRest) {

    }


    private fun getLocation() {
        if (ContextCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                AppConstant.LOCATION_REQUEST
            )
        } else {
            try {
                locationRequest!!.interval = 6000
                locationRequest!!.fastestInterval = 1000
                locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                mFusedLocationClient!!.lastLocation
                    .addOnSuccessListener(OnSuccessListener<Location?> { location ->
                        if (location != null) {
                            wayLatitude = location.latitude
                            wayLongitude = location.longitude
//                            getAddress(mContext, wayLatitude, wayLongitude)
                            mPresenter.getRest("$wayLatitude,$wayLongitude","restaurant","AIzaSyBNi4H4h7NUKCMFJUyEzUgPrfRhcRKhSnQ",
                            "25000")
                            Log.e(
                                "locationnnn","" +
                                        wayLatitude.toString() + " " + wayLongitude
                            )


                        } else {
                            mFusedLocationClient!!.requestLocationUpdates(
                                locationRequest,
                                object : LocationCallback() {
                                    override fun onLocationResult(p0: LocationResult?) {

                                        super.onLocationResult(p0)
                                        if (p0 != null)
                                            if (p0!!.locations.isEmpty().not()) {
                                                wayLatitude = p0.locations[0].latitude
                                                wayLongitude = p0.locations[0].longitude
//                                                getAddress(mContext, wayLatitude, wayLongitude)
                                                mPresenter.getRest("$wayLatitude,$wayLongitude","restaurant","AIzaSyBNi4H4h7NUKCMFJUyEzUgPrfRhcRKhSnQ",
                                                    "25000")
                                                Log.e(
                                                    "locationnnn",
                                                    wayLatitude.toString() + " " + wayLongitude
                                                )
                                                mFusedLocationClient!!.removeLocationUpdates(this)

                                            }
                                    }
                                },
                                null
                            )
                        }
                    })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            LOCATION_PERMISSION -> // If the permission is granted, get the location,
                getLocation()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            AppConstant.LOCATION_REQUEST -> // If the permission is granted, get the location,
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation()
                } else {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(
//                            this,
//                            android.Manifest.permission.ACCESS_FINE_LOCATION
//                        )
//                    ) {
////                        getLocation()
//                    } else {
//                        val snackbar: Snackbar = Snackbar.make(
//                            drawerLayout,
//                            resources.getString(R.string.message_text),
//                            5000
//                        )
//                        snackbar.setAction(
//                            resources.getString(R.string.settings),
//                            View.OnClickListener {
//                                if (this == null) {
//                                    return@OnClickListener
//                                }
//                                val intent = Intent()
//                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                                val uri: Uri = Uri.fromParts(
//                                    "package",
//                                    packageName,
//                                    null
//                                )
//                                intent.data = uri
//                                this.startActivity(intent)
//                            })
//                        snackbar.show()
//
//                    }
                }
        }
    }
}