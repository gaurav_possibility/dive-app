package com.pbs.diveapp.fragments.signUpFlow.userName

import android.content.Context
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.ageScreen.AgeScreenFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_name.*
/*
* This class is used as Name Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class NameFragment : BaseFragment(), View.OnClickListener {

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_name
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        etName.setText(SharedPref.instance.getString(AppConstant.USER_NAME))
        allClicks()
    }

    private fun allClicks(){
        btnNextName.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextName -> {
                if (etName.text.toString().trim().isEmpty()){
                    CommonMethods.showToast(mContext,"Please enter name")
                }else{
                    CommonMethods.replaceFragmentScreenWithBackStack(requireFragmentManager(),
                        AgeScreenFragment(etName.text.toString().trim()),R.id.splashContainer)
                }
            }
        }
    }
}