package com.pbs.diveapp.fragments.signUpFlow.phoneVerify.view

import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.utils.BaseView

interface PhoneVerifyScreenFragmentView :BaseView{
    fun onSuccessfulUpdateProfile(signUpResponse: LoginResponse)
    fun onSuccessfullySendOTP(signUpResponse: LoginResponse)
    fun onSuccessfullyVerifyOTP(signUpResponse: SignUpResponse)
}