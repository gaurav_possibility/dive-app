package com.pbs.diveapp.fragments.chatScreen.model

import com.google.gson.annotations.SerializedName

class SocketCallModel {
    @SerializedName("userId")
    var userId = ""


    @SerializedName("receiverId")
    var receiverId = ""

    @SerializedName("chatId")
    public var chatId = ""


    @SerializedName("usertype")
    var usertype: String? = null

    @SerializedName("groupId")
    var groupId: String? = null
}