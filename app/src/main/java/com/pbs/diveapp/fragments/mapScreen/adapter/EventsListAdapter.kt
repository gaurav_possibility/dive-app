package com.pbs.diveapp.fragments.mapScreen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_events_layout.view.*

class EventsListAdapter(
    val mContext: Context,
    val list: ArrayList<String>,
    val clickListener: ItemClickView
) :
    RecyclerView.Adapter<EventsListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventsListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.item_events_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: EventsListAdapter.MyViewHolder, position: Int) {

        holder.tvView.setOnClickListener {
            clickListener.onClickView(position)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val tvView: TextView = item.tvView
    }
}

interface ItemClickView {
    fun onClickView(position: Int)
}