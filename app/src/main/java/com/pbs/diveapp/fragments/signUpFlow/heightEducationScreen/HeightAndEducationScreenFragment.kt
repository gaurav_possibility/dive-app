package com.pbs.diveapp.fragments.signUpFlow.heightEducationScreen

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.favcofeebar.FavCoffeeBar
import com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.FavoritePlaceScreenFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_height_and_education_screen.*

/*
* This class is used as Height, Education and Home town Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class HeightAndEducationScreenFragment(
    var userName: String,
    var userAge: String,
    var sexualOrientation: String,
    var whyRuHere: String
) : BaseFragment(), View.OnClickListener {

    private lateinit var listPopupWindow: ListPopupWindow
    private lateinit var listPopupWindowEducation: ListPopupWindow
    private lateinit var listPopupWindowVaccine: ListPopupWindow
    private lateinit var listPopupWindowTown: ListPopupWindow
    lateinit var mContext: Context

    private var heightList = ArrayList<String>()
    private var educationList = ArrayList<String>()
    private var vaccineLIst = ArrayList<String>()
    private var hometownList = ArrayList<String>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_height_and_education_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        allClicks()

        heightList.clear()   // clear list of height
        heightList.addAll(mContext.resources.getStringArray(R.array.feet))  // add data in height list

        educationList.clear()
        educationList.addAll(mContext.resources.getStringArray(R.array.education))   // add data in education list

        vaccineLIst.clear()
        vaccineLIst.addAll(mContext.resources.getStringArray(R.array.vaccinelist))   // add data in vaccine list

        hometownList.clear()
        hometownList.addAll(mContext.resources.getStringArray(R.array.townList))   // add data in hometown list

        listPopupWindow = ListPopupWindow(mContext)
        listPopupWindow.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                heightList     // set adapter for height list
            )
        )
        listPopupWindow.anchorView = etHeight
        listPopupWindow.isModal = true
        listPopupWindow.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        //     listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            etHeight.setText(heightList[position])
            listPopupWindow.dismiss()
        }

/// education popup

        listPopupWindowEducation = ListPopupWindow(mContext)
        listPopupWindowEducation.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                educationList     // set adapter for height list
            )
        )
        listPopupWindowEducation.anchorView = etEducation
        listPopupWindowEducation.isModal = true
        listPopupWindowEducation.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        //     listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindowEducation.setOnItemClickListener { parent, view, position, id ->
            etEducation.setText(educationList[position])
            listPopupWindowEducation.dismiss()
        }

        // vaccine popup

        listPopupWindowVaccine = ListPopupWindow(mContext)
        listPopupWindowVaccine.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                vaccineLIst     // set adapter for height list
            )
        )
        listPopupWindowVaccine.anchorView = et_vaccinated
        listPopupWindowVaccine.isModal = true
        listPopupWindowVaccine.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        //     listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindowVaccine.setOnItemClickListener { parent, view, position, id ->
            et_vaccinated.setText(vaccineLIst[position])
            listPopupWindowVaccine.dismiss()
        }

        ///  town popup

        listPopupWindowTown = ListPopupWindow(mContext)
        listPopupWindowTown.setAdapter(
            ArrayAdapter(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                hometownList     // set adapter for height list
            )
        )
        listPopupWindowTown.anchorView = etHomeTown
        listPopupWindowTown.isModal = true
        listPopupWindowTown.setBackgroundDrawable(
            this.getResources().getDrawable(
                android.R.drawable.alert_light_frame
            )
        )

        //     listPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT)
        //    listPopupWindow.setWidth(width - 40)
//        listPopupWindow.setHeight(height-20)
        //    listPopupWindow.setWidth(700)
        listPopupWindowTown.setOnItemClickListener { parent, view, position, id ->
            etHomeTown.setText(hometownList[position])
            listPopupWindowTown.dismiss()
        }

    }

    private fun allClicks() {
        btnNextHeight.setOnClickListener(this)
        etHeight.setOnClickListener(this)
        et_vaccinated.setOnClickListener(this)
        etHomeTown.setOnClickListener(this)
        etEducation.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextHeight -> {
                when {
                    etHeight.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please enter your height")
                    }
                    etEducation.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please enter your education")
                    }
                    etHomeTown.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please enter your home town")
                    }
                    et_vaccinated.text.toString().trim().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please enter you are vaccinated or not")
                    }
                    else -> {
                        CommonMethods.replaceFragmentScreenWithBackStack(
                            requireFragmentManager(),
                            FavCoffeeBar(
                                userName,
                                userAge,
                                sexualOrientation,
                                whyRuHere,
                                etHeight.text.toString().trim(),
                                etEducation.text.toString().trim(),
                                etHomeTown.text.toString().trim(),
                                arrayListOf(),
                                "",
                                et_vaccinated.text.toString().trim()
                            ),
                            R.id.splashContainer
                        )
                    }
                }
            }
            R.id.etHeight -> {
                listPopupWindow.show()
            }
            R.id.etHomeTown -> {
                listPopupWindowTown.show()
            }
            R.id.et_vaccinated -> {
                listPopupWindowVaccine.show()
            }
            R.id.etEducation -> {
                listPopupWindowEducation.show()
            }
        }
    }
}