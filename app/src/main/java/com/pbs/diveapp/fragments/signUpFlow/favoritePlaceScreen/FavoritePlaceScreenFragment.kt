package com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import androidx.recyclerview.widget.GridLayoutManager
import com.dating.datingapp.fragments.selectPhotos.SelectPhotosFragment
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.MainActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.adapter.InterestListAdapter
import com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.adapter.SelectItem
import com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.model.InterestModel
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.PhoneVerifyScreenFragment
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.presenter.PhoneVerifyScreenFragmentPresenter
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.view.PhoneVerifyScreenFragmentView
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.fragments.signUpFlow.verifyOTP.VerifyOTPScreenFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_favorite_place_screen.*
import kotlinx.android.synthetic.main.fragment_phone_verify_screen.*

/*
* This class is used as Favorite Places Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class FavoritePlaceScreenFragment(
    var userName: String,
    var userAge: String,
    var sexualOrientation: String,
    var whyRuHere: String,
    var userHeight: String,
    var userEdutcation: String,
    var userHomeTown: String,
    var vaccinated:String,
    var favDinner:String,
    var favCoffee:String,
    var favBar:String
) : BaseFragment(), View.OnClickListener, SelectItem,
    PhoneVerifyScreenFragmentView {

    lateinit var mContext: Context
    var intersList = ArrayList<String>()
    lateinit var mPresenter: PhoneVerifyScreenFragmentPresenter
    private lateinit var userData: Data

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_favorite_place_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = PhoneVerifyScreenFragmentPresenter(this)
        allClick()

        val intersetList = arrayListOf(
            "Hiking", "Travel", "DIY", "Politics",
            "Spirituality", "Movies", "Gardening", "Walking", "Reading", "Shopping", "Board Games",
            "Swimming", "Blogging", "Photography"
        )
        val interList = ArrayList<InterestModel>()
        for (i in intersetList.indices) {
            interList.add(InterestModel(intersetList[i]))
        }
        rvInterestList.layoutManager =
            GridLayoutManager(mContext, 4, GridLayoutManager.HORIZONTAL, false)
        rvInterestList.adapter = InterestListAdapter(mContext, interList, this)

    }

    private fun allClick() {
        btnNextFavoritePlace.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNextFavoritePlace -> {

                if(intersList.size==0){
                    CommonMethods.showToast(mContext,"Please select at least one")

                }else{
                    val hashMap = HashMap<String, String>()
                    hashMap["country_code"] = "+91"
                    hashMap["name"] = userName
                    hashMap["mobile"] = ""
                    hashMap["gender"] = ""
                    hashMap["dob"] = userAge
                    hashMap["looking_for"] = sexualOrientation
                    hashMap["here_for"] = whyRuHere
                    hashMap["education"] = userEdutcation
                    hashMap["hometown"] = userHomeTown
                    hashMap["interest"] = intersList.toString().replace("[", "").replace("]", "")
                    hashMap["height"] = userHeight
                    hashMap["bio"] = ""
                    hashMap["is_profile_completed"] = "1"
                    mPresenter.updateProfile(hashMap)
                }
            }
        }
    }

    override fun onClick(position: Int, name: String) {
        if (intersList.contains(name)) {
            intersList.remove(name)
        } else {
            if (intersList.size < 5) {
                CommonMethods.logPrint(intersList.size.toString())
                intersList.add(name)
            } else {
                CommonMethods.showToast(mContext, "You can select max 5 interests.")
            }
        }
    }

    override fun onSuccessfulUpdateProfile(signUpResponse: LoginResponse) {
        SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(signUpResponse.data))
        SharedPref.instance.setString(AppConstant.USER_NAME,signUpResponse.data.name.toString())

        userData=Gson().fromJson(SharedPref.instance.getString(AppConstant.USER_DATA),Data::class.java)

        if (userData.mobile.isNullOrEmpty()){
            CommonMethods.replaceFragmentScreenWithBackStack(
                requireFragmentManager(),
                PhoneVerifyScreenFragment(
                    userName, userAge, sexualOrientation, whyRuHere,
                    userHeight, userEdutcation, userHomeTown, intersList,
                ""),
                R.id.splashContainer
            )

        }else{

            CommonMethods.replaceFragmentActivityWithoutStack(
                requireFragmentManager(),
                SelectPhotosFragment(),
                R.id.splashContainer
            )
//            requireActivity().startActivity(
//                Intent(mContext, MainActivity::class.java)
//                    .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//            )
//            requireActivity().finishAffinity()
        }

    }

    override fun onSuccessfullySendOTP(signUpResponse: LoginResponse) {

    }

    override fun onSuccessfullyVerifyOTP(signUpResponse: SignUpResponse) {

    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }
}