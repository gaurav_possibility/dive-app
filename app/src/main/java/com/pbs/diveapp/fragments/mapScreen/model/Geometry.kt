package com.pbs.diveapp.fragments.mapScreen.model

data class Geometry(
    val location: Location,
    val viewport: Viewport
)