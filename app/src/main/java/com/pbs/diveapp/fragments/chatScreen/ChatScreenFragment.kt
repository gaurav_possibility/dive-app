package com.pbs.diveapp.fragments.chatScreen

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.chatScreen.adapter.MessageAdapter
import com.pbs.diveapp.fragments.chatScreen.model.*
import com.pbs.diveapp.fragments.chatScreen.presenter.MessageScreenFragmentPresenter
import com.pbs.diveapp.fragments.chatScreen.view.MessageScreenFragmentView
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.fragments.notification.presenter.ChatScreenFragmentPresenter
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.ApplicationGlobal
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_chat_screen.*
import kotlinx.android.synthetic.main.item_toolbar.*
import org.json.JSONObject


class ChatScreenFragment(private val details: String) : BaseFragment(), View.OnClickListener,
    MessageScreenFragmentView {

    lateinit var mContext: Context
    lateinit var mPresenter:MessageScreenFragmentPresenter

    lateinit var userDetails: DataXMatch
    lateinit var myDetails:Data

    lateinit var adapter: MessageAdapter
     var listMessage1= ArrayList<DataChat>()

    lateinit var socket: Socket



    var chatId = ""
    var userId = ""
    var reciverIdI = ""
    var receiverName = ""
    var lastMessageRandomId = ""
    var receiverImages = ""
    var userImages = ""
    var userName = ""
    var groupId = ""
    var userType = ""
    var userBlocked11: Boolean = false

    var fromScreen = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_chat_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = MessageScreenFragmentPresenter(this)
        allClicks()
        userDetails = Gson().fromJson(details, DataXMatch::class.java)
        myDetails = Gson().fromJson(SharedPref.instance.getString(AppConstant.USER_DATA),Data::class.java)
        tvUserName.text = userDetails.name
        CommonMethods.setProfileImage(mContext, userDetails.image, ivUserProfile)
        CommonMethods.logPrint(userDetails.toString())


        userId = myDetails.id.toString()
        reciverIdI = userDetails.id.toString()
        userName = myDetails.name
        receiverName = userDetails.name
        userImages = myDetails.image!!
        receiverImages = userDetails.image
        chatId = reciverIdI+userId

        listMessage1.clear()
//        listMessage1.add(DataItemChat("","","","","",false,
//            "","asdfasdfads",0,"99","","","","",
//            "",0,"","","","",""))
//        listMessage1.add(DataItemChat("","","","","",false,
//            "","asdfasdfads",0,"","","","","",
//            "",0,"","","","",""))
//        listMessage1.add(DataItemChat("","","","","",false,
//            "","asdfasdfads",0,"","","","","",
//            "",0,"","","","",""))
//        listMessage1.add(DataItemChat("","","","","",false,
//            "","asdfasdfads",0,"99","","","","",
//            "",0,"","","","",""))

        var layoutManager = LinearLayoutManager(mContext)

        rvChatList.layoutManager = layoutManager
        adapter = MessageAdapter(
            mContext,
            SharedPref.instance.getString(AppConstant.USER_ID).toString(),
            listMessage1
        )
        rvChatList.adapter = adapter

        initSocket()
    }

    private fun allClicks() {
        ivBackArrow.setOnClickListener(this)
        ivSendMge.setOnClickListener(this)
    }



    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                requireActivity().onBackPressed()
            }
            R.id.ivSendMge -> {
                if (etMessageBox.text.toString().trim().isEmpty()){
                    showToast("Please type your message")
                }else{
//                    CommonMethods.toastUnderDevelopment(mContext)
                    sendMessage(false)
                }

            }
        }
    }

    override fun onResume() {
        super.onResume()

        mPresenter.getMessageList(reciverIdI)
    }

    private var onSeen: Emitter.Listener = Emitter.Listener {
        Log.e("seenf", "seenf")
        val dataSeen = it[0] as JSONObject
        var model = Gson().fromJson(dataSeen.toString(), ChatNewModel::class.java)
        Log.e("listenMessage", "listenSeen" + dataSeen.toString())
        if (!listMessage1.isNullOrEmpty()) {
            for (i in listMessage1.indices) {
                if (listMessage1[i].sent_by.toString() == userDetails.id.toString()) {
                    listMessage1[i].is_seen = "1"
                }
            }
            Log.e("listenMessage", "listenSeen" + Gson().toJson(listMessage1))
            Handler(Looper.getMainLooper()).post(Runnable { //do stuff like remove view etc
                adapter.notifyDataSetChanged()
                rvChatList.smoothScrollToPosition(listMessage1.size)
            })
        }
//            chatAdapter.lastSeenUpdate()
    }

    private fun initSocket() {
        val app: ApplicationGlobal =requireActivity().application as ApplicationGlobal
        socket = app.getSocket()
        socket.on(Socket.EVENT_CONNECT, onConnect)
        // socket.on(Socket.EVENT_DISCONNECT, onConnect)

        socket.on(chatId, onSeen)
        socket.on("receivedMessage", onReceiveMessage)
        Log.e("Socket Connection: h", "connecting")
        socket.connect()

    }


    fun seenMessage(model: SocketCallModel) {
//        model.chatId = chatId
        Log.e("qwrqwerqwe", "" + Gson().toJson(model))
        // Log.e("test",Gson().fromJson(model.toString())+"")
        var data = Gson().toJson(model)
        socket.emit("seen", data)

    }

    private var onConnect: Emitter.Listener = Emitter.Listener {
        //Log.e("Socket Connection: h", "Socket onnection" + Gson().toJson(it))
        Log.e("qwerqewrq23423d", " join " + userId + " " + reciverIdI)

        socket.emit("join", userId + reciverIdI)
        //   socket.emit("seen",dataModel.otherid + SharedPref.getInstance(mContext).getString(Constants.USER_ID))
        var data = SocketCallModel()
        data.chatId = userId + reciverIdI
        data.usertype = userType
        data.groupId = groupId
        seenMessage(data)

        //   sendMessage();
    }

    private val onReceiveMessage =
        Emitter.Listener { args ->
            //   Log.e("listen","listen")
            val data = args[0] as JSONObject

            var model = Gson().fromJson(data.toString(), MessageReceiverResponse::class.java)
            Log.e("listenMessage", "listen" + data.toString())
            if (model.last_message.message.isNullOrEmpty().not() && model.last_message.sent_by.toString() != myDetails.id.toString()) {
                var chatCustomerList = ArrayList<DataChat>()
                var chatmodel = DataChat(
                    model.last_message.chat_id!!.toInt(),
                    model.created_at,
                    "",
                    "",
                    "",
                    model.id,
                    model.last_message.is_seen,
                   model.last_message.message,
                    model.last_message.sent_by,
                    model.last_message.type,
                    "",
                    SentDetail(model.last_message.sent_by_detail.id,model.last_message.sent_by_detail.name,model.last_message.sent_by_detail.image)
                );
                chatCustomerList.add(chatmodel)
                listMessage1.add(chatmodel)
                Handler(Looper.getMainLooper()).post(Runnable { //do stuff like remove view etc
                    adapter.notifyDataSetChanged()
                    rvChatList.smoothScrollToPosition(listMessage1.size)
                })
//                adapter.notifyDataSetChanged()

//                val hashMap = HashMap<String, String>()
//                hashMap["userId"] = userId + reciverIdI
//                hashMap["receiverId"] = reciverIdI + userId
//                mPresenter.getChat(hashMap)
            }
            var data11 = SocketCallModel()
            data11.chatId = userId + reciverIdI
            data11.usertype = userType
            data11.groupId = groupId
            seenMessage(data11)
//                        }
//                    }
//                        }
//            }
            //  Toast.makeText(mContext,"blocked me- ${blockedme} other blocked- ${otherBlocked.toString()}",Toast.LENGTH_LONG).show()
        }

    private fun sendMessage(isBlocked: Boolean) {
        if (CommonMethods.phoneIsOnline(mContext)) {
            if (socket.connected()) {
                if (isBlocked) {

                } else
                    if (etMessageBox.text.toString().isNotEmpty()) {
                        var request = ChatNewModel()
//                        request.userId = userId
//                        request.username = userName
//                        request.receiverId = reciverIdI
                        request.message = etMessageBox.text.toString().trim()
//                        request.receivername = receiverName
//                        request.receiverPic = receiverImages
//                        request.userPic = userImages.toString()
//                        request.usertype = userType.toString()
//                        request.groupId = groupId
                        request.receiver_id = reciverIdI.toInt()
                        request.sender_id = userId.toInt()
                        request.type = "text"
                        request.chatId = chatId
                        request.createdAt = CommonMethods.getCurrentDateUTC()
                        val param = JSONObject(Gson().toJson(request))
                        Log.e("sendmessage", param.toString())
                        socket.emit("sendMessage", param)

                        if (!isBlocked) {
                            var chatCustomerList = ArrayList<DataChat>()
                            var chatmodel = DataChat(
                                chatId!!.toInt(),
                                CommonMethods.getCurrentDateUTC(),
                                "",
                                "",
                                "",
                                userId.toInt(),
                                "",
                                etMessageBox.text.toString().trim(),
                                userId.toInt(),
                                "text",
                                CommonMethods.getCurrentDateUTC(),
                                SentDetail(myDetails.id,myDetails.name,myDetails.image)
                            );
                            chatCustomerList.add(chatmodel)
//                                        chatList.add(IndividualChatHeaderModel("", chatCustomerList))
                            listMessage1.add(chatmodel)

//                                        chatAdapter.updateList(chatList)
                            adapter.notifyDataSetChanged()
                            rvChatList.smoothScrollToPosition((listMessage1.size))
                            etMessageBox.setText("")
                        }
                    }
            } else {
                showToast("Not connected")
            }
        }else
            showToast("No internet connection available!")
    }

    override fun onPause() {
        super.onPause()
        socket.off()
        socket.close()
        socket.disconnect()
    }

    override fun onSuccessful(chatMsgListResponse: ChatMsgListResponse) {

        listMessage1.clear()
        listMessage1.addAll(chatMsgListResponse.data)
        listMessage1.reverse()
        adapter.notifyDataSetChanged()
        try {
            rvChatList.smoothScrollToPosition(adapter.itemCount - 1)
        } catch (e: Exception) {
        }
        rvChatList.scrollToPosition(listMessage1.lastIndex)
    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(mContext)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }


}