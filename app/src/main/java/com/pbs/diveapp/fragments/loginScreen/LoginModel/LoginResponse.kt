package com.pbs.diveapp.fragments.loginScreen.LoginModel

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    val access_token: String,
    val `data`: Data,
    val message: String,
    val status: Int,
    val success: Boolean,
    val is_number_already_exist: Boolean,
    @SerializedName("otp") var otp: String? = null
)