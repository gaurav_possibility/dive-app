package com.pbs.diveapp.fragments.mapScreen.model

data class Southwest(
    val lat: Double,
    val lng: Double
)