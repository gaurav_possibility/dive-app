package com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.view

import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.utils.BaseView

interface SignUpPresenterView : BaseView {

    fun onSuccessSignUP(response: LoginResponse)
    // fun onSuccessfulupdate(loginResponse: LoginResponse)
}