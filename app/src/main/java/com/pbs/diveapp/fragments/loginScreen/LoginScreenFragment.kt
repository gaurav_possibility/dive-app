package com.pbs.diveapp.fragments.loginScreen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.pbs.diveapp.fragments.loginScreen.presenter.LoginFragmentPresenter
import com.dating.datingapp.fragments.loginScreen.view.LoginFragmentView
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.MainActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.PhoneVerifyScreenFragment
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.SignUpScreenFragment
import com.pbs.diveapp.fragments.signUpFlow.userName.NameFragment
import com.pbs.diveapp.fragments.signUpFlow.verifyOTP.VerifyOTPScreenFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_login_screen.*
import kotlinx.android.synthetic.main.fragment_phone_verify_screen.*
import java.util.*
import kotlin.collections.HashMap


class LoginScreenFragment : BaseFragment(), View.OnClickListener, LoginFragmentView {
    lateinit var mContext: Context
    lateinit var mPresenter: LoginFragmentPresenter
    lateinit var callbackManager: CallbackManager
    private val RC_SIGN_IN = 7
    var socialId: String = ""
    lateinit var userType: String
    var firstName: String = ""
    var userName: String = ""
    var emailFacebook: String = ""
    var lastname: String = ""
    var profileImage: String = ""
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_login_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = LoginFragmentPresenter(this)
        allClicks()
        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
        fbLogin()
    }

    private fun allClicks() {
        btnLogin.setOnClickListener(this)
        tvSignUp.setOnClickListener(this)
        btnPhone.setOnClickListener(this)
        btnFacebook.setOnClickListener(this)

    }

    private fun fbLogin() {
        // this is used to facebook lognin

        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    val request =
                        GraphRequest.newMeRequest(result!!.accessToken) { `object`, response ->
                            try {
                                LoginManager.getInstance().logOut()
                                AccessToken.setCurrentAccessToken(null)
                                socialId = `object`.getString("id")
                                userName = `object`.getString("name")
                                firstName = `object`.getString("first_name")
                                if (`object`.has("last_name"))
                                    lastname = `object`.getString("last_name")

                                if (`object`.has("email")) {
                                    emailFacebook = `object`.getString("email")
                                } else {
                                    emailFacebook = ""
                                }

                                val im = `object`.getString("picture")
                                val profileImage =
                                    ("https://graph.facebook.com/" + socialId + "/picture?type=large")

                                var hashMap = HashMap<String, String>()
                                hashMap["email"] = emailFacebook
                                hashMap["type"] = "1"
                                hashMap["facebook_id"] = socialId
                                hashMap["name"] = userName
                                mPresenter.socialLogin(hashMap)
                                Log.i(
                                    "FacebookSingIn",
                                    "Response  :" + socialId + "  " + emailFacebook + " f " + firstName + " l " + lastname + " u " + userName
                                )
                            } catch (e: Exception) {
                                Log.i("FacebookSingIn", "Response error  :" + e.message)
                            }
                        }
                    val parameters = Bundle()
                    parameters.putString("fields", "email,name,first_name, last_name,picture")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    CommonMethods.showToast(mContext, "Login Cancel")
                }

                override fun onError(error: FacebookException?) {
                    CommonMethods.showToast(mContext, "" + error!!.message)
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnLogin -> {

                when {
                    etEmailAndPhone.text!!.trim().toString().isEmpty() -> {
                        CommonMethods.showToast(
                            mContext,
                            "Please enter email address or phone number"
                        )
                    }
                    etPassword.text!!.trim().toString().isEmpty() -> {
                        CommonMethods.showToast(mContext, "Please enter password")
                    }
                    else -> {
                        var hashMap = HashMap<String, String>()
                        hashMap["email"] = etEmailAndPhone.text!!.trim().toString()
                        hashMap["password"] = etPassword.text!!.trim().toString()

                        mPresenter.login(hashMap)
                    }
                }
            }
            R.id.btnFacebook -> {
                if (CommonMethods.phoneIsOnlineSocial(mContext)) {
                    LoginManager.getInstance().logInWithReadPermissions(
                        this,
                        Arrays.asList("public_profile", "email")
                    )
                } else {
                    CommonMethods.showToast(mContext, "Internet connection not available")
                }
            }

            R.id.tvSignUp -> {
                CommonMethods.replaceFragmentActivityWithoutStack(
                    requireFragmentManager(),
                    SignUpScreenFragment(),
                    R.id.splashContainer
                )
            }
            R.id.btnPhone -> {
                CommonMethods.replaceFragmentActivityWithoutStack(
                    requireFragmentManager(),
                    PhoneVerifyScreenFragment("", "", "", "", "", "", "", arrayListOf(),"withNumber"),
                    R.id.splashContainer
                )

            }
        }
    }

    override fun onSuccessfulLogin(response: LoginResponse) {
        if (response.success) {
            SharedPref.instance.setString(AppConstant.ACCESS_TOKEN, response.access_token)
            SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(response.data))
            SharedPref.instance.setString(AppConstant.USER_ID, response.data.id.toString())
            SharedPref.instance.setBoolean(AppConstant.USER_LOGGED_IN, true)
            SharedPref.instance.setString(AppConstant.USER_NAME, response.data.name.toString())
            requireActivity().startActivity(
                Intent(mContext, MainActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            )
            requireActivity().finishAffinity()
        }
    }

    override fun onSuccessfulSocialLogin(response: LoginResponse) {
        if (response.success) {
            SharedPref.instance.setString(AppConstant.ACCESS_TOKEN, response.access_token)
            SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(response.data))
            SharedPref.instance.setString(AppConstant.USER_ID, response.data.id.toString())
            SharedPref.instance.setString(AppConstant.USER_NAME, response.data.name.toString())
            SharedPref.instance.setString(AppConstant.EMAIL, response.data.email.toString())
            SharedPref.instance.setBoolean(AppConstant.USER_LOGGED_IN, true)
            CommonMethods.replaceFragmentScreenWithBackStack(
                requireFragmentManager(),
                NameFragment(),
                R.id.splashContainer
            )
        }

    }

    override fun showToast(message: String) {
        CommonMethods.showToast(mContext, message)
    }

    override fun showProgressBar() {
        showBaseProgressDialog(mContext, "")
    }

    override fun hideProgressBar() {
        hideBaseProgressDialog()
    }
}