package com.pbs.diveapp.fragments.mapScreen.view

import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.utils.BaseView

interface MapScreenFragmentView:BaseView {
    fun onSuccessful(getRestaurantResponse: GetRestaurantResponse)
}