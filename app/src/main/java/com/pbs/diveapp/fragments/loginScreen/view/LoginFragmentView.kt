package com.dating.datingapp.fragments.loginScreen.view
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.utils.BaseView

interface LoginFragmentView : BaseView {
    fun onSuccessfulLogin(response: LoginResponse)
    fun onSuccessfulSocialLogin(response: LoginResponse)
}