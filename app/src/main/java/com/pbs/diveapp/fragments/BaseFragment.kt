package com.pbs.diveapp.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.github.ybq.android.spinkit.style.ChasingDots
import com.github.ybq.android.spinkit.style.WanderingCubes
import com.google.android.material.snackbar.Snackbar
import com.pbs.diveapp.R
import java.io.File

/*
* This abstract class is used as Base Fragment class for this app
*
* created by GTB on 20/09/2021
*/
abstract class BaseFragment : Fragment() {
    private lateinit var mAlertDialog: AlertDialog
    internal open var mImageFile: File? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutToInsert(), container, false)
    }

    fun showBaseProgressDialog(context: Context, message: String) {
        val mAlertDialogBuilder = AlertDialog.Builder(context)
        mAlertDialogBuilder.setCancelable(false)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.view_progress_dialog, null)
        view.findViewById<ProgressBar>(R.id.progressBarPB).apply {
            indeterminateDrawable = ChasingDots()
        }
        view.findViewById<TextView>(R.id.progressMessageTV).apply {
            text = message
        }
        mAlertDialogBuilder.setView(view)
        mAlertDialog = mAlertDialogBuilder.create()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        try {

            if (mAlertDialog.isShowing.not()){
                mAlertDialog.show()
            }

        }catch (e:Exception){
            e.printStackTrace()
            Log.e("appClosedHere"," AppClosed "+e.message)
        }

        val layoutParams = WindowManager.LayoutParams()
        val window = mAlertDialog.window
        window!!.setGravity(Gravity.CENTER)
        layoutParams.copyFrom(window.attributes)
        // show dialog on full screen
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = layoutParams
    }

    fun hideBaseProgressDialog() {
        if (this::mAlertDialog.isInitialized && mAlertDialog.isShowing) {
            try {
                mAlertDialog.dismiss()
            }catch (e:Exception){
                e.printStackTrace()
                Log.e("appClosedHere"," AppClosed "+e.message)
            }
        }
    }

    internal fun showBaseToast(view: View, message: String) {
        val snackBar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
        snackBar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
            .apply { maxLines = 5 }
        if (snackBar.isShown) snackBar.dismiss()
        snackBar.show()
    }

    protected abstract fun getLayoutToInsert(): Int


}