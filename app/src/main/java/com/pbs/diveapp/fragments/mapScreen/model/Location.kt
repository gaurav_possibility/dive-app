package com.pbs.diveapp.fragments.mapScreen.model

data class Location(
    val lat: Double,
    val lng: Double
)