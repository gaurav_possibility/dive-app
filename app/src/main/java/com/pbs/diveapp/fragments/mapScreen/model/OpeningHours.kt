package com.pbs.diveapp.fragments.mapScreen.model

data class OpeningHours(
    val open_now: Boolean
)