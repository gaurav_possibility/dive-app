package com.pbs.diveapp.fragments.loginScreen.presenter


import com.dating.datingapp.fragments.loginScreen.view.LoginFragmentView
import com.dating.datingapp.webServices.handler.loginHandler
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.webServices.WebService

class LoginFragmentPresenter(val view: LoginFragmentView) : LoginFragmentHandler {
    override fun login(request: HashMap<String, String>) {
        view.showProgressBar()
        WebService.mInstance.login(request, object : loginHandler {

            override fun onSuccess(response: LoginResponse) {
                view.hideProgressBar()
                view.onSuccessfulLogin(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }

    override fun socialLogin(request: HashMap<String, String>) {
        WebService.mInstance.socialLogin(request,object :loginHandler{
            override fun onSuccess(response: LoginResponse) {
                view.hideProgressBar()
                view.onSuccessfulSocialLogin(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }

        })
    }
}