package com.pbs.diveapp.fragments.notification.presenter

import com.pbs.diveapp.fragments.chatScreen.model.MatchesListResponse
import com.pbs.diveapp.fragments.notification.view.ChatScreenFragmentView
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.MatchListHandler

class ChatScreenFragmentPresenter(val view : ChatScreenFragmentView): ChatScreenFragmentHandler {
    override fun getMatchList() {
        view.hideProgressBar()
        WebService.mInstance.getMatchList(object :MatchListHandler{
            override fun onSuccessful(matchesListResponse: MatchesListResponse) {
                view.hideProgressBar()
                view.onSuccessful(matchesListResponse)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }
}