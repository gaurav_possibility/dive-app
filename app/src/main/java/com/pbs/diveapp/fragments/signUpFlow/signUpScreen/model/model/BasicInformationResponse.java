
package com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model;
import com.google.gson.annotations.SerializedName;

public class BasicInformationResponse {

    @SerializedName("data")
    private BasicInformationData mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("success")
    private Boolean mSuccess;

    public BasicInformationData getData() {
        return mData;
    }

    public void setData(BasicInformationData data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(Boolean success) {
        mSuccess = success;
    }

}
