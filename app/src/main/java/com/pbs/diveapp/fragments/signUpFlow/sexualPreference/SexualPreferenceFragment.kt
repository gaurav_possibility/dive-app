package com.pbs.diveapp.fragments.signUpFlow.sexualPreference

import android.content.Context
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.whyAreScreen.WhyAreScreenFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_sexual_preference.*

/*
* This class is used as Sexual Preference Screen Fragment class for this app
*
* created by GTB on 20/09/2021
*/
class SexualPreferenceFragment(var userName:String,var userAge:String) : BaseFragment(),View.OnClickListener {

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_sexual_preference
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        allClicks()
    }

    private fun allClicks(){
        btnNextSexual.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
          R.id.btnNextSexual->{
                if (etLookingFor.text.toString().trim().isEmpty()){
                    CommonMethods.showToast(mContext,"Please enter your sexual preference")
                }else{
                    CommonMethods.replaceFragmentScreenWithBackStack(
                        requireFragmentManager(),
                        WhyAreScreenFragment(userName,userAge,etLookingFor.text.toString().trim()),
                        R.id.splashContainer
                    )
                }
            }
        }
    }


}