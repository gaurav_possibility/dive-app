package com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.signUpFlow.favoritePlaceScreen.model.InterestModel
import kotlinx.android.synthetic.main.item_interest_layout.view.*

class InterestListAdapter(
    val mContext: Context,
    private val list: ArrayList<InterestModel>,
    private val clickListener: SelectItem
) :
    RecyclerView.Adapter<InterestListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InterestListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.item_interest_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: InterestListAdapter.MyViewHolder, position: Int) {
        if (list[position].selected) {
            holder.interestName.background =
                mContext.resources.getDrawable(R.drawable.bg_black_color)
            holder.interestName.setTextColor(Color.WHITE)
        } else {
            holder.interestName.background =
                mContext.resources.getDrawable(R.drawable.bg_button_white)
            holder.interestName.setTextColor(Color.BLACK)
        }
        holder.interestName.text = list[position].interestName
        holder.itemView.setOnClickListener {
            clickListener.onClick(position,list[position].interestName)
            list[position].selected = !list[position].selected
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = list.size

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var interestName: TextView = item.tvInterestName
    }
}

interface SelectItem {
    fun onClick(position: Int,interest:String)
}