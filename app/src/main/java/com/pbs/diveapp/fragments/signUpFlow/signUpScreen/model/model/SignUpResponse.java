
package com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model;


import com.google.gson.annotations.SerializedName;

public class SignUpResponse {

    @SerializedName("data")
    private SignUpData mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("otp")
    private String otp;
    @SerializedName("success")
    private Boolean mSuccess;

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("is_number_already_exist")
    private Boolean is_number_already_exist;

    public Boolean getIs_number_already_exist() {
        return is_number_already_exist;
    }

    public void setIs_number_already_exist(Boolean is_number_already_exist) {
        this.is_number_already_exist = is_number_already_exist;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public SignUpData getData() {
        return mData;
    }

    public void setData(SignUpData data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(Boolean success) {
        mSuccess = success;
    }

}
