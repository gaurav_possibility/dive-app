package com.pbs.diveapp.fragments.likeScreen.model

data class SwippeResponse(
    val is_matched: Boolean,
    val message: String,
    val profile_detail: ProfileDetail,
    val status: Int,
    val success: Boolean
)