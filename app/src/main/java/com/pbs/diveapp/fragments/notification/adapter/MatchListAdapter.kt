package com.pbs.diveapp.fragments.notification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.chatScreen.model.DataXMatch
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.item_match_layout.view.*

class MatchListAdapter(
    val context: Context,
    private val matchList: ArrayList<DataXMatch>,
    private val clickListener: MatchItemClick
) :
    RecyclerView.Adapter<MatchListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MatchListAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_match_layout, parent, false)
        )

    }

    override fun onBindViewHolder(holder: MatchListAdapter.MyViewHolder, position: Int) {
        holder.userName.text = matchList[position].name
        holder.itemView.setOnClickListener {
            clickListener.onMatchClick(position,matchList[position])

        }
       CommonMethods.setProfileImage(context,matchList[position].image,holder.userImage)
        holder.tvMatchTime.text =
            CommonMethods.getDate(CommonMethods.getLocalTime(matchList[position].created_at).toString(),0)

    }

    override fun getItemCount() = matchList.size

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val userName: TextView = item.tvUserNameMatch
        val userImage :ImageView =item.ivUserProfileMatch
        val userDetails :TextView = item.tvUserDetailsMatch
        val tvMatchTime :TextView = item.tvMatchTime

    }
}

interface MatchItemClick {
    fun onMatchClick(position: Int,details:DataXMatch)
}