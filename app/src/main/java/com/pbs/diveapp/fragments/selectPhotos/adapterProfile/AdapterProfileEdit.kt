package com.dating.datingapp.activities.editProfile.adapterProfile

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.ImageViewTarget
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.selectPhotos.model.EditModel

class AdapterProfileEdit(
    var mcontext: Context,
    var list: MutableList<EditModel>,
    val listenerAddImage: addImage,
    var listenerremoveimage: removeImage,
    var listenerEditImage:editImage
) :
    RecyclerView.Adapter<AdapterProfileEdit.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(mcontext).inflate(R.layout.edit_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

//        CommonMethods.LoadImage(
//            holder.upload_img,
//            list[position].image.toString()
//        )

        val circularProgressDrawable = CircularProgressDrawable(mcontext)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        var options: RequestOptions = RequestOptions()
            .placeholder(circularProgressDrawable)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .error(R.drawable.cs_icon)
        options = options.transforms(CenterCrop(), RoundedCorners(16))
        if (list[position].imageUrl.size != 0) {
            if (!list[position].imageUrl[position].isNullOrEmpty()) {
                if (list[position].imageUrl[position].startsWith("http")) {
                    Glide.with(mcontext)
                        .asDrawable()
                        .load(list[position].imageUrl[position])
                        .apply(options)
                        .into(holder.upload_img)
                    holder.removePhoto.setVisibility(View.VISIBLE)
                    holder.addPhoto.visibility = View.GONE
                } else {
                    Glide.with(mcontext)
                        .asDrawable()
                        .load(list[position].imageUrl[position])
                        .apply(options)
                        .into(object :
                            ImageViewTarget<Drawable?>(holder.upload_img) {
                            override fun setResource(resource: Drawable?) {
                                holder.upload_img.setImageDrawable(resource) //This is important
                            }
                        })
                    holder.removePhoto.setVisibility(View.VISIBLE)
                    holder.addPhoto.visibility = View.GONE
                }
            }
        }
        holder.itemView.setOnClickListener(View.OnClickListener {
            listenerAddImage.addImages(holder.adapterPosition)
            listenerEditImage.editImage(holder.adapterPosition)
        })
        holder.removePhoto.setOnClickListener(View.OnClickListener {
            listenerremoveimage.removeImages(holder.adapterPosition)
        })

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var upload_img = itemView.findViewById<ImageView>(R.id.img_edit)
        var addPhoto = itemView.findViewById<ImageView>(R.id.add_Photo)
        var removePhoto = itemView.findViewById<ImageView>(R.id.remove_Photo)

    }


}

interface addImage {

    fun addImages(position: Int)

}

interface removeImage {

    fun removeImages(position: Int)
}

interface editImage {
    fun editImage(position: Int)
}

