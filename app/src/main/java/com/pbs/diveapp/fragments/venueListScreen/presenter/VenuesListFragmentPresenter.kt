package com.pbs.diveapp.fragments.venueListScreen.presenter

import com.pbs.diveapp.fragments.mapScreen.model.GetRestaurantResponse
import com.pbs.diveapp.fragments.venueListScreen.view.VenuesListFragmentView
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.GetRestaurantHandler

class VenuesListFragmentPresenter(val view :VenuesListFragmentView):VenuesListFragmentHandler {
    override fun getList() {

    }

    override fun getRest(location: String, type: String, key: String, radius: String) {
        view.showProgressBar()
        WebService.mInstance.getRestaurant(location,type,key,radius,object :GetRestaurantHandler{
            override fun onSuccessful(getRestaurantResponse: GetRestaurantResponse) {
                view.hideProgressBar()
                view.onSuccessfulRest(getRestaurantResponse)
            }

            override fun onError(message: String?) {
               view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }

}