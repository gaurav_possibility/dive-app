package com.pbs.diveapp.fragments.signUpFlow.phoneVerify.presenter

import com.dating.datingapp.webServices.handler.signupHandler
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.phoneVerify.view.PhoneVerifyScreenFragmentView
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model.SignUpResponse
import com.pbs.diveapp.webServices.WebService
import com.pbs.diveapp.webServices.handler.asfasdfasddsfHandler

class PhoneVerifyScreenFragmentPresenter(val view: PhoneVerifyScreenFragmentView) :
    PhoneVerifyScreenFragmentHandler {
    override fun updateProfile(hashMap: HashMap<String, String>) {
        view.showProgressBar()
        WebService.mInstance.updateProfile(hashMap, object : signupHandler {
            override fun onSuccess(response: LoginResponse) {
                view.hideProgressBar()
                view.onSuccessfulUpdateProfile(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }

    override fun sendOTP(hashMap: HashMap<String, String>) {
        view.showProgressBar()
        WebService.mInstance.sendOTP(hashMap, object : signupHandler {
            override fun onSuccess(response: LoginResponse) {
                view.hideProgressBar()
                view.onSuccessfullySendOTP(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }

        })
    }

    override fun verifyOTP(hashMap: HashMap<String, String>) {
        view.showProgressBar()
        WebService.mInstance.verify(hashMap, object : asfasdfasddsfHandler {
            override fun onSuccess(response: SignUpResponse) {
                view.hideProgressBar()
                view.onSuccessfullyVerifyOTP(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }

        })

    }

}