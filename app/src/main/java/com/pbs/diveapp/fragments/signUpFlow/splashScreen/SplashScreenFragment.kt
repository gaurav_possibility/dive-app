package com.pbs.diveapp.fragments.signUpFlow.splashScreen

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.MainActivity
import com.pbs.diveapp.fragments.BaseFragment
import com.pbs.diveapp.fragments.signUpFlow.userName.NameFragment
import com.pbs.diveapp.fragments.signUpFlow.welcomeScreen.WelcomeScreenFragment
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.loged_in_dialog.view.*

/*
* This class is used as Splash screen Container for this app
*
* created by GTB on 20/09/2021
*/
class SplashScreenFragment : BaseFragment() {

    lateinit var mContext: Context
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_splash_screen
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        Handler(Looper.getMainLooper()).postDelayed({
            if (SharedPref.instance.getBoolean(AppConstant.USER_LOGGED_IN)) {
                if (SharedPref.instance.getString(AppConstant.USER_NAME)!!.isEmpty()) {
                   loggedInAccount(SharedPref.instance.getString(AppConstant.EMAIL)!!)
                } else {
                    requireActivity().startActivity(
                        Intent(mContext, MainActivity::class.java)
                            .setFlags(
                                Intent.FLAG_ACTIVITY_SINGLE_TOP or
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            )
                    )
                    requireActivity().finishAffinity()
                }
            } else {
                CommonMethods.replaceFragmentActivityWithoutStack(
                    fragmentManager,
                    WelcomeScreenFragment(),
                    R.id.splashContainer
                )
            }
        }, 1500)
    }
    private fun loggedInAccount(email:String) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(mContext)
        builder.setCancelable(false)
        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        @SuppressLint("InflateParams")
        val view: View = inflater.inflate(R.layout.loged_in_dialog, null)
        builder.setView(view)
        builder.setCancelable(false)
        val formPopup = builder.create()
        formPopup.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val title=view.tv_TitleComplete
        title.text="You are previously register with $email please complete your profile."
        val yesTV = view.yesTvComplte
        val noTV = view.noTVHireComplete
        yesTV.setOnClickListener {
            formPopup.dismiss()
            CommonMethods.replaceFragmentActivityWithoutStack(
                fragmentManager,
                NameFragment(),
                R.id.splashContainer)
        }

        noTV.setOnClickListener {
            CommonMethods.replaceFragmentActivityWithoutStack(
                fragmentManager,
                WelcomeScreenFragment(),
                R.id.splashContainer
            )
            formPopup.dismiss()
        }

        formPopup.show()
    }

}