package com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.presenter

import com.pbs.diveapp.webServices.handler.BasicInformationHandler
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.view.SignUpPresenterView
import com.pbs.diveapp.webServices.WebService
import com.pbs.strive.signup.presenter.SingUpPresenterHandler

class SignupPresenter(var view: SignUpPresenterView) : SingUpPresenterHandler {
    override fun signUp(request: HashMap<String, String>) {
        view.showProgressBar()
        WebService.mInstance.signup(request, handler = object : BasicInformationHandler {
            override fun onSuccess(response: LoginResponse) {
                view.hideProgressBar()
                view.onSuccessSignUP(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message.toString())
            }

        })
    }

    override fun updateInfo(auth: String, hashMap: HashMap<String, String>) {
        TODO("Not yet implemented")
    }

//    override fun updateInfo(auth: String, hashMap: HashMap<String, String>) {
//        view.showProgressBar()
//        WebService.mInstance.updateUserDetails(auth, hashMap, object : LoginHandler {
//            override fun onSuccess(response: LoginResponse) {
//                view.hideProgressBar()
//                view.onSuccessfulupdate(response)
//            }
//
//            override fun onError(message: String) {
//                view.hideProgressBar()
//                view.showToast(message)
//            }
//        })
//    }
}