package com.pbs.diveapp.fragments.mapScreen.presenter

interface MapScreenFragmentViewHandler {
    fun getRest(location:String,type:String,key:String,radius:String)
}