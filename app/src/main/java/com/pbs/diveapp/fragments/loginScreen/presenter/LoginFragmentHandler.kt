package com.pbs.diveapp.fragments.loginScreen.presenter

interface LoginFragmentHandler {
    fun login(request: HashMap<String, String>)
    fun socialLogin(request: HashMap<String, String>)
}