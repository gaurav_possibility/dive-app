package com.pbs.diveapp.fragments.signUpFlow.phoneVerify.presenter

interface PhoneVerifyScreenFragmentHandler {
    fun updateProfile(hashMap: HashMap<String,String>)
    fun sendOTP(hashMap: HashMap<String,String>)
    fun verifyOTP(hashMap: HashMap<String,String>)
}