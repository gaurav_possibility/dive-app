package com.pbs.diveapp.fragments.notification.presenter

interface ChatScreenFragmentHandler {
    fun getMatchList()
}