package com.pbs.diveapp.fragments.likeScreen.adapter


import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import coil.api.load
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.likeScreen.model.Images
import com.pbs.diveapp.utils.CommonMethods
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlin.math.round


class ImageAdapter(
    val mContext: Context,
    private val list: ArrayList<Images>
) : PagerAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout: View =
            inflater.inflate(R.layout.item_image_layout, view, false)!!
        val imageView: ImageView = imageLayout
            .findViewById<View>(R.id.ivUserImages) as ImageView


       // CommonMethods.LoadImage(imageView,list[position].file)

        imageView.load(list[position].file) {
            crossfade(true)
            transformations(coil.transform.RoundedCornersTransformation(1F))
        }
      //  imageView.setImageResource(list[position])
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}
    override fun saveState(): Parcelable? {
        return null
    }
}