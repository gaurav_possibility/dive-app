package com.dating.datingapp.fragments.selectPhotos.model

data class SelectPhotoRespons(
	val data: DataPhoto? = null,
	val success: Boolean? = null,
	val message: String? = null
)

data class LanguagesItem(
	val language: String? = null
)

data class DataPhoto(
	val firstname: String? = null,
	val career: String? = null,
	val education: String? = null,
	val gender: String? = null,
	val latitude: String? = null,
	val questions: List<Any?>? = null,
	val isBlocked: Boolean? = null,
	val description: String? = null,
	val subscription: String? = null,
	val deviceId: String? = null,
	val createdAt: String? = null,
	val forgotPassword: Boolean? = null,
	val personalitys: List<PersonalitysItem?>? = null,
	val email: String? = null,
	val height: String? = null,
	val longitude: String? = null,
	val updatedAt: String? = null,
	val loginuser: String? = null,
	val images: List<String?>? = null,
	val address: String? = null,
	val languages: List<LanguagesItem?>? = null,
	val usertype: String? = null,
	val avatar: String? = null,
	val community: String? = null,
	val lastname: String? = null,
	val religion: String? = null,
	val phone: String? = null,
	val dob: String? = null,
	val companyname: String? = null,
	val lookingfor: String? = null,
	val socialId: String? = null,
	val isprofile: Boolean? = null,
	val _id: String? = null,
	val age: String? = null,
	val sexual: String? = null
)

data class PersonalitysItem(
	val personality: String? = null
)

