
package com.pbs.diveapp.fragments.signUpFlow.signUpScreen.model.model;


import com.google.gson.annotations.SerializedName;

public class SignUpData {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("deviceId")
    private String mDeviceId;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("forgotPassword")
    private Boolean mForgotPassword;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("isApprovedByAdmin")
    private Long mIsApprovedByAdmin;
    @SerializedName("isBlocked")
    private Boolean mIsBlocked;
    @SerializedName("isDeleted")
    private Boolean mIsDeleted;
    @SerializedName("latitude")
    private Double mLatitude;
    @SerializedName("loggedIn")
    private Boolean mLoggedIn;
    @SerializedName("longitude")
    private Double mLongitude;
    @SerializedName("name")
    private String mName;
    @SerializedName("notification")
    private Boolean mNotification;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("report")
    private Long mReport;
    @SerializedName("socialId")
    private String mSocialId;
    @SerializedName("type")
    private Long mType;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("usertype")
    private String mUsertype;
    @SerializedName("id")
    private String m_id;
    @SerializedName("otp")
    private String otp;
    @SerializedName("otpId")
    private String otpId;
    @SerializedName("userId")
    private String userId;

    @SerializedName("education")
    private String education;

    public String getmAvatar() {
        return mAvatar;
    }

    public void setmAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    @SerializedName("career")
    private String career;
    @SerializedName("religion")
    private String religion;
    @SerializedName("community")
    private String community;

    public String getLoginuser() {
        return loginuser;
    }

    public void setLoginuser(String loginuser) {
        this.loginuser = loginuser;
    }

    @SerializedName("loginuser")
    private String loginuser;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpId() {
        return otpId;
    }

    public void setOtpId(String otpId) {
        this.otpId = otpId;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Boolean getForgotPassword() {
        return mForgotPassword;
    }

    public void setForgotPassword(Boolean forgotPassword) {
        mForgotPassword = forgotPassword;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public Long getIsApprovedByAdmin() {
        return mIsApprovedByAdmin;
    }

    public void setIsApprovedByAdmin(Long isApprovedByAdmin) {
        mIsApprovedByAdmin = isApprovedByAdmin;
    }

    public Boolean getIsBlocked() {
        return mIsBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        mIsBlocked = isBlocked;
    }

    public Boolean getIsDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        mIsDeleted = isDeleted;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        mLatitude = latitude;
    }

    public Boolean getLoggedIn() {
        return mLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        mLoggedIn = loggedIn;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double longitude) {
        mLongitude = longitude;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Boolean getNotification() {
        return mNotification;
    }

    public void setNotification(Boolean notification) {
        mNotification = notification;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public Long getReport() {
        return mReport;
    }

    public void setReport(Long report) {
        mReport = report;
    }

    public String getSocialId() {
        return mSocialId;
    }

    public void setSocialId(String socialId) {
        mSocialId = socialId;
    }

    public Long getType() {
        return mType;
    }

    public void setType(Long type) {
        mType = type;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

    public String get_id() {
        return m_id;
    }

    public void set_id(String _id) {
        m_id = _id;
    }

}
