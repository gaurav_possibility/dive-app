package com.pbs.diveapp.utils

import android.app.Application
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException

class ApplicationGlobal:Application() {

    companion object {
        lateinit var isSocket: Socket
    }

    override fun onCreate() {
        super.onCreate()
        CommonMethods.invoke(applicationContext)
        SharedPref(applicationContext)

        //this is used to connect with socket
        try {
            isSocket = IO.socket(AppConstant.SOCKET_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    public fun getSocket(): Socket {
        return isSocket
    }
}