package com.pbs.diveapp.utils

object AppConstant {
    const val longitude: Long=0.0.toLong()
    const val Latitude: Long =0.0.toLong()
    const val Device_ID: String=""
    const val APP_NAME: String = "Dive"
    const val PREF_NAME = "Dive_App"
    const val REQUEST_CHOOSE_ADDRESS: Int = 881
    const val CAMERA_INTENT_REQUEST: Int = 7599
    const val GALLERY_INTENT_REQUEST: Int = 9543
    const val SPLASH_TIMER = 3000L
    const val USER_DATA = "USER_DATA"
    const val CAMERA_PERMISSIONS_REQUEST_CODE = 323L
    const val CHANNEL_ID = "CHANNEL_INBOARD"
    const val ACCESS_TOKEN = "access_token"
    const val APP_BASE_URL = "http://3.15.220.47/api/"
    const val SOCKET_URL="http://3.15.220.47:3000"
//    const val APP_BASE_URL = "http://3.22.101.65:4000/api/"


    const val LOCATION_ON: String = "LOCATION_ON"
    const val FIREBASE_TOKEN: String = "FIREBASE_TOKEN"
    const val NOTIFICATION_ON: String = "NOTIFICATION_ON"
    const val LOCATION_REQUEST = 1000

    const val USER_LOGGED_IN: String = "userLoggedIn"
    const val FIRST_SIGN_UP: String = "firstSignUp"
    const val SERVER_ERROR_MESSAGE = ""

    const val DATA = "data"
    const val TITLE = "title"
    const val USER_ADDRESS = "USER_ADDRESS"
    const val USER_ID = "USER_ID"
    const val USER_NAME = "USER_Name"
    const val EMAIL = "EMAIL"
    const val AcessToken = "AcessToken"
    const val LIKE_FROM_PROFILE="like_from_profile"


    const val  ALL_PERMISSIONS = 22
    const val PICK_IMAGE_CAMERA = 1
    const val PICK_IMAGE_GALLERY = 2

}