package com.pbs.diveapp.utils

interface BaseView {
    fun showToast(message:String)
    fun showProgressBar()
    fun hideProgressBar()
}