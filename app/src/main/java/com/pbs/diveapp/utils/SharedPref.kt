package com.pbs.diveapp.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPref(context: Context) {
    private val sharedpreferences: SharedPreferences

    companion object {
         lateinit var instance: SharedPref
            private set
    }

    init {
        instance = this
        sharedpreferences = context.getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
    }
    fun setString(name: String?, value: String?) {
        sharedpreferences.edit().putString(name, value).apply()
    }

    fun setInt(name: String?, value: Int) {
        sharedpreferences.edit().putInt(name, value).apply()
    }

    fun setFloat(name: String?, value: Float) {
        sharedpreferences.edit().putFloat(name, value).apply()
    }

    fun getFloat(name: String?): Float {
        return sharedpreferences.getFloat(name, 0.0f)
    }

    fun getString(name: String?): String? {
        return sharedpreferences.getString(name, "")
    }

    fun getInt(name: String?): Int {
        return sharedpreferences.getInt(name, 0)
    }

    fun setBoolean(name: String?, value: Boolean?) {
        sharedpreferences.edit().putBoolean(name, value!!).apply()
    }

    fun getBoolean(name: String?): Boolean {
        return sharedpreferences.getBoolean(name, false)
    }

    fun clear() {
        sharedpreferences.edit().clear().apply()
    }

    var locationList: String?
        get() = sharedpreferences.getString("LocationList", "")
        set(list) {
            sharedpreferences.edit().putString("LocationList", list).apply()
        }

}
