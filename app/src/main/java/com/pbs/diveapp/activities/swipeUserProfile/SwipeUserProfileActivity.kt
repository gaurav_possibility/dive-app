package com.pbs.diveapp.activities.swipeUserProfile

import android.app.Dialog
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.swipeUserProfile.adapter.ImageAdapter
import com.pbs.diveapp.activities.swipeUserProfile.adapter.ItemClick
import com.pbs.diveapp.fragments.likeScreen.model.DataX
import com.pbs.diveapp.fragments.likeScreen.model.Images
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SnapHelperOneByOne
import kotlinx.android.synthetic.main.activity_swipe_user_profile.*
import kotlinx.android.synthetic.main.item_toolbar.*
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator

class SwipeUserProfileActivity : AppCompatActivity(),View.OnClickListener,ItemClick {
    lateinit var userDetails:DataX
    val imageList = ArrayList<Images>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swipe_user_profile)
        ivBackArrow.setColorFilter(Color.WHITE)
        allClicks()
        userDetails = Gson().fromJson(intent.getStringExtra("UserDetailsGet"),DataX::class.java)
        tvUserName.text = userDetails.name
        CommonMethods.setProfileImage(this,userDetails.image,ivUserProfileImage)
        tvUserTagName.text = userDetails.email
        tvLookingFor.text="I'm looking for a : " + userDetails.here_for
        tvStraight.text =  userDetails.looking_for
        tvEducation.text = userDetails.education
   imageList.clear()
            imageList.addAll(userDetails.user_images)
        rvUserImageList.layoutManager = GridLayoutManager(this,4)
        rvUserImageList.adapter = ImageAdapter(this,imageList,this)
    }

    private fun allClicks(){
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.ivBackArrow->{
                onBackPressed()
            }
        }
    }

    override fun onClickItem(position: Int) {
        var snapHelper: LinearSnapHelper? = null
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.item_alert_layout_images)
        val rvIMage = dialog.findViewById<RecyclerView>(R.id.rvImages)
        val indicator = dialog.findViewById<ScrollingPagerIndicator>(R.id.indicator)

        snapHelper = SnapHelperOneByOne()
        rvIMage.setOnFlingListener(null)
        snapHelper!!.attachToRecyclerView(rvIMage)
        rvIMage.setLayoutManager(
            LinearLayoutManager(
                this,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        )
        rvIMage.adapter = ImageAdapter(this,imageList,this)
        indicator.attachToRecyclerView(rvIMage)

        dialog.show()
    }
}