package com.pbs.diveapp.activities.userProfile.presenter

import com.dating.datingapp.webServices.handler.loginHandler
import com.pbs.diveapp.activities.userProfile.view.UserProfileActivityView
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.webServices.WebService
import okhttp3.MultipartBody

class UserProfileActivityPresenter(val view:UserProfileActivityView):UserProfileActivityHandler {
    override fun uploadImage(image: MultipartBody.Part?) {
        view.showProgressBar()
        WebService.mInstance.updateImage(image,object :loginHandler{
            override fun onSuccess(response: LoginResponse) {
                view.hideProgressBar()
                view.onSuccessful(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }

    override fun getUserDetails() {
        view.showProgressBar()
        WebService.mInstance.getUserDetails(object :loginHandler{
            override fun onSuccess(response: LoginResponse) {
               view.hideProgressBar()
                view.onSuccessfulDetail(response)
            }

            override fun onError(message: String?) {
                view.hideProgressBar()
                view.showToast(message!!)
            }
        })
    }
}