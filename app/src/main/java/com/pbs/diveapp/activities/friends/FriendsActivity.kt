package com.pbs.diveapp.activities.friends

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.friends.adapter.FriendsAdapter
import com.pbs.diveapp.activities.friends.model.FriendsModel
import kotlinx.android.synthetic.main.activity_freinds.*
import kotlinx.android.synthetic.main.item_toolbar.*

class FriendsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var friendsAdapter: FriendsAdapter
    lateinit var list: MutableList<FriendsModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_freinds)
        list = mutableListOf()
        tv_screen.text = "Friends"
        ivBackArrow.setOnClickListener(this)
        tv_screen.setTextColor(resources.getColor(R.color.white))

        list.add(FriendsModel("Elbert", "24"))
        list.add(FriendsModel("Jessica", "21"))
        list.add(FriendsModel("sam", "20"))
        list.add(FriendsModel("John", "28"))
        friendsAdapter = FriendsAdapter(this, list)
        recyclerview_list.adapter = friendsAdapter
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.ivBackArrow -> {
                onBackPressed()
            }
        }
    }
}