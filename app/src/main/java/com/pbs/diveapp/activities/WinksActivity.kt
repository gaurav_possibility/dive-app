package com.pbs.diveapp.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_toolbar.*

class WinksActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_winks)
        onClicks()
    }

    fun onClicks() {
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                onBackPressed()
            }
        }
    }
}