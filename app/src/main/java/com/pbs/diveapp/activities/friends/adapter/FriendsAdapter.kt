package com.pbs.diveapp.activities.friends.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.askout.AskOutActivity
import com.pbs.diveapp.activities.directaskout.DirectAskOutActivity
import com.pbs.diveapp.activities.friends.model.FriendsModel
import kotlinx.android.synthetic.main.list_friends.view.*

class FriendsAdapter(var mContext: Context, var list: MutableList<FriendsModel>) :
    RecyclerView.Adapter<FriendsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(mContext).inflate(R.layout.list_friends, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.btnAskOut.setOnClickListener(View.OnClickListener {

            mContext.startActivity(
                Intent(
                    mContext,
                    DirectAskOutActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            )
        })

    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var imgFriends = itemView.Img_Friends!!
        var btnAskOut = itemView.btnAskOut!!

    }

}