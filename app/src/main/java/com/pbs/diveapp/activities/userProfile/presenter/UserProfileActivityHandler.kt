package com.pbs.diveapp.activities.userProfile.presenter

import okhttp3.MultipartBody

interface UserProfileActivityHandler {
    fun uploadImage(image: MultipartBody.Part?)
    fun getUserDetails()
}