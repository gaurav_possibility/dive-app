package com.pbs.diveapp.activities.invitefriends

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R

class InviteFriendsItemAdapters() : RecyclerView.Adapter<InviteFriendsItemAdapters.Holder>() {
    class Holder(view: View) : RecyclerView.ViewHolder(view) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val items = LayoutInflater.from(parent.context)
            .inflate(R.layout.invite_friend_itemview, parent, false)
        return Holder(items)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }
}