package com.pbs.diveapp.activities.checkInUserList

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.checkInUserList.adapter.CheckInUserAdapter
import com.pbs.diveapp.activities.checkInUserList.adapter.ItemClick
import kotlinx.android.synthetic.main.activity_check_in_user_list.*
import kotlinx.android.synthetic.main.item_toolbar.*

class CheckInUserListActivity : AppCompatActivity(), View.OnClickListener, ItemClick {
    var listName = arrayListOf(
        "asdf",
        "asdfaf",
        "asdf",
        "asdfaf",
        "asdf",
        "asdfaf",
        "asdf",
        "asdfaf",
        "asdf",
        "asdfaf"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in_user_list)
        ivBackArrow.setColorFilter(Color.WHITE)
        allClicks()
        rvCheckInUserList.layoutManager = GridLayoutManager(this, 2)
        rvCheckInUserList.adapter = CheckInUserAdapter(this, listName, this)
    }

    private fun allClicks() {
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                onBackPressed()
            }
        }
    }

    override fun onClick(position: Int) {
        dialogOfferItem()
    }


    private fun dialogOfferItem() {
        val dialogBuilder =
            android.app.AlertDialog.Builder(this)

        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView: View =
            inflater.inflate(R.layout.item_offer_an_item_layout, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(true)

        val alertDialog = dialogBuilder.create()
        val layoutDrink = dialogView.findViewById<View>(R.id.layoutDrink) as LinearLayout
        val layoutCoffee = dialogView.findViewById<View>(R.id.layoutCoffee) as LinearLayout
        val layoutDessert = dialogView.findViewById<View>(R.id.layoutDessert) as LinearLayout
        val layoutAppetizer = dialogView.findViewById<View>(R.id.layoutAppetizer) as LinearLayout

        layoutDessert.setOnClickListener {
            alertDialog.dismiss()
        }
        layoutDrink.setOnClickListener {
            alertDialog.dismiss()
        }
        layoutCoffee.setOnClickListener {
            alertDialog.dismiss()
        }
        layoutAppetizer.setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.show()
        alertDialog.window!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}