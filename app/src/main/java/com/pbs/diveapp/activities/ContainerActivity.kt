package com.pbs.diveapp.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.chatScreen.ChatScreenFragment
import com.pbs.diveapp.fragments.matchScreen.MatchScreenFragment
import com.pbs.diveapp.fragments.venueListScreen.VenuesListFragment
import com.pbs.diveapp.utils.CommonMethods

class ContainerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
        val fromScreen = intent.getStringExtra("FromScreens")
        if (fromScreen == "1") {
            CommonMethods.addFragmentActivity(
                supportFragmentManager,
                ChatScreenFragment(intent.getStringExtra("UserDetails").toString()),
                R.id.fragmentContainer
            )
        } else if (fromScreen == "2") {
            CommonMethods.addFragmentActivity(
                supportFragmentManager,
                MatchScreenFragment(intent.getStringExtra("UserDetails").toString()),
                R.id.fragmentContainer
            )
        }
    }
}