package com.pbs.diveapp.activities.invitefriends

import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R

class InviteFriendsActivity : AppCompatActivity() {
    lateinit var recyclerview_list: RecyclerView
    lateinit var inviteFriendsItemAdapters: InviteFriendsItemAdapters
    lateinit var invite:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite_friends)

        recyclerview_list = findViewById(R.id.recyclerview_list)
        invite=findViewById(R.id.invite)
        invite.setShadowLayer(1F, 0F, 0F,Color.BLACK)
        inviteFriendsItemAdapters = InviteFriendsItemAdapters()
        recyclerview_list.layoutManager = LinearLayoutManager(applicationContext)
        recyclerview_list.adapter = inviteFriendsItemAdapters

    }

}