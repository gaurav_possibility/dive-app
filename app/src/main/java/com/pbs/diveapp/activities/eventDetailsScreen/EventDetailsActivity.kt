package com.pbs.diveapp.activities.eventDetailsScreen

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_toolbar.*

class EventDetailsActivity : AppCompatActivity(),View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)
        ivBackArrow.setColorFilter(Color.WHITE)
        allClicks()
    }
    private fun allClicks(){
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.ivBackArrow->{
                onBackPressed()
            }
        }
    }
}