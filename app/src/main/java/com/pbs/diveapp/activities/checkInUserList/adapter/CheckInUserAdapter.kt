package com.pbs.diveapp.activities.checkInUserList.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_check_in_layout.view.*

class CheckInUserAdapter(val mContext: Context, val list: ArrayList<String>,val click: ItemClick) :
    RecyclerView.Adapter<CheckInUserAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CheckInUserAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.item_check_in_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CheckInUserAdapter.MyViewHolder, position: Int) {

        holder.btnSendWink.setOnClickListener {
            click.onClick(position)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val btnSendWink: AppCompatButton = item.btnSendWink
        val btnDirectAsk: AppCompatButton = item.btnDirectAsk
    }
}
interface ItemClick{
    fun onClick(position: Int)
}