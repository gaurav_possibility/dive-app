package com.pbs.diveapp.activities.userProfile.view

import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.utils.BaseView

interface UserProfileActivityView:BaseView {
    fun onSuccessful(loginResponse: LoginResponse)
    fun onSuccessfulDetail(loginResponse: LoginResponse)
}