package com.pbs.diveapp.activities.directaskout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_toolbar.*

class DirectAskOutActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_direct_ask_out)

        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.ivBackArrow -> {
                onBackPressed()
            }
        }
    }
}