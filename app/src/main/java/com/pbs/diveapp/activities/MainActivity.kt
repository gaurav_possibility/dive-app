package com.pbs.diveapp.activities

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.likeScreen.ListScreenFragment
import com.pbs.diveapp.fragments.mapScreen.MapScreenFragment
import com.pbs.diveapp.fragments.notification.NotificationFragment
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.activity_main.*

/*
* This class is used as Home Screen for this app
*
* created by GTB on 20/09/2021
*/
class MainActivity : AppCompatActivity() {
    private var backPressed = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 4
        //set viewPager Page Changed
        viewPager.currentItem = 0
        bottom_nav.menu.getItem(0).isChecked = true

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                //set Bottom Navigation with ViewPager
                bottom_nav.menu.getItem(position).isChecked = true
            }

        })

        //Bottom Navigation Selected Listener
        bottom_nav.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_like -> {
                    viewPager.currentItem = 0
                }
                R.id.nav_map -> {
                    viewPager.currentItem = 1
                }

                R.id.chat -> {
                    viewPager.currentItem = 2
                }
            }
            false
        }
    }

    //view pager Adapter Class
    inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(
        fragmentManager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> ListScreenFragment()
                1 -> MapScreenFragment()
                else -> NotificationFragment()
            }
        }


        override fun getCount() = 3
    }

    override fun onBackPressed() {
        if (backPressed) {
            backPressed = false
            CommonMethods.showToast(
                this,
                "Press again to exit"
            )
            Handler().postDelayed({ backPressed = true }, 2000)
        } else super.onBackPressed()
    }
}