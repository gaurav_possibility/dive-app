package com.pbs.diveapp.activities.meetAndEvents

import android.graphics.Color.red
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.activity_meet_and_events.*
import kotlinx.android.synthetic.main.fragment_map_screen.*

class MeetAndEventsActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meet_and_events)
        allClick()

    }

    fun allClick() {
        tv_text_list.setOnClickListener(this)
        tv_text_map.setOnClickListener(this)
        tv_text_events.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tv_text_list -> {
                tv_text_list.setTextColor(resources.getColor(R.color.white))
                tv_text_map.setTextColor(resources.getColor(R.color.black))
                tv_text_events.setTextColor(resources.getColor(R.color.black))
                tv_text_list.setBackgroundColor(resources.getColor(R.color.red))
                tv_text_events.setBackgroundColor(resources.getColor(R.color.white))
                tv_text_map.setBackgroundColor(resources.getColor(R.color.white))


            }

            R.id.tv_text_map -> {
                tv_text_list.setTextColor(resources.getColor(R.color.black))
                tv_text_map.setTextColor(resources.getColor(R.color.white))
                tv_text_events.setTextColor(resources.getColor(R.color.black))
                tv_text_list.setBackgroundColor(resources.getColor(R.color.white))
                tv_text_events.setBackgroundColor(resources.getColor(R.color.white))
                tv_text_map.setBackgroundColor(resources.getColor(R.color.red))
                tv_events_des.visibility=View.GONE
                tv_events.visibility=View.GONE
                tv_meet_person.visibility=View.VISIBLE
                tv_dive_des.visibility=View.VISIBLE
            }

            R.id.tv_text_events -> {
                tv_text_list.setTextColor(resources.getColor(R.color.black))
                tv_text_map.setTextColor(resources.getColor(R.color.black))
                tv_text_events.setTextColor(resources.getColor(R.color.white))
                tv_text_list.setBackgroundColor(resources.getColor(R.color.white))
                tv_text_events.setBackgroundColor(resources.getColor(R.color.red))
                tv_text_map.setBackgroundColor(resources.getColor(R.color.white))
                tv_events_des.visibility=View.VISIBLE
                tv_events.visibility=View.VISIBLE
                tv_meet_person.visibility=View.GONE
                tv_dive_des.visibility=View.GONE

            }
        }
    }
}