package com.pbs.diveapp.activities.userProfile

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.SubscriptionActivity
import com.pbs.diveapp.activities.WinksActivity
import com.pbs.diveapp.activities.editInfo.EditInfoActivity
import com.pbs.diveapp.activities.friends.FriendsActivity
import com.pbs.diveapp.activities.setting.SettingActivity
import com.pbs.diveapp.activities.userProfile.presenter.UserProfileActivityPresenter
import com.pbs.diveapp.activities.userProfile.view.UserProfileActivityView
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.fragments.loginScreen.LoginModel.LoginResponse
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.AppConstant.ALL_PERMISSIONS
import com.pbs.diveapp.utils.AppConstant.PICK_IMAGE_CAMERA
import com.pbs.diveapp.utils.AppConstant.PICK_IMAGE_GALLERY
import com.pbs.diveapp.utils.CommonMethods
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.item_toolbar.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File

class UserProfileActivity : AppCompatActivity(), View.OnClickListener, UserProfileActivityView {

    lateinit var userDetails: Data
    private var mImageFile: File? = null
    var imgPath: String = ""

    lateinit var mPresenter: UserProfileActivityPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        ivBackArrow.setColorFilter(Color.WHITE)
        mPresenter = UserProfileActivityPresenter(this)
        allClicks()
        try {
            userDetails = Gson().fromJson(
                SharedPref.instance.getString(AppConstant.USER_DATA),
                Data::class.java
            )
        } catch (e: java.lang.Exception) {
        }
        CommonMethods.setProfileImage(this, userDetails.image.toString(), ivUserProfileImage)
        tvUserName.text = userDetails.name
//        tvUserDetails.text = userDetails.bio
        tvLookingFor.text = "I'm looking for a : " + userDetails.here_for
        tvStraight.text = userDetails.looking_for
        tvEducation.text = userDetails.education
        tvHeight.text = userDetails.height


        if (userDetails.email.isNullOrEmpty().not()) {
            tvUserTagName.text = userDetails.email
        } else {
            tvUserTagName.text = userDetails.mobile
        }
    }

    private fun allClicks() {
        LL_SuperLike.setOnClickListener(this)
        LL_BuyWinks.setOnClickListener(this)
        ivBackArrow.setOnClickListener(this)
        layoutSettings.setOnClickListener(this)
        layoutEditInfo.setOnClickListener(this)
        ivUserProfileImage.setOnClickListener(this)
        layoutFriends.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.LL_SuperLike -> {
                startActivity(
                    Intent(
                        this,
                        SubscriptionActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )

            }
            R.id.LL_BuyWinks -> {
                startActivity(
                    Intent(
                        this,
                        WinksActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )

            }
            R.id.layoutSettings -> {
                startActivity(
                    Intent(
                        this,
                        SettingActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )

            }
            R.id.layoutEditInfo -> {
                startActivity(
                    Intent(
                        this,
                        EditInfoActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )

            }
            R.id.ivBackArrow -> {
                onBackPressed()
            }
            R.id.ivUserProfileImage -> {
                selectImagesCameraPermission()
            }
            R.id.layoutFriends -> {
                startActivity(
                    Intent(
                        this,
                        FriendsActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )
            }
        }
    }

    private fun selectImagesCameraPermission() {
        if (CommonMethods.isReadStorageGranted(this)
        ) {
            Log.e("qwer234", " click1 ")
            if (CommonMethods.isCameraGranted(this!!)) {
                selectImage()
                Log.e("qwer234", " click2 ")
            } else {
                val permissions =
                    arrayOf(Manifest.permission.CAMERA)
                request_Permissions(this, 2, *permissions)
                Log.e("qwer234", " click3 ")
            }
        } else {
            val permissions = arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            request_Permissions(this, 1, *permissions)
            Log.e("qwer234", " click4 ")
        }
    }

    private fun request_Permissions(
        activity: Activity?,
        permissionId: Int,
        vararg permissions: String?
    ) {
        requestPermissions(permissions, permissionId)
    }

    private fun selectImage() {
        try {
            val pm: PackageManager = packageManager
            val hasPerm = pm.checkPermission(Manifest.permission.CAMERA, packageName)
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                val options =
                    arrayOf<CharSequence>("Take Photo", "Choose From Gallery", "Cancel")
                val builder =
                    AlertDialog.Builder(this)
                builder.setTitle("Select Option")
                builder.setItems(
                    options
                ) { dialog: DialogInterface, item: Int ->
                    if (options[item] == "Take Photo") {
                        dialog.dismiss()
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        mImageFile = CommonMethods.createImageFile(this)
                        cameraIntent.putExtra(
                            MediaStore.EXTRA_OUTPUT,
                            FileProvider.getUriForFile(
                                this,
                                packageName + ".provider",
                                mImageFile!!
                            )
                        )
                        startActivityForResult(cameraIntent, PICK_IMAGE_CAMERA)
                    } else if (options[item] == "Choose From Gallery") {
                        dialog.dismiss()
                        val pickPhoto = Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        )
                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)
                    } else if (options[item] == "Cancel") {
                        dialog.dismiss()
                    }
                }
                builder.show()
            } else requestPermission()
            // Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (e: Exception) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }

    }

    private fun requestPermission() {
        val permissions = arrayOf(Manifest.permission.CAMERA)
        requestPermissions(permissions, ALL_PERMISSIONS)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                Log.e("TAGFragment", "External storage2")
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(
                        "TAGFragment",
                        "Permission: " + permissions[0] + "was " + grantResults[0]
                    )
                    //resume tasks needing this permission
//                    selectImage()
                    selectImagesCameraPermission()
                } else {
                    Log.e("TAGFragment", "External storage2 Deny ")

                    // permission was not granted
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                    ) {
//                        selectImagesCameraPermission()

                    } else {
                        val snackbar: Snackbar = Snackbar.make(
                            layout_r,
                            resources.getString(R.string.message_text),
                            Snackbar.LENGTH_LONG
                        )
                        snackbar.setAction(
                            resources.getString(R.string.settings),
                            View.OnClickListener {
                                if (this == null) {
                                    return@OnClickListener
                                }
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri: Uri = Uri.fromParts(
                                    "package",
                                    this.packageName,
                                    null
                                )
                                intent.data = uri
                                this.startActivity(intent)
                            })
                        snackbar.show()
                    }
                }
            }
            2 -> {
                Log.d("TAGFragment", "Camera ")
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(
                        "TAGFragment",
                        "Permission: " + permissions[0] + "was " + grantResults[0]
                    )
                    //resume tasks needing this permission
//                    selectImage()
                    selectImagesCameraPermission()
                } else {
                    Log.e("TAGFragment", "External storage2 Deny ")

                    // permission was not granted
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.CAMERA
                        )
                    ) {

//                        selectImagesCameraPermission()
                    } else {
                        val snackbar: Snackbar = Snackbar.make(
                            layout_r, resources.getString(R.string.message_text),
                            Snackbar.LENGTH_LONG
                        )
                        snackbar.setAction(
                            resources.getString(R.string.settings),
                            View.OnClickListener {
                                if (this == null) {
                                    return@OnClickListener
                                }
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri: Uri = Uri.fromParts(
                                    "package",
                                    this.packageName,
                                    null
                                )
                                intent.data = uri
                                this.startActivity(intent)
                            })
                        snackbar.show()
                    }
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE_CAMERA) {
                try {
                    val selectedImage = mImageFile!!.toUri()

                    val bitmap1 = BitmapFactory.decodeFile(mImageFile!!.path)
                    val newBitMap: Bitmap =
                        CommonMethods.changeImageOrientation(mImageFile!!.path, bitmap1!!)!!
                    val resizedBitMap: Bitmap =
                        CommonMethods.getResizedBitmap(newBitMap, 800, 800)!!

                    imgPath = if (Build.VERSION.SDK_INT != Build.VERSION_CODES.R) {
                        val newImage1: File = CommonMethods.createFileFromBitMap(resizedBitMap)!!
                        newImage1.path
                    } else {
                        mImageFile!!.path
                    }
                    ivUserProfileImage.setImageBitmap(resizedBitMap)

                    var part: MultipartBody.Part? = null
                    if (imgPath.isNotEmpty()) {
                        var fileGallery1: File? = null
                        fileGallery1 = File(imgPath)
                        part = MultipartBody.Part.createFormData(
                            "image", fileGallery1.name, RequestBody.create(
                                "image/*".toMediaTypeOrNull(),
                                fileGallery1
                            )
                        )
                    }
                    mPresenter.uploadImage(part)
                } catch (e: java.lang.Exception) {
                    Log.e("errorPrint", e.localizedMessage)
                }
            } else if (requestCode == PICK_IMAGE_GALLERY && data != null) {
                val selectedImage = data.data
                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(
                            contentResolver,
                            selectedImage
                        )
                    val bytes = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    Log.e("Activity", "Pick from Gallery::>>> ")
                    val newBitMap: Bitmap = CommonMethods.changeImageOrientation(
                        CommonMethods.getRealPathFromUri(
                            this,
                            selectedImage
                        )!!.absolutePath,
                        bitmap!!
                    )!!
                    imgPath = CommonMethods.compressImage(
                        CommonMethods.getRealPathFromUri(
                            this,
                            selectedImage
                        )!!.absolutePath
                    ).toString()

                    ivUserProfileImage.setImageBitmap(newBitMap)

                    var part: MultipartBody.Part? = null
                    if (imgPath.isNotEmpty()) {
                        var fileGallery1: File? = null
                        fileGallery1 = File(imgPath)
                        part = MultipartBody.Part.createFormData(
                            "image", fileGallery1.name, RequestBody.create(
                                "image/*".toMediaTypeOrNull(),
                                fileGallery1
                            )
                        )
                    }
                    mPresenter.uploadImage(part)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onSuccessful(loginResponse: LoginResponse) {
        mPresenter.getUserDetails()
    }

    override fun onSuccessfulDetail(loginResponse: LoginResponse) {
        SharedPref.instance.setString(AppConstant.USER_DATA, Gson().toJson(loginResponse.data))
    }

    override fun showToast(message: String) {
        CommonMethods.showToast(this, message)
    }

    override fun showProgressBar() {
        CommonMethods.showBaseProgressDialog(this)
    }

    override fun hideProgressBar() {
        CommonMethods.hideBaseProgressDialog()
    }

}