package com.pbs.diveapp.activities.swipeUserProfile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.likeScreen.model.Images
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.item_image_layout_swipe_user_profile.view.*

class ImageAdapter(val mContext: Context,val list:ArrayList<Images>,val clickListener:ItemClick):RecyclerView.Adapter<ImageAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageAdapter.MyViewHolder {
        return MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_image_layout_swipe_user_profile,parent,false))
    }

    override fun onBindViewHolder(holder: ImageAdapter.MyViewHolder, position: Int) {
       CommonMethods.setImage(mContext,list[position].file,holder.image)
        holder.itemView.setOnClickListener {
            clickListener.onClickItem(position)
        }
    }

    override fun getItemCount(): Int {
    return list.size
    }

    inner class  MyViewHolder(item: View):RecyclerView.ViewHolder(item){
        val image = item.ivSwipeUserImage
    }
}
interface ItemClick{
    fun onClickItem(position: Int)
}