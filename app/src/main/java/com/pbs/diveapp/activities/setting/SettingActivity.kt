package com.pbs.diveapp.activities.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.loginScreen.LoginModel.Data
import com.pbs.diveapp.utils.AppConstant
import com.pbs.diveapp.utils.SharedPref
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.item_toolbar.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var userDetails: Data

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        allClicks()

        try {
            userDetails = Gson().fromJson(
                SharedPref.instance.getString(AppConstant.USER_DATA),
                Data::class.java
            )
        } catch (e: java.lang.Exception) {
        }

        tv_phoneNumber.text = userDetails.mobile
        tv_userName.text = userDetails.name
        tv_email.text = userDetails.email
        tv_age.text = userDetails.dob.toString()
    }

    fun allClicks() {
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                onBackPressed()
            }
        }
    }
}