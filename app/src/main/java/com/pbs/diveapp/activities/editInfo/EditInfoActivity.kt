package com.pbs.diveapp.activities.editInfo

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R
import kotlinx.android.synthetic.main.item_toolbar.*

class EditInfoActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var recyclerview_image_list: RecyclerView
    lateinit var editInfoAdapterClass: EditInfoAdapterClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_info)

        init()
        onClicks()
    }

    private fun init() {
        recyclerview_image_list = findViewById(R.id.recyclerview_image_list)
        editInfoAdapterClass = EditInfoAdapterClass()
        recyclerview_image_list.layoutManager = GridLayoutManager(applicationContext, 3)
        recyclerview_image_list.adapter = editInfoAdapterClass
    }

    private fun onClicks() {
        ivBackArrow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                onBackPressed()
            }
        }
    }
}