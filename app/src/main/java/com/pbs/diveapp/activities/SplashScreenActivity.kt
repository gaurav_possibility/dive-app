package com.pbs.diveapp.activities

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.pbs.diveapp.R
import com.pbs.diveapp.fragments.signUpFlow.splashScreen.SplashScreenFragment
import com.pbs.diveapp.utils.CommonMethods
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/*
* This class is used as Splash screen Container for this app
*
* created by GTB on 20/09/2021
*/

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        CommonMethods.addFragmentActivity(
            supportFragmentManager,
            SplashScreenFragment(),
            R.id.splashContainer
        )

        //this is for getting KeyHash value
        try {
            val info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("name not found", e.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        }
    }
}