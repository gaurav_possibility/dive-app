package com.pbs.diveapp.activities.venueDetailsScreen

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.pbs.diveapp.R
import com.pbs.diveapp.activities.checkInUserList.CheckInUserListActivity
import com.pbs.diveapp.fragments.mapScreen.model.ResultRest
import com.pbs.diveapp.utils.CommonMethods
import kotlinx.android.synthetic.main.activity_venue_details.*
import kotlinx.android.synthetic.main.item_toolbar.*

class VenueDetailsActivity : AppCompatActivity(),View.OnClickListener {
    lateinit var venueDetail :ResultRest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venue_details)
        ivBackArrow.setColorFilter(Color.WHITE)
        allClicks()
        venueDetail = Gson().fromJson(intent.getStringExtra("restDetails"),ResultRest::class.java)
        tvVenueName.text = venueDetail.name
//        CommonMethods.setImage(this,venueDetail.photos[0].html_attributions[0],ivVenueImage)
    }

    private fun allClicks(){
        ivBackArrow.setOnClickListener(this)
        btnCheckInVenue.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.ivBackArrow->{
                onBackPressed()
            }
            R.id.btnCheckInVenue->{
                startActivity(Intent(this,CheckInUserListActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
        }
    }
}