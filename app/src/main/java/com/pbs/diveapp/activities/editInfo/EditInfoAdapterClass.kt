package com.pbs.diveapp.activities.editInfo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pbs.diveapp.R

class EditInfoAdapterClass ():RecyclerView.Adapter<EditInfoAdapterClass.Holder>() {
    class Holder (view: View): RecyclerView.ViewHolder(view)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.edit_info_recycler_itemview,parent,false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
    }

    override fun getItemCount(): Int {

        return 9
    }
}